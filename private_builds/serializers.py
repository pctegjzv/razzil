from rest_framework import serializers

from commons.furion_client import FurionUtil
from private_builds.models import Build, BuildCode, BuildEndpoint, BuildImageTag, BuildIntegration


class BuildEndpointeSerializer(serializers.ModelSerializer):
    class Meta:
        model = BuildEndpoint


class BuildSimpleEndpointSerializer(serializers.ModelSerializer):
    class Meta:
        model = BuildEndpoint
        exclude = ('builder_image', 'es_host', 'es_username', 'es_password', 'proxy_config')


class BuildCodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = BuildCode
        read_only_fields = ('created_at',)


class BuildCodeForUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BuildCode
        read_only_fields = ('code_id', 'created_at', 'code_repo_client',
                            'code_repo_path', 'code_repo_private_key',
                            'code_repo_user_name', 'code_repo_password')


class BuildSerializer(serializers.ModelSerializer):
    ci_envs = serializers.JSONField(required=False)
    ci_steps = serializers.JSONField(required=False)
    build_filters = serializers.JSONField(required=False)

    class Meta:
        model = Build
        read_only_fields = ('build_id', 'created_at')
    
    def to_representation(self, instance):
        ret = super(BuildSerializer, self).to_representation(instance)
        ret["exec_cmd_enabled"] = instance.exec_cmd_enabled
        return ret


class BuildIncludeCodeSerializer(serializers.ModelSerializer):
    code_repo = BuildCodeSerializer(read_only=True)
    can_removed = serializers.BooleanField()
    config_id = serializers.CharField()
    ci_envs = serializers.JSONField(required=False)
    ci_steps = serializers.JSONField(required=False)
    endpoint_name = serializers.CharField(required=False)
    build_filters = serializers.JSONField(required=False)

    def to_representation(self, instance):
        ret = super(BuildIncludeCodeSerializer, self).to_representation(instance)
        ret["exec_cmd_enabled"] = instance.exec_cmd_enabled
        return ret

    class Meta:
        model = Build
        read_only_fields = ('build_id', 'created_at')


class BuildForUpdateSerializer(serializers.ModelSerializer):
    ci_envs = serializers.JSONField(required=False)
    ci_steps = serializers.JSONField(required=False)
    build_filters = serializers.JSONField(required=False)

    class Meta:
        model = Build
        read_only_fields = ('build_id', 'created_at', 'config_id',
                            'docker_repo_tag', 'docker_repo_version_tag',
                            'notification_id', 'build_cache_enabled', 'is_auto_created',
                            'created_by')

class BuildIntegrationSerializer(serializers.ModelSerializer):
    build_integration_config = serializers.JSONField(required=False)
    integration_execution = serializers.JSONField(required=False)

    class Meta:
        model = BuildIntegration
        read_only_fields = ('created_at', 'updated_at')

class BuildIntegrationForUpdateSerializer(serializers.ModelSerializer):
    build_integration_config = serializers.JSONField(required=False)
    integration_execution = serializers.JSONField(required=False)

    class Meta:
        model = BuildIntegration
        read_only_fields = ('created_at', 'id', 'build_id', 'type',
            'build_integration_config', 'integration_instance_id')