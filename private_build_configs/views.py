import logging
import uuid
import re
import yaml
import string
import traceback

from django.db.models import Q
from config import settings
from django.db import transaction
from django.shortcuts import get_object_or_404
from django.conf import settings

from rest_framework import status, viewsets
from rest_framework.response import Response

from alauda.tracing import tracing_response_time

from commons.code_client.code_client_factory import CodeClientFactory
from commons.docker_build_exception import DockerBuildException
from commons.medusa_client import MedusaClient
from commons.serializers import UUIDListSerializer
from commons.util import RazzilUtil
from private_build_configs.models import (
    BuildConfig,
    BuildConfigCode,
    CODE_REPO_WITHOUT_WEBHOOK,
    BuildConfigIntegration,
    INTEGRATION_SONARQUBE)
from private_code_clients.models import CodeClientUser
from private_build_configs.cache import AlaudaCache, SVN_KEY
from alauda_redis_client import RedisClientFactory
from private_build_configs.serializers import (BuildConfigCodeSerializer,
                                               BuildConfigCodeForUpdateSerializer,
                                               BuildConfigSerializer,
                                               BuildConfigForUpdateSerializer,
                                               BuildConfigCodeIncludeConfigSerializer,
                                               BuildConfigIncludeCodeSerializer,
                                               BuildConfigIntegrationSerializer,
                                               BuildConfigIntegrationForRespSerializer,
                                               BuildConfigIntegrationForUpdateSerializer)
from commons.weblab import Weblab
from commons.sonarqube_client import SonarClient
from commons.davion_client import DavionClient
from ci_envs.models import CIEnvsCatalog
from commons.docker_build_exception import DockerBuildException
from commons.jakiro_client import JakiroInternalClient
from dockerfile_manage.models import Dockerfile

LOG = logging.getLogger(__name__)
ALAUDACI_YML_VERSION = "0.1.0"


class PrivateBuildConfigsViewset(viewsets.ViewSet):
    build_config_query_set = BuildConfig.objects.all()
    build_config_code_query_set = BuildConfigCode.objects.all()
    build_code_clients_query_set = CodeClientUser.objects.all()
    build_config_integration_query_set = BuildConfigIntegration.objects.all()
    dockerfile_query_set = Dockerfile.objects.all()

    @tracing_response_time()
    def list(self, request):
        uuid_querystr = request.query_params.get('uuids', '')
        filter_args = {}
        if uuid_querystr:
            build_config_uuids = uuid_querystr.strip(' ,').split(',')
            serializer = UUIDListSerializer(data={'uuids': build_config_uuids})
            serializer.is_valid(raise_exception=True)
            build_config_uuids = serializer.data['uuids']
            filter_args['config_id__in'] = build_config_uuids

        image_repo_id = request.query_params.get('image_repo_id', '')
        if image_repo_id:
            filter_args['image_repo_id'] = image_repo_id
        build_configs = self.build_config_query_set.filter(**filter_args)

        serializer = BuildConfigIncludeCodeSerializer(build_configs, many=True)
        config_list = serializer.data
        config_list = RazzilUtil.append_endpoint_name(config_list)
        schedule_config_id_list = [
            config.schedule_config_id for config in build_configs if config.schedule_config_id]
        schedule_config_list = MedusaClient.get_schedule_event_config_list(schedule_config_id_list)
        schedule_config_dict = {}
        for schedule_config in schedule_config_list:
            schedule_config_dict[schedule_config.get('id')] = schedule_config.get('rule')
        for config in config_list:
            config['schedule_rule'] = schedule_config_dict.get(config.get('schedule_config_id'), '')
        return Response(config_list)

    @tracing_response_time()
    def retrieve(self, request, private_build_config_id):
        namespace = request.GET.get('namespace')
        build_config = get_object_or_404(self.build_config_query_set,
                                         config_id=private_build_config_id)
        serializer = BuildConfigIncludeCodeSerializer(build_config)
        result = RazzilUtil.append_endpoint_name(serializer.data)
        result['schedule_rule'] = ''
        if build_config.schedule_config_id:
            schedule_config_result = MedusaClient.get_schedule_event_config(
                build_config.schedule_config_id)
            result['schedule_rule'] = schedule_config_result.get('rule')

        if Weblab.CI_INTEGRATION_SONARQUBE(namespace):
            self._fulfill_build_config_integration_retrieve(result, build_config.config_id)
        return Response(result)

    def _fulfill_build_config_integration_retrieve(self, result, build_config_id):
        sonarqube_query = self.build_config_integration_query_set.filter(
            build_config_id=build_config_id,
            type=INTEGRATION_SONARQUBE)
        if not sonarqube_query.exists():
            return
        sonar_integration = sonarqube_query.get()
        if sonar_integration:
            data = BuildConfigIntegrationForRespSerializer(sonar_integration).data
            result["sonarqube"] = data
        return

    @tracing_response_time()
    def retrieve_config_capability(self, request, private_build_config_id):
        build_config = get_object_or_404(self.build_config_query_set,
                                         config_id=private_build_config_id)
        result = self._get_config_capability(build_config)
        return Response(result)

    @tracing_response_time()
    def retrieve_by_hook_code(self, request, code_repo_webhook_code):
        namespace = request.GET.get("namespace")
        build_config_code = get_object_or_404(self.build_config_code_query_set,
                                              code_repo_webhook_code=code_repo_webhook_code)

        code_serializer = BuildConfigCodeIncludeConfigSerializer(build_config_code)

        build_config = get_object_or_404(self.build_config_query_set,
                                         config_id=code_serializer
                                         .data['build_config']['config_id'])

        config_serializer = BuildConfigIncludeCodeSerializer(build_config)
        config_data = RazzilUtil.append_endpoint_name(config_serializer.data)

        if Weblab.CI_INTEGRATION_SONARQUBE(namespace):
            self._fulfill_build_config_integration_retrieve(config_data, build_config.config_id)
        return Response(config_data)

    @tracing_response_time()
    @transaction.atomic
    def create(self, request):
        if not request.data.get('build_image_enabled', True):
            self._check_image_settings(request)

        code_repo = request.data.pop('code_repo')
        if (not RazzilUtil.is_need_username_and_password(
                code_repo.get('code_repo_client'), code_repo.get('code_repo_path')) and
                (code_repo.get('code_repo_username') or code_repo.get('code_repo_password'))):
            raise DockerBuildException('invalid_username_or_password')

        namespace = request.data.get('namespace')
        config_id = request.data['config_id'] = str(uuid.uuid4())

        # if set wildcard, delete schedule rule
        if BuildConfigCode.is_wildcard_of_repo_type_value(code_repo.get("code_repo_type_value")):
            if request.data.get('schedule_rule'):
                LOG.info("deleting schedule_rule ({}), because using wildcard ({})".format(
                    request.data.get('schedule_rule'), code_repo.get("code_repo_type_value")))
                request.data.pop('schedule_rule')

        # if has schedule config
        if request.data.get('schedule_rule'):
            rule = request.data.get('schedule_rule')
            build_callback_url = self.get_schedule_build_url(config_id)
            result = MedusaClient.create_schedule_event_config(rule, build_callback_url)
            request.data['schedule_config_id'] = result.get('id')
            request.data['schedule_config_secret_key'] = result.get('secret_key')

        # if has custom image_tag rule
        if request.data.get('auto_tag_type') == 'CUSTOM_RULE':
            image_tag_rule = request.data.get('customize_tag_rule')
            if image_tag_rule and not self.check_image_tag_rule_is_valid(
                    image_tag_rule,
                    code_repo.get('code_repo_client')):
                raise DockerBuildException('invalid_image_tag_rule')

        # if has dockerfile info
        if request.data.get('dockerfile_info'):
            dockerfile_info = request.data.pop('dockerfile_info')
            LOG.info('create build_config dockerfile_info is: {}'.format(dockerfile_info))
            request.data['dockerfile'] = dockerfile_info.get('uuid')

        try:
            build_config_serializer = BuildConfigSerializer(data=request.data)
            build_config_serializer.is_valid(raise_exception=True)
            build_config_serializer.save()
            auto_build_enabled = build_config_serializer.data['auto_build_enabled']

            self._fulfill_build_config_code(config_id, auto_build_enabled, code_repo, namespace)
            build_config_code_serializer = BuildConfigCodeSerializer(data=code_repo)
            build_config_code_serializer.is_valid(raise_exception=True)
            build_config_code_serializer.save()

            if Weblab.CI_INTEGRATION_SONARQUBE(namespace):
                sonarqube = self._fulfill_build_config_integration(config_id, request.data)
                if sonarqube:
                    build_config_integration_serializer = BuildConfigIntegrationSerializer(data=sonarqube)
                    build_config_integration_serializer.is_valid(raise_exception=True)
                    build_config_integration_serializer.save()

        except:
            if request.data.get('schedule_rule'):
                MedusaClient.delete_schedule_event_config(request.data['schedule_config_id'])
            raise

        return Response({'config_id': request.data['config_id']}, status=status.HTTP_201_CREATED)

    def check_image_tag_rule_is_valid(self, image_tag_rule, code_repo_client):
        pattern = re.compile("(?=({[^{,}]+}))")
        rules = pattern.findall(image_tag_rule)
        rules = [rule[1:len(rule) - 1] for rule in rules]
        if code_repo_client == 'SIMPLE_SVN':
            tag_rule = settings.SVN_IMAGE_TAG_RULE
        else:
            tag_rule = settings.GIT_IMAGE_TAG_RULE
        for rule_item in rules:
            if rule_item not in tag_rule:
                return False
        return True

    def get_schedule_build_url(self, build_config_id):
        return '#ALAUDA_JAKIRO_ENDPOINT#/schedule_callback/builds/?build_config_id={}'.format(
            build_config_id)

    @tracing_response_time()
    @transaction.atomic
    def update(self, request, private_build_config_id):
        if not request.data.get('build_image_enabled', True):
            self._check_image_settings(request)
        code_repo = request.data.pop('code_repo')
        namespace = request.data.get('namespace')

        # if has custom image_tag rule
        if request.data.get('auto_tag_type') == 'CUSTOM_RULE':
            image_tag_rule = request.data.get('customize_tag_rule')
            if not self.check_image_tag_rule_is_valid(image_tag_rule,
                                                      code_repo.get('code_repo_client')):
                raise DockerBuildException('invalid_image_tag_rule')

        # if set wildcard, delete schedule rule
        if BuildConfigCode.is_wildcard_of_repo_type_value(code_repo.get("code_repo_type_value")):
            if request.data.get('schedule_rule'):
                LOG.info("deleting schedule_rule , because using wildcard in branch")
                request.data.pop('schedule_rule')

        build_config = get_object_or_404(self.build_config_query_set,
                                         config_id=private_build_config_id)

        action = ''
        schedule_config_id = build_config.schedule_config_id
        schedule_config_secret_key = build_config.schedule_config_secret_key
        if schedule_config_id or request.data.get('schedule_rule'):
            if not schedule_config_id:
                action = 'create'
                rule = request.data.get('schedule_rule')
                build_callback_url = self.get_schedule_build_url(private_build_config_id)
                result = MedusaClient.create_schedule_event_config(rule, build_callback_url)
                request.data['schedule_config_id'] = result.get('id')
                request.data['schedule_config_secret_key'] = result.get('secret_key')
            elif not request.data.get('schedule_rule'):
                action = 'delete'
                request.data['schedule_config_id'] = ''
                request.data['schedule_config_secret_key'] = ''
            else:
                action = 'update'
                request.data['schedule_config_id'] = schedule_config_id
                request.data['schedule_config_secret_key'] = schedule_config_secret_key

        sonar_weblab = Weblab.CI_INTEGRATION_SONARQUBE(namespace)
        if sonar_weblab:
            integration_action, integration_serializer = self._fulfill_build_config_integration_update(
                request.data, private_build_config_id, namespace)

        # if has dockerfile info
        if request.data.get('dockerfile_info'):
            dockerfile_info = request.data.pop('dockerfile_info')
            LOG.info('update build_config dockerfile_info is: {}'.format(dockerfile_info))
            request.data['dockerfile'] = dockerfile_info.get('uuid')
        else:
            request.data['dockerfile'] = None

        build_config_serializer = BuildConfigForUpdateSerializer(build_config, data=request.data)
        build_config_code = get_object_or_404(self.build_config_code_query_set,
                                              build_config_id=private_build_config_id)
        build_config_code_serializer = BuildConfigCodeForUpdateSerializer(build_config_code,
                                                                          data=code_repo)
        try:
            build_config_serializer.is_valid(raise_exception=True)
            build_config_code_serializer.is_valid(raise_exception=True)
            if sonar_weblab:
                if integration_action == "create" or integration_action == "update":
                    integration_serializer.is_valid(raise_exception=True)
        except:
            if action == 'create':
                MedusaClient.delete_schedule_event_config(request.data['schedule_config_id'])
            raise

        build_config_serializer.save()
        build_config_code_serializer.save()
        if sonar_weblab:
            if integration_action == "delete":
                self.build_config_integration_query_set.filter(
                    build_config_id=private_build_config_id,
                    type=INTEGRATION_SONARQUBE).delete()
                self._check_delete_sonar_project_webhook(integration_serializer.data, False)
            elif integration_action == "update" or integration_action == "create":
                integration_serializer.save()

        if action == 'delete':
            MedusaClient.delete_schedule_event_config(schedule_config_id)
        elif action == 'update':
            MedusaClient.update_schedule_event_config(
                request.data.get('schedule_rule'), schedule_config_id)

        return Response(status=status.HTTP_204_NO_CONTENT)

    def _fulfill_build_config_integration_update(self, data, private_build_config_id, namespace):
        sonarqube = data.get("sonarqube")
        db_sonarqubes_query = self.build_config_integration_query_set.filter(
            type=INTEGRATION_SONARQUBE, build_config_id=private_build_config_id)
        if db_sonarqubes_query.exists():
            db_sonarqube =db_sonarqubes_query.get()
            if not sonarqube:
                integration_action = "delete"
                build_config_integration_serializer = BuildConfigIntegrationSerializer(
                    db_sonarqube)
            else:
                integration_action = "update"
                sonarqube['build_integration_config']['project'] = \
                    db_sonarqube.build_integration_config.get("project")
                sonarqube['build_integration_config']['project_key'] = \
                    db_sonarqube.build_integration_config.get("project_key")
                build_config_integration_serializer = BuildConfigIntegrationForUpdateSerializer(
                    db_sonarqube, data=sonarqube)
        else:
            if not sonarqube:
                integration_action = "nothing"
                build_config_integration_serializer = None
            else:
                integration_action = "create"
                sonarqube["type"] = INTEGRATION_SONARQUBE
                sonarqube["build_config_id"] = private_build_config_id
                if not sonarqube.get("build_integration_config"):
                    sonarqube["build_integration_config"] = {}
                # using config name as project name
                sonarqube["build_integration_config"]["project"] = namespace+":"+data.get('name')
                sonarqube["build_integration_config"]["project_key"] = private_build_config_id
                build_config_integration_serializer = BuildConfigIntegrationSerializer(data=sonarqube)
        return integration_action, build_config_integration_serializer

    @tracing_response_time()
    def delete(self, request, private_build_config_id):
        namespace = request.data.get('namespace')
        build_config = get_object_or_404(self.build_config_query_set,
                                         config_id=private_build_config_id)

        if build_config.schedule_config_id:
            MedusaClient.delete_schedule_event_config(build_config.schedule_config_id)

        build_code = get_object_or_404(self.build_config_code_query_set,
                                       build_config_id=private_build_config_id)
        code_repo_webhook_id = build_code.code_repo_webhook_id
        code_repo_key_id = build_code.code_repo_key_id
        repo_id = build_code.repo_id

        sonar_int_weblab = Weblab.CI_INTEGRATION_SONARQUBE(namespace)
        sonar_integration = None
        if sonar_int_weblab:
            sonar_query = self.build_config_integration_query_set.filter(
                build_config_id=build_config.config_id,
                type=INTEGRATION_SONARQUBE
            )
            if sonar_query.exists():
                sonar_integration = sonar_query.get()

        build_config.delete()

        # delete webhook and deploy key
        code_repo_client = CodeClientFactory.create_code_client(
            build_code.code_repo_client, namespace)
        try:
            code_repo_client.delete_deploy_key(
                build_code.code_repo_path, code_repo_key_id, repo_id)
            code_repo_client.delete_webhook(
                build_code.code_repo_path, code_repo_webhook_id, repo_id)
        except:
            LOG.error("delete deploy key and webhook failed")
       
        if sonar_int_weblab:
            if sonar_integration:
                self._check_delete_sonar_project_webhook(
                    BuildConfigIntegrationSerializer(sonar_integration).data, False)
        with transaction.atomic():
            RazzilUtil.handle_build_list(build_config)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def _fulfill_build_config_code(self, config_id, auto_build_enabled, code_repo, namespace):
        code_repo['build_config'] = config_id
        repo_id = code_repo.get('repo_id', '')
        code_repo_client = CodeClientFactory.create_code_client(
            code_repo['code_repo_client'], namespace)

        deploy_key = code_repo_client.create_deploy_key(
            code_repo['code_repo_path'], repo_id)
        code_repo['code_repo_key_id'] = deploy_key['id']
        code_repo['code_repo_private_key'] = deploy_key['private_key']
        code_repo['code_repo_public_key'] = deploy_key['public_key']
        # always generate the web code, even customer didn't enable
        # auto build
        webhook = code_repo_client.create_webhook(
            code_repo['code_repo_path'], namespace, repo_id)
        code_repo['code_repo_webhook_id'] = webhook['id']
        code_repo['code_repo_webhook_code'] = webhook['code']
    
    def _fulfill_build_config_integration(self, config_id, data):
        sonarqube = data.get("sonarqube")
        if sonarqube:
            sonarqube["type"] = INTEGRATION_SONARQUBE
            sonarqube["build_config_id"] = config_id
            if not sonarqube.get("build_integration_config"):
                sonarqube["build_integration_config"] = {}
            # using config name as project name
            sonarqube["build_integration_config"]["project"] = data.get("namespace")+":"+data.get('name')
            sonarqube["build_integration_config"]["project_key"] = config_id
            return sonarqube
    
    def _check_delete_sonar_project_webhook(self, sonarqube_integraion, raise_exception):
        try:
            sonarqube = DavionClient.get_integration(sonarqube_integraion.get("integration_instance_id"))
            sonar_endpoint = sonarqube["fields"].get('endpoint')
            sonar_token = sonarqube["fields"].get('token')
            sonar_username = sonarqube["fields"].get('username')
            sonar_password = sonarqube["fields"].get('password')
            client = SonarClient(endpoint=sonar_endpoint, token=sonar_token,
                username=sonar_username, password=sonar_password)
            project_key = sonarqube_integraion.get("build_integration_config").get("project_key")
            if project_key:
                client.remove_project_webhook(project_key)
            LOG.info("remove sonar project webhook, key is " + project_key)
        except Exception as ex:
            LOG.warn("remove sonar project webhook error: \n{}".format(traceback.format_exc()))
            if raise_exception:
                raise ex
        return

    # get config_list without webhook from redis
    @tracing_response_time()
    def config_list_without_webhook(self, request):
        redis_client = RedisClientFactory.get_client_by_key(settings.REDIS_CLIENT_KEY)
        result = redis_client.hgetall(SVN_KEY)
        if not result:
            LOG.info('get no repo without webhook from cache, will get from db')
            queryset = BuildConfigCode.objects.filter(
                Q(code_repo_client__in=CODE_REPO_WITHOUT_WEBHOOK) &
                Q(build_config__auto_build_enabled=True))

            if queryset:
                result = AlaudaCache.save_config(queryset=queryset)

        return Response(result)

    @tracing_response_time()
    def preview_alaudaci_yml(self, request):
        pre_ci_boot = {}
        ci = []
        sonarqube = {}
        ci_envs = request.data.get('ci_envs')
        ci_steps = request.data.get('ci_steps')
        ci_imgs = CIEnvsCatalog.objects.get_ci_images(ci_envs)
        if len(ci_imgs) <= 0:
            raise DockerBuildException('invalid_args', "not found images for {}".format(
                ci_envs
            ))
        if ci_envs:
            pre_ci_boot['image'] = ci_imgs[0]['image']
            pre_ci_boot['tag'] = ci_imgs[0]['tag']
        if ci_steps:
            for step in ci_steps:
                ci.append(step)

        if request.data.get('sonarqube'):
            sonarqube_data = request.data.get('sonarqube')
            sonarqube["integration_name"] = sonarqube_data.get("integration_instance_id")
            # will using when we support name
            # if sonarqube_data.get("integration_instance_id"):
            #     res = JakiroInternalClient.get_resources(
            #         sonarqube_data.get("integration_instance_id"))
            #     sonarqube["integration_name"] = res["name"]
            sonar_config = sonarqube_data['build_integration_config']
            def set_val(field):
                sonarqube[field] = ""
                if sonar_config.get(field):
                    sonarqube[field] = sonar_config.get(field, "")
            set_val("code_encoding")
            set_val("code_language")
            set_val("quality_gate")
            set_val("code_scan_path")
        # simple to control the order of key
        arr =[
            yaml.safe_dump({"version": ALAUDACI_YML_VERSION},
                default_flow_style=False, allow_unicode=True),
            yaml.safe_dump({"pre_ci_boot": pre_ci_boot},
                default_flow_style=False, allow_unicode=True),
            yaml.safe_dump({"ci": ci}, default_flow_style=False,
                allow_unicode=True)
        ]

        if sonarqube:
            arr.append(yaml.safe_dump({"sonarqube": sonarqube}, default_flow_style=False,
                allow_unicode=True))

        return Response(data={"yml":string.join(arr, "")}, status=status.HTTP_200_OK)

    @tracing_response_time()
    def parse_auto_build(self, request):
        web_hook_data = RazzilUtil.parse_and_fufill_web_hook_data(request.data)
        return Response({
            "code_head_commit": web_hook_data.get_code_head_commit(),
            "code_repo_type_value": web_hook_data.get_code_repo_type_value()
        })

    def _get_config_capability(self, build_config):
        return {
            "config_id": build_config.config_id,
            "code_cache_enabled": build_config.code_cache_enabled,
            "artifact_upload_enabled": build_config.artifact_upload_enabled,
            "allow_artifact_upload_fail": build_config.allow_artifact_upload_fail,
            "auto_build_enabled": build_config.auto_build_enabled,
            "ci_enabled": build_config.ci_enabled,
            "build_image_enabled": build_config.build_image_enabled,
            "image_cache_enabled": build_config.image_cache_enabled,
            "sonarqube_enabled": False
        }

    def _check_image_settings(self, request):
        image_settings = [
            'customize_tag', 'image_repo_id', 'customize_tag_rule'
        ]
        for item in image_settings:
            if item in request.data:
                request.data[item] = ''
        if 'auto_tag_type' in request.data:
            request.data['auto_tag_type'] = 'NONE'
