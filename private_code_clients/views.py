import logging

from rest_framework import status, viewsets
from rest_framework.response import Response

from commons.code_client.code_client_factory import CodeClientFactory
from commons.docker_build_exception import DockerBuildException
from private_build_configs.models import CODE_REPO_CLIENT_LIST, CODE_REPO_CLIENT_ALL_LIST
from private_code_clients.models import CodeClientUser
from private_code_clients.serializers import CodeClientUserSerializer
from alauda.tracing import tracing_response_time
from django.conf import settings


LOG = logging.getLogger(__name__)


class PrivateBuildCodesViewset(viewsets.ViewSet):
    query_set_code_client_user = CodeClientUser.objects.all()

    @tracing_response_time()
    def list_supported_code_client(self, request, user_name):
        # get authed code client list
        query_set_code_client_user_list = self.query_set_code_client_user.filter(
            user_name=user_name)
        authed_code_client_user_list = CodeClientUserSerializer(
            query_set_code_client_user_list, many=True)
        # parse the result from arry to map
        authed_code_client_user_map = {element['code_repo_client']: element
                                       for element in authed_code_client_user_list.data}

        # generate the result
        result = []
        code_clients = [client
                        for client in settings.CUSTOM_CODE_REPO_CLIENT_LIST.split(',')
                        if client in CODE_REPO_CLIENT_LIST
                       ]
        for code_client_name in code_clients:
            authed_code_client = authed_code_client_user_map.get(code_client_name)
            # when the client is authed
            if authed_code_client:
                is_authed = True
            else:
                is_authed = False

            code_client_info = {}
            code_client_info['name'] = code_client_name
            code_client_info['is_authed'] = is_authed
            result.append(code_client_info)

        return Response(result, status=status.HTTP_200_OK)

    @tracing_response_time()
    def add_code_client(self, request, user_name):
        code_client_name = request.data['code_client_name']
        is_authed = self.query_set_code_client_user.filter(
            user_name=user_name, code_repo_client=code_client_name).exists()
        if is_authed:
            raise DockerBuildException('is_already_authed')
        code_client = CodeClientFactory.create_code_client(code_client_name)
        state = request.data['state']
        data = {
            "code_repo_client": code_client_name,
            "user_name": state
        }
        access_token, refresh_token = code_client.get_access_token(request.data)
        data['access_token'] = access_token
        if refresh_token:
            data['refresh_token'] = refresh_token
        code_client.set_code_client_user(data)
        data['code_repo_client_user_name'] = code_client.get_client_login(user_name)

        code_client_user_serializer = CodeClientUserSerializer(data=data)
        code_client_user_serializer.is_valid(raise_exception=True)
        code_client_user_serializer.save()

        return Response({"success": True}, status=status.HTTP_201_CREATED)

    @tracing_response_time()
    def retrieve_code_client(self, request, user_name, code_client_name):
        code_client = CodeClientFactory.create_code_client(code_client_name, user_name)
        org_list = code_client.get_client_orgs(**request.query_params)
        return Response(org_list)

    @tracing_response_time()
    def delete_code_client(self, request, user_name, code_client_name):
        self.query_set_code_client_user.get(
            code_repo_client=code_client_name, user_name=user_name).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @tracing_response_time()
    def retrieve_auth_url(self, request, user_name, code_client_name):
        code_client = CodeClientFactory.create_code_client(code_client_name)
        auth_url = code_client.get_auth_url(user_name)
        result = {
            'auth_url': auth_url
        }
        return Response(result)

    @tracing_response_time()
    def list_code_repo(self, request, user_name, code_client_name, code_client_org):
        code_client = CodeClientFactory.create_code_client(
            code_client_name, user_name)
        repo_list = code_client.get_repo_list(code_client_org, **request.query_params)
        return Response(repo_list)

    @tracing_response_time()
    def create_repo_webhook(self, request, user_name, code_client_name, code_client_org, repo):
        code_client = CodeClientFactory.create_code_client(
            code_client_name, user_name)
        hook_url = request.data.get('hook_url', '')
        if len(hook_url) == 0:
            return Response({'error': 'hook_url needed'}, status=status.HTTP_400_BAD_REQUEST)
        repo_path = '{}/{}'.format(code_client_org, repo)
        result = code_client.create_repo_webhook(repo_path, hook_url)
        hook_id = str(result.get('id') or result.get('uuid'))
        return Response({'hook_id': str(hook_id)})

    @tracing_response_time()
    def delete_repo_webhook(self, request, user_name, code_client_name, code_client_org, repo):
        code_client = CodeClientFactory.create_code_client(
            code_client_name, user_name)
        hook_id = request.query_params.get('hook_id', '')
        if len(hook_id) == 0:
            return Response({'error': 'query paramter hook_id needed'}, status=status.HTTP_400_BAD_REQUEST)
        repo_path = '{}/{}'.format(code_client_org, repo)
        result = code_client.delete_repo_webhook(repo_path, hook_id)

        return Response(result)

    @tracing_response_time()
    def parse_webhook_payload(self, request):
        code_repo_client = request.data.get("code_repo_client")
        payload = request.data.get("payload")
        if(code_repo_client not in CODE_REPO_CLIENT_ALL_LIST):
            raise DockerBuildException("invalid_args","code_repo_client is invalid")
        if not payload:
            raise DockerBuildException("invalid_args","payload is required")
        code_client = CodeClientFactory.create_code_client(code_repo_client)
        res = code_client.parse_web_hook_payload(payload)
        return Response({
            "code_repo_client": code_repo_client,
            "code_repo_type": res.code_repo_type,
            "code_repo_type_value": res.code_repo_type_value,
            "code_head_commit": res.code_head_commit
        })

    @tracing_response_time()
    def create_repo_deploy_key(self, request, user_name, code_client_name, code_client_org, repo):
        code_client = CodeClientFactory.create_code_client(
            code_client_name, user_name)
        title = request.data.get('title', '')
        if len(title) == 0:
            raise DockerBuildException("invalid_args","title is required")
        repo_path = '{}/{}'.format(code_client_org, repo)
        result = code_client.create_deploy_key(repo_path, repo_path, title)
        return Response(result, status=status.HTTP_201_CREATED)

    @tracing_response_time()
    def delete_repo_deploy_key(self, request, user_name, code_client_name, code_client_org, repo, key_id):
        code_client = CodeClientFactory.create_code_client(
            code_client_name, user_name)
        repo_path = '{}/{}'.format(code_client_org, repo)
        code_client.delete_deploy_key(repo_path, key_id, repo_path)

        return Response(status=status.HTTP_204_NO_CONTENT)
