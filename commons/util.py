import logging
import time
import exceptions
import uuid
import json
import traceback
import datetime
import pytz
import re

from django.conf import settings
from django.utils import timezone
from mekansm.utils import convert_to_unicode, is_string

from ci_envs.models import CIEnvsCatalog
from private_builds.build_client.tresdin_client import TresdinClient
from private_builds.models import (Build, BuildEndpoint,
                                   STATUS_WAITING, STATUS_FAILED,
                                   COMPLETE_STATUS, BuildIntegration, STEP_NEW_RUN_SONARSCANNER)
from private_builds.serializers import (BuildIncludeCodeSerializer,
                                        BuildSerializer,
                                        BuildIntegrationSerializer,
                                        BuildIntegrationForUpdateSerializer)
from private_build_configs.models import (CODE_REPO_CLIENT_SIMPLE_GIT,
                                          CODE_REPO_CLIENT_SIMPLE_SVN,
                                          BuildConfigCode, BuildConfig,
                                          BuildConfigIntegration,
                                          INTEGRATION_SONARQUBE)
from private_build_configs.serializers import (
                BuildConfigIntegrationSerializer,
                BuildConfigSerializer)
from .code_client.code_client_factory import CodeClientFactory
from .docker_build_exception import DockerBuildException
from commons.medusa_client import MedusaClient
from commons.weblab import Weblab
from commons.davion_client import DavionClient
from commons.sonarqube_client import SonarClient
from commons.jakiro_client import JakiroInternalClient
from commons.furion_client import FurionUtil

LOG = logging.getLogger(__name__)
TIMEOUT = settings.DOCKER_BUILD_TIMEOUT * 60
STATUS_SYNC_RULE = '*/5 * * * *'
SONAR_STATUS_SYNC_RULE = '*/3 * * * *'
CI_MEMORY_RE = r"^[1-9][0-9]*[mMgG]"


class RazzilUtil(object):
    @staticmethod
    def handle_build_list(build_data):
        if isinstance(build_data, Build):
            build_data = BuildSerializer(build_data).data
        elif isinstance(build_data, BuildConfig):
            build_data = BuildConfigSerializer(build_data).data
        if 'config' in build_data and build_data['config']:
            build_data['config_id'] = build_data['config']
        filter_field = {'image_repo_id': build_data['image_repo_id']} \
            if build_data.get('build_image_enabled', True) else {'config__config_id': build_data['config_id']}
        build_list = RazzilUtil.get_not_finished_builds(filter_field)
        build_list.reverse()
        RazzilUtil._handle_not_finished_builds(build_list)

    @staticmethod
    def get_not_finished_builds(filter_field):
        query_set = Build.objects.all().filter(
            **filter_field).exclude(status__in=COMPLETE_STATUS)
        build_serializer = BuildIncludeCodeSerializer(query_set, many=True)
        return build_serializer.data

    # check whether there are some in progress builds what are timeout, and change
    # their status to failed, change the bocked status to waiting and send to
    # the gundar.
    @staticmethod
    def _handle_not_finished_builds(build_list):
        """
        Check build status,start first on if avaliable.

        :param build_list: list of build object
        :type build_list: list
        """
        is_removed_timeout_build = False
        blocked_build_list = []
        in_progress_build_list = []
        for build in build_list:
            if Build.is_in_progress_status(build['status']) or \
                    Build.is_waiting_status(build['status']):
                if RazzilUtil._remove_timeout_build(build):
                    is_removed_timeout_build = True
                else:
                    in_progress_build_list.append(build)
            elif Build.is_blocked_status(build['status']):
                blocked_build_list.append(build)

        if (is_removed_timeout_build or not in_progress_build_list) and blocked_build_list:
            RazzilUtil._start_build_now(blocked_build_list[0])

    @staticmethod
    def _start_build_now(build):
        """
        Start build.

        :param build: build object
        :type build: Build object
        """
        sonar_weblab = Weblab.CI_INTEGRATION_SONARQUBE(build['namespace'])
        try:
            RazzilUtil._send_build_to_gundar(build)
        except Exception as ex:
            db_build = Build.objects.all().get(build_id=build['build_id'])
            db_build.status = STATUS_FAILED
            db_build.save()
            if sonar_weblab:
                sonarqube_integration = RazzilUtil._get_sonarqube_build_integration(build['build_id'])
                if sonarqube_integration:
                    RazzilUtil._update_sonarqube_build_integration_fail(
                        sonarqube_integration, "Send Job Error")
            LOG.error(
                "send build job to gundar error build_id = {}, error = {}".format
                (build['build_id'], traceback.format_exc()))
            RazzilUtil.handle_build_list(build)
            return

        db_build = Build.objects.all().get(build_id=build['build_id'])
        db_build.status = STATUS_WAITING
        db_build.started_at = timezone.now()
        db_build.status_sync_schedule_id = str(uuid.uuid4())
        db_build.save()
        if sonar_weblab:
            sonarqube_integration = RazzilUtil._get_sonarqube_build_integration(build['build_id'])
            if sonarqube_integration and sonarqube_integration.execution_status != STATUS_FAILED:
                sonarqube_integration.execution_status = STATUS_WAITING
                sonarqube_integration.save()
        try:
            RazzilUtil._add_status_sync_scheduler(
                build['build_id'],
                db_build.status_sync_schedule_id)
        except Exception as ex:
            LOG.error("add sync scheduler error ! build_id={} error ={}".format(
                build['build_id'],
                ex
            ))


    @staticmethod
    def _add_status_sync_scheduler(build_id, config_id):
        sync_url = RazzilUtil._get_build_status_sync_url(build_id)
        result = MedusaClient.create_schedule_event_config(
            STATUS_SYNC_RULE,
            sync_url,
            config_id)
        LOG.info("add sync scheduler to medusa, build_id ={}, id = {}".format(
            build_id,
            result.get('id')))

    @staticmethod
    def _add_sonar_status_sync_scheduler(build_id, config_id=None):
        sync_url = RazzilUtil._get_sonar_status_sync_url(build_id)
        result = MedusaClient.create_schedule_event_config(
            SONAR_STATUS_SYNC_RULE,
            sync_url,
            config_id)
        LOG.info("add sonar task sync scheduler to medusa, build_id {}, config id {}".format(
            build_id, config_id))

        return result


    @staticmethod
    def _get_build_status_sync_url(build_id):
        return '#ALAUDA_RAZZIL_ENDPOINT#/v1/private_builds/{0}/sync-status/'.format(build_id)

    @staticmethod
    def _get_sonar_status_sync_url(build_id):
        return '#ALAUDA_RAZZIL_ENDPOINT#/v1/private_builds/{0}/sync-sonar-task-status/'.format(build_id)

    @staticmethod
    def _send_build_to_gundar(build):
        if build.get('endpoint_id'):
            endpoint_query_set = BuildEndpoint.objects.filter(endpoint_id=build.get('endpoint_id'))
        else:
            endpoint_query_set = BuildEndpoint.objects.filter(namespace=build.get('namespace'))
        if not endpoint_query_set.exists():
            raise DockerBuildException('build_endpoint_not_exists')
        endpoint = endpoint_query_set.first()

        sonarqube_integration_data = None
        if Weblab.CI_INTEGRATION_SONARQUBE(build.get('namespace')):
            sonarqube_build_integration = RazzilUtil._get_sonarqube_build_integration(build['build_id'])
            sonarqube_integration_data, friendly_err = RazzilUtil._collect_sonarqube_build_integration_data(
                sonarqube_build_integration, build)
            if sonarqube_build_integration and friendly_err:
                # just set fail , not block the orthers
                LOG.error("sonar build integration, build_id={} error : {}".format(build['build_id'], friendly_err))
                RazzilUtil._update_sonarqube_build_integration_fail(sonarqube_build_integration, friendly_err)
        job_data = RazzilUtil._constract_job_data(build, endpoint, sonarqube_integration_data)
        TresdinClient.create(job_data)

    @staticmethod
    def _update_sonarqube_build_integration_fail(sonarqube_integration, err):
        sonarqube_integration.integration_execution["error"] = err
        # we should update instead of save
        # prevent `sonarqube_integration` keeping some overdue data
        BuildIntegration.objects.filter(
            type=INTEGRATION_SONARQUBE, build_id=sonarqube_integration.build_id
            ).exclude(execution_status__in=COMPLETE_STATUS).update(
                execution_status=STATUS_FAILED,
                ended_at=timezone.now(),
                integration_execution=sonarqube_integration.integration_execution
            )

    @staticmethod
    def check_to_set_sonarqube_fail(build_id, fail_on_step_at):
        # if fail before run sonar we should set sonar fail
        LOG.debug("checking sonarqube should be fail, build:{}, step_at:{}".format(
            build_id, fail_on_step_at
        ))
        if fail_on_step_at <= STEP_NEW_RUN_SONARSCANNER or fail_on_step_at is None:
            sonarqube_build_integration = RazzilUtil._get_sonarqube_build_integration(build_id)
            if not sonarqube_build_integration:
                return
            if sonarqube_build_integration.execution_status not in COMPLETE_STATUS:
                LOG.info("build:{} fail on step:{}, sonarqube never been scheduled, set to fail".format(
                    build_id, fail_on_step_at
                ))
                RazzilUtil._update_sonarqube_build_integration_fail(
                    sonarqube_build_integration, "Not Been Scheduled")

    @staticmethod
    def _get_sonarqube_build_integration(build_id):
        sonarqube_query = BuildIntegration.objects.filter(
            type=INTEGRATION_SONARQUBE, build_id=build_id)
        if not sonarqube_query.exists():
            return
        return sonarqube_query.get()

    @staticmethod
    def _collect_sonarqube_build_integration_data(sonarqube_integration, build):
        if not sonarqube_integration:
            return None, ""
        data = {}
        try:
            integration = DavionClient.get_integration(
                sonarqube_integration.integration_instance_id)
        except Exception as ex:
            LOG.error("get integration from davion client error : \n{}".format(traceback.format_exc()))
            err = "Get integration info error"
            return None, err
        if not integration.get("enabled"):
            LOG.warning("SonarQube {} is inactive".format(
                sonarqube_integration.integration_instance_id))
            return None, "SonarQube Integration is inactive"
        endpoint = integration['fields'].get('endpoint')
        if not endpoint:
            return None, "SonarQube Endpoint is Required"
        data['sonar_endpoint'] = endpoint.rstrip("/")
        data['login_token'] = integration['fields'].get('token')
        data['username'] = integration['fields'].get('username')
        data['password'] = integration['fields'].get('password')
        data['code_scan_path'] = sonarqube_integration.build_integration_config.get(
                                'code_scan_path')
        data["code_encoding"] = sonarqube_integration.build_integration_config.get(
                                'code_encoding')
        data["project_key"] = sonarqube_integration.build_integration_config['project_key']
        data["project"] = sonarqube_integration.build_integration_config['project']
        data['code_language'] = sonarqube_integration.build_integration_config.get(
                                'code_language')
        data['quality_gate'] = sonarqube_integration.build_integration_config.get(
                                'quality_gate')
        return data, None

    @staticmethod
    def new_sonar_client(sonarqube_integration_config):
        endpoint = sonarqube_integration_config['fields'].get('endpoint')
        username = sonarqube_integration_config['fields'].get('username')
        password = sonarqube_integration_config['fields'].get('password')
        token = sonarqube_integration_config['fields'].get('token')
        return SonarClient(endpoint=endpoint, token=token,
                    username=username, password=password)

    @staticmethod
    def _constract_job_data(build, endpoint, sonarqube_integration_data=None):
        build_data = RazzilUtil._constract_build_data(build, endpoint)
        RazzilUtil._append_sonarqube_data(sonarqube_integration_data, build_data)
        job_data = {
            'name': 'private_build_' + build['build_id'],
            'image': endpoint.builder_image,
            'command': '/bin/bash -c /worker.sh',
            'cpu': build['cpu'],
            'memory': build['memory'],
            'network': 'HOST',
            'environment_variables': RazzilUtil._build_data_to_envvars(build_data),
            'volumes': [
                {
                    'container_path': '/var/run/docker.sock',
                    'host_path': '/var/run/docker.sock',
                    'mode': 'RO'
                },
                {
                    'container_path': '/var/log/mathilde',
                    'host_path': '/var/log/mathilde',
                    'mode': 'RW'
                },
                {
                    'container_path': '/var/alaudaci/{}'.format(build['build_id']),
                    'host_path': '/var/alaudaci/{}'.format(build['build_id']),
                    'mode': 'RW'
                }
            ]
        }
        if not endpoint.is_public:
            job_data['region_id'] = endpoint.region_id
        return job_data

    @staticmethod
    def _build_data_to_envvars(build_data):
        return [
            {'name': k.upper(), 'value': v}
            for k, v in RazzilUtil.format_dict_to_unicode(build_data).items()
        ]

    @staticmethod
    def _constract_build_data(build, endpoint):
        code_repo = build['code_repo']
        code_repo_client = CodeClientFactory.create_code_client(code_repo['code_repo_client'])

        build_data = {
            'debug': settings.DEBUG,
            'timezone': settings.CUR_TIME_ZONE,
            'jakiro_api_endpoint': settings.JAKIRO_API_ENDPOINT,
            'jakiro_api_version': settings.JAKIRO_API_VERSION,
            'oss_endpoint': settings.OSS_ENDPOINT,
            'namespace': build['namespace'],
            'username': build['created_by'],
            'user_token': build['user_token'],
            'build_id': build['build_id'],
            'build_image_enabled': build.get('build_image_enabled'),
            'build_config_id': build['config'],
            'build_enabled': build.get('build_enabled'),
            'build_cache_enabled': build.get('build_cache_enabled'),
            'code_repo_path': code_repo_client.get_full_path(code_repo.get('code_repo_path'),
                                                             code_repo.get('code_repo_username'),
                                                             code_repo.get('code_repo_password')),
            'code_repo_client': code_repo.get('code_repo_client'),
            'code_repo_type': code_repo.get('code_repo_type'),
            'code_repo_type_value': code_repo.get('code_repo_type_value'),
            'code_repo_private_key': code_repo.get('code_repo_private_key'),
            'code_head_commit': code_repo.get('code_head_commit'),
            'ci_worker_host_mode': settings.CI_WORKER_HOST_MODE,
            'svn_trust_server_cert_failures': settings.SVN_TRUST_SERVER_CERT_FAILURES,
            'ci_enabled': build.get('ci_enabled'),
            'ci_config_file_location': build.get('ci_config_file_location'),
            'artifact_upload_enabled': build.get('artifact_upload_enabled'),
            'allow_artifact_upload_fail': build.get('allow_artifact_upload_fail'),
            'dockerfile_location': code_repo.get('dockerfile_location', '/'),
            'build_context_path': code_repo.get('build_context_path'),
            'docker_repo_path': build.get('docker_repo_path'),
            'docker_repo_tag': build.get('docker_repo_tag'),
            'docker_repo_version_tag': build.get('docker_repo_version_tag'),
            'auto_tag_type': build.get('auto_tag_type'),
            'customize_tag_rule': build.get('customize_tag_rule'),
            'registry_index': build.get('registry_index'),
            'exec_cmd_enabled': build.get('exec_cmd_enabled'),
            'extra_dockerfile_enabled': build.get('extra_dockerfile_enabled'),
            'ci_report_enabled': settings.CI_REPORT_ENABLED
        }

        if settings.CUSTOM_ENVVAR_PREFIX is not None:
            build_data['custom_envvar_prefix'] = settings.CUSTOM_ENVVAR_PREFIX
        if build['ci_enabled'] and build.get('ci_envs'):
            if build.get('ci_image_name') and build.get('ci_image_tag'):
                build_data['ci_image_name'] = build['ci_image_name']
                build_data['ci_image_tag'] = build['ci_image_tag']
            else:
                ci_envs = CIEnvsCatalog.objects.get_ci_images(build['ci_envs'])
                if ci_envs:
                    build_data['ci_image_name'] = ci_envs[0]['image']
                    build_data['ci_image_tag'] = ci_envs[0]['tag']
        if endpoint.proxy_config and code_repo['code_repo_client'] != CODE_REPO_CLIENT_SIMPLE_SVN:
            proxy_config = endpoint.proxy_config
            build_data['git_proxy_enabled'] = True
            build_data['git_proxy_type'] = proxy_config['type']
            build_data['git_proxy_host'] = proxy_config['host']
            build_data['git_proxy_port'] = proxy_config['port']
            build_data['git_proxy_socks5_encrypt_method'] = proxy_config['encrypt_method']
            build_data['git_proxy_socks5_password'] = proxy_config['password']
        # checkout ci resource env
        if settings.CI_MEMORY:
            pattern = re.compile(CI_MEMORY_RE)
            if pattern.match(settings.CI_MEMORY):
                build_data["ci_memory"] = settings.CI_MEMORY
            else:
                LOG.error("env CI_MEMORY is illegal: {}".format(settings.CI_MEMORY))
        if settings.CI_CPU:
            ci_cpu = int(settings.CI_CPU)
            if ci_cpu in range(1, 16):
                build_data["ci_cpu"] = settings.CI_CPU
            else:
                LOG.error("env CI_CPU is illegal: {}".format(settings.CI_CPU))
        build_data['alauda_sonarscanner_image'] = settings.ALAUDA_SONARSCANNER_IMAGE

        try:
            res = JakiroInternalClient.get_resources(build['config'])
            build_data['build_config_name'] = res["name"]
        except:
            LOG.error("can not get build config name for build_config:{}, Exception:{}".format(
                build['config'], traceback.format_exc()))
        return build_data

    @staticmethod
    def _append_sonarqube_data(sonarqube_integration_data, build_data):
        if not sonarqube_integration_data:
            build_data['sonarqube_enabled'] = False
            return
        build_data['sonarqube_enabled'] = True
        prefix = "sonarqube_"
        build_data[prefix+'sonar_endpoint'] = sonarqube_integration_data['sonar_endpoint']
        build_data[prefix+'login_token'] = sonarqube_integration_data['login_token']
        build_data[prefix+'sonar_username'] = sonarqube_integration_data['username']
        build_data[prefix+'sonar_password'] = sonarqube_integration_data['password']
        build_data[prefix+'code_scan_path'] = sonarqube_integration_data['code_scan_path']
        build_data[prefix+'code_encoding'] = sonarqube_integration_data['code_encoding']
        build_data[prefix+'project_key'] = sonarqube_integration_data['project_key']
        build_data[prefix+'project'] = sonarqube_integration_data['project']
        build_data[prefix+'code_language'] = sonarqube_integration_data['code_language']
        build_data[prefix+'quality_gate'] = sonarqube_integration_data['quality_gate']
        build_data[prefix+"scan_timeout"] = settings.SONARQUBE_SCAN_TIMEOUT

    @staticmethod
    def _remove_timeout_build(build):
        # for bad data build stop it.
        if not build.get('started_at'):
            LOG.info("build {} is timeout, stop it".format(build["build_id"]))
            RazzilUtil._stop_time_out_build(build)
            return True

        started_time = int(time.mktime(
            time.strptime(build['started_at'], "%Y-%m-%dT%H:%M:%S.%fZ")))
        seconds = time.time() - started_time
        if seconds > TIMEOUT:
            LOG.info("build {} is timeout, stop it".format(build["build_id"]))
            RazzilUtil._stop_time_out_build(build)
            return True
        else:
            LOG.info("build {} is not timeout".format(build["build_id"]))
            return False

    @staticmethod
    def _stop_time_out_build(build):
        Build.objects.all().filter(build_id=build['build_id']) \
            .update(status=STATUS_FAILED, ended_at=timezone.now())
        RazzilUtil.delete_build_to_gundar(build['namespace'],
                                          build['build_id'],
                                          build['endpoint_id'])

    @staticmethod
    def delete_in_progress_build_to_gundar(status, namespace, build_id, endpoint_id):
        if not Build.need_call_gundar_delete(status):
            return True
        RazzilUtil.delete_build_to_gundar(namespace, build_id, endpoint_id)

    @staticmethod
    def delete_build_to_gundar(namespace, build_id, endpoint_id):
        endpoint = RazzilUtil.get_endpoint(namespace, endpoint_id)
        params = {}
        if not endpoint.is_public:
            params['region_id'] = endpoint.region_id
        try:
            TresdinClient.delete('private_build_' + build_id, params=params)
        except Exception as ex:
            LOG.error("delete build failed build_id:{}, reason:{}".format(build_id, ex))

    @staticmethod
    def get_endpoint(namespace=None, endpoint_id=None):
        if endpoint_id:
            query_set = BuildEndpoint.objects.filter(endpoint_id=endpoint_id)
        else:
            query_set = BuildEndpoint.objects.filter(namespace=namespace)
        return query_set.first()

    @staticmethod
    def is_need_username_and_password(code_repo_client, code_repo_path):
        if code_repo_client not in [CODE_REPO_CLIENT_SIMPLE_GIT, CODE_REPO_CLIENT_SIMPLE_SVN]:
            return False

        if not (
                    RazzilUtil.is_http_protocol(code_repo_path) or (
                        RazzilUtil.is_svn_protocol(code_repo_path))
        ):
            return False

        return True

    @staticmethod
    def is_http_protocol(code_repo_path):
        return code_repo_path.lower().startswith('http')

    @staticmethod
    def is_svn_protocol(code_repo_path):
        return code_repo_path.lower().startswith('svn:')

    @staticmethod
    def is_svn_and_ssh_protocol(code_repo_path):
        return code_repo_path.lower().startswith('svn') and \
               not RazzilUtil.is_svn_protocol(code_repo_path)

    @staticmethod
    def parse_and_fufill_web_hook_data(data):
        LOG.debug("auto webhook payload: {}".format(data.get('external_data')))

        code_client = CodeClientFactory.create_code_client(
            data['code_repo']['code_repo_client'])

        web_hook_data = code_client.parse_web_hook_post_data(
            data['external_data'], data['code_repo'])

        LOG.debug("web_hook_data: {}".format(web_hook_data))
        data.pop('external_data')
        data['code_repo']['code_head_commit'] = web_hook_data.get_code_head_commit()
        return web_hook_data

    @staticmethod
    def format_dict_to_unicode(input_dict):
        for key, value in input_dict.items():
            if isinstance(value, dict):
                input_dict[key] = RazzilUtil.format_dict_to_unicode(value)
            elif value is None:
                input_dict[key] = ''
            else:
                if is_string(value):
                    input_dict[key] = convert_to_unicode(value)
                else:
                    input_dict[key] = str(value)

        return input_dict

    @staticmethod
    def append_endpoint_name(data):
        '''
            data: list or object of: build_config, build
        '''
        if isinstance(data, list):
            endpoint_ids = map(lambda item:item.get('endpoint_id'), data)
            display_name_map = FurionUtil.get_display_name_map_by_endpoint_ids(endpoint_ids)
            for item in data:
                item["endpoint_name"] = display_name_map.get(item.get('endpoint_id'))
            return data
        if isinstance(data, dict):
            display_name_map = FurionUtil.get_display_name_map_by_endpoint_ids(
                [data.get('endpoint_id')])
            data['endpoint_name'] = display_name_map.get(data.get('endpoint_id'))
        LOG.warn("data type:{} is not expected, no endpoint name appended".format(type(data)))
        return data

    @staticmethod
    def split_as_days(start_datetime, end_datetime):
        days = []
        current_date = start_datetime.date()
        tzinfo = start_datetime.tzinfo
        while True:
            current_start_datetime = datetime.datetime.strptime(
                str(current_date)+" 00:00:00", "%Y-%m-%d %H:%M:%S").replace(tzinfo=tzinfo)
            if current_start_datetime < start_datetime:
                current_start_datetime = start_datetime
            if current_start_datetime > end_datetime:
                break
            current_end_datetime = datetime.datetime.strptime(
                str(current_date)+" 23:59:59", "%Y-%m-%d %H:%M:%S").replace(tzinfo=tzinfo)
            if current_end_datetime > end_datetime:
                current_end_datetime = end_datetime
            days.append({
                "start": current_start_datetime,
                "end": current_end_datetime,
                "date": current_date
            })
            current_date = current_date + datetime.timedelta(days=1)
        return days


# only "True"/"true" can be converted to True
def convert_string_to_bool(bool_str):
    return bool_str and bool_str.lower() == 'true'


def covert_string_to_datatime(time_str):
    try:
        timestamp = float(time_str)
        dt = datetime.datetime.utcfromtimestamp(timestamp)
        return dt
    except ValueError as e:
        logging.error(e.message)
        raise

def get_now_cur_time_zone():
    """
    Get time by timezone of config
    """
    now = datetime.datetime.now()
    if settings.CUR_TIME_ZONE != settings.TIME_ZONE:
        now = datetime.datetime.now(pytz.timezone(settings.CUR_TIME_ZONE))
    return now