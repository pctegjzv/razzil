# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-20 15:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('private_builds', '0009_buildendpoint_proxy_config'),
    ]

    operations = [
        migrations.AddField(
            model_name='buildendpoint',
            name='region_id',
            field=models.CharField(blank=True, max_length=64),
        ),
    ]
