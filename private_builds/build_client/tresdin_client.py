from django.conf import settings

from mekansm.request import MekRequest


class TresdinRequest(MekRequest):

    endpoint = '{}/{}'.format(settings.TRESDIN_CLIENT.get('endpoint'),
                              settings.TRESDIN_CLIENT.get('api_version'))
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': 'razzil/v1.0'
    }



class TresdinClient(object):

    STATUS_WAITING = "waiting"
    STATUS_RUNNING = 'running'
    STATUS_SUCCEEDED = 'succeeded'
    STATUS_FAILED = 'failed'

    @classmethod
    def create(cls, payload):
        return TresdinRequest.send('instant-jobs/', method='POST', data=payload)['data']

    @classmethod
    def retrieve(cls, name, params={}):
        return TresdinRequest.send('instant-jobs/{}'.format(name), method='GET',
                                   params=params)['data']

    @classmethod
    def delete(cls, name, params={}):
        return TresdinRequest.send('instant-jobs/{}'.format(name), method='DELETE',
                                   params=params)['data']
