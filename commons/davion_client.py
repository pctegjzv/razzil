import logging

from django.conf import settings

from mekansm.request import MekRequest

logger = logging.getLogger(__name__)


class DavionRequest(MekRequest):
    endpoint = '{}/{}'.format(settings.DAVION_ENDPOINT, settings.DAVION_API_VERSION)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': 'razzil/v1.0'
    }


class DavionClient(object):

    @classmethod
    def get_integration(cls, instance_id):
        path = "/integrations/{}".format(instance_id)
        return DavionRequest.send(path)['data']