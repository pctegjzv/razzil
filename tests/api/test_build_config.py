from rest_framework.test import APITestCase
from private_build_configs.models import (
    BuildConfig,
    BuildConfigCode,
    BuildConfigIntegration,
    INTEGRATION_SONARQUBE)
from commons.medusa_client import MedusaClient
from alauda.weblab import WeblabClient
from commons.furion_client import FURION_CLIENT
from tests.api.common import WebLabResult
import mock
from commons.sonarqube_client import SonarClient
from tests.api.common import SonarClientMock
from commons.davion_client import DavionClient

json_create_build_config = {
    "namespace":"admin",
    "name":"json_create_build_config",
    "image_repo_id": "798f19e7-f99c-452a-82a5-66728c54113e",
    "code_repo": {
        "code_repo_client": "SIMPLE_GIT",
        "code_repo_path": "git@52.24.16.115:root/testCodeRepo.git",
        "code_repo_type": "BRANCH",
        "code_repo_type_value": "master",
        "dockerfile_location": "/",
        "build_context_path": "/"
    },
    "created_by": "liaoj",
    "customize_tag": "latest",
    "auto_tag_type": "TIME",
    "image_cache_enabled": True,
    "auto_build_enabled": True,
    "notification_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "schedule_rule": "0 8 * * *"
}

json_create_build_config1 = {
    "namespace":"admin",
    "name":"json_create_build_config1",
    "image_repo_id": "798f19e7-f99c-452a-82a5-66728c54113e",
    "code_repo": {
        "code_repo_client": "SIMPLE_GIT",
        "code_repo_path": "git@52.24.16.115:root/testCodeRepo.git",
        "code_repo_type": "BRANCH",
        "code_repo_type_value": "master/*",
        "dockerfile_location": "/",
        "build_context_path": "/"
    },
    "created_by": "liaoj",
    "customize_tag": "latest",
    "auto_tag_type": "TIME",
    "image_cache_enabled": True,
    "auto_build_enabled": True,
    "notification_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "schedule_rule": "0 8 * * *"
}

json_create_build_config_sonarqube = {
    "namespace":"admin",
    "name":"json_create_build_config_sonarqube",
    "image_repo_id": "798f19e7-f99c-452a-82a5-66728c54113e",
    "code_repo": {
        "code_repo_client": "SIMPLE_GIT",
        "code_repo_path": "git@52.24.16.115:root/testCodeRepo.git",
        "code_repo_type": "BRANCH",
        "code_repo_type_value": "master/*",
        "dockerfile_location": "/",
        "build_context_path": "/"
    },
    "created_by": "liaoj",
    "customize_tag": "latest",
    "auto_tag_type": "TIME",
    "image_cache_enabled": True,
    "auto_build_enabled": True,
    "notification_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "schedule_rule": "0 8 * * *",
    "sonarqube":{
        "integration_instance_id": "xxx-xxxx",
        "build_integration_config": {
            "quality_gate": "default",
            "code_language": "java",
            "code_encoding": "utf8",
            "code_scan_path": "/src"
        }
    }
}

json_update_build_config = {
    "namespace": "admin",
    "image_repo_id": "998f19e7-f99c-452a-82a5-66728c54113e",
    "code_repo": {
        "code_repo_client": "SIMPLE_GIT",
        "code_repo_path": "git@52.24.16.115:root/testCodeRepo.git",
        "code_repo_type": "DIR",
        "code_repo_type_value": "liaojian",
        "dockerfile_location": "/liaojian",
        "build_context_path": "/liaojian"
    },
    "customize_tag": "liaojian",
    "auto_tag_type": "COMMIT",
    "image_cache_enabled": False,
    "auto_build_enabled": False,
    "notification_id": "998f19e7-f99c-452a-82a5-66728c54113e"
}

json_update_build_config_sonarqube = {
    "namespace": "admin",
    "image_repo_id": "998f19e7-f99c-452a-82a5-66728c54113e",
    "code_repo": {
        "code_repo_client": "SIMPLE_GIT",
        "code_repo_path": "git@52.24.16.115:root/testCodeRepo.git",
        "code_repo_type": "DIR",
        "code_repo_type_value": "liaojian",
        "dockerfile_location": "/liaojian",
        "build_context_path": "/liaojian"
    },
    "customize_tag": "liaojian",
    "auto_tag_type": "COMMIT",
    "image_cache_enabled": False,
    "auto_build_enabled": False,
    "notification_id": "998f19e7-f99c-452a-82a5-66728c54113e",
    "sonarqube":{
        "integration_instance_id": "11-dwec-ss",
        "build_integration_config": {
            "quality_gate": "sonarway",
            "code_language": "java",
            "code_encoding": "utf8",
            "code_scan_path": "/src"
        }
    }
}


class PrivateBuildConfigsTest(APITestCase):
    def call_api_create_config(self, json=None):
        if json is None:
            json = json_create_build_config
        response = self.client.post('/v1/private_build_configs/',
                                    json,
                                    format='json')
        return response

    def call_api_get_config_list(self, uuids):
        return self.client.get('/v1/private_build_configs/?uuids={}'.format(uuids))

    def call_api_get_config_detail(self, uuid):
        return self.client.get('/v1/private_build_configs/{}/'.format(uuid))

    def call_api_delete_config_detail(self, uuid, namespace):
        response = self.client.delete('/v1/private_build_configs/{}/'.format(uuid),{"namespace":namespace})
        return response

    def call_api_update_config_detail(self, uuid, json_update=None):
        if not json_update:
            json_update = json_update_build_config
        response = self.client.put('/v1/private_build_configs/{}/'.format(uuid),
                                   json_update,
                                   format='json')
        return response

    def call_api_get_config_detail_by_hook(self, webhook_code):
        return self.client.get('/v1/private_build_configs/auto_build/{}/'.format(webhook_code))

    @mock.patch.object(FURION_CLIENT, 'list_regions')
    @mock.patch.object(MedusaClient, "create_schedule_event_config")
    @mock.patch.object(WeblabClient, "prefetch")
    def test_create_build_config(self, weblab_prefetch_mock, medusa_create_mock, furion_list_mock):
        medusa_create_mock.return_value={
            "id":"test-id",
            "secret_key":"key"
        }
        furion_list_mock.return_value=[]
        weblab_prefetch_mock.return_value=WebLabResult()
        data =[
            json_create_build_config, json_create_build_config1,
            json_create_build_config_sonarqube
        ]

        for json in data:
            response = self.call_api_create_config(json)
            self.assertEquals(response.status_code, 201)
            build_config_id_of_created = response.data['config_id']
            self.assertIsNotNone(build_config_id_of_created)
            self._verfy_create_build_config(build_config_id_of_created, json)

    def _verfy_create_build_config(self, build_config_id_of_created, build_config_of_json_source):

        build_config = BuildConfig.objects.get(config_id=build_config_id_of_created)
        self.assertEquals(str(build_config.image_repo_id),
                          build_config_of_json_source['image_repo_id'])
        self.assertEquals(build_config.customize_tag,
                          build_config_of_json_source['customize_tag'])
        self.assertEquals(build_config.auto_tag_type,
                          build_config_of_json_source['auto_tag_type'])
        self.assertEquals(build_config.image_cache_enabled,
                          build_config_of_json_source['image_cache_enabled'])
        self.assertEquals(build_config.auto_build_enabled,
                          build_config_of_json_source['auto_build_enabled'])
        self.assertEquals(str(build_config.notification_id),
                          build_config_of_json_source['notification_id'])
        if build_config_of_json_source['schedule_rule']:
            if not BuildConfigCode.is_wildcard_of_repo_type_value(
                build_config_of_json_source['code_repo']["code_repo_type_value"]
            ):          
                self.assertTrue(build_config.schedule_config_id)
            else:
                self.assertFalse(build_config.schedule_config_id)    
        build_config_code = BuildConfigCode.objects.get(build_config_id=build_config_id_of_created)

        self.assertEquals(build_config_code.code_repo_client,
                          build_config_of_json_source['code_repo']['code_repo_client'])
        self.assertEquals(build_config_code.code_repo_path,
                          build_config_of_json_source['code_repo']['code_repo_path'])
        self.assertEquals(build_config_code.code_repo_type,
                          build_config_of_json_source['code_repo']['code_repo_type'])
        self.assertEquals(build_config_code.code_repo_type_value,
                          build_config_of_json_source['code_repo']['code_repo_type_value'])
        self.assertEquals(build_config_code.build_context_path,
                          build_config_of_json_source['code_repo']['build_context_path'])                
        self.assertIsNotNone(build_config_code.code_repo_private_key)
        self.assertIsNotNone(build_config_code.code_repo_public_key)
        if build_config_of_json_source['auto_build_enabled']:
            self.assertIsNotNone(build_config_code.code_repo_webhook_code)
        if build_config_of_json_source.get("sonarqube"):
            sonarqube = BuildConfigIntegration.objects.filter(
                type = INTEGRATION_SONARQUBE,
                build_config_id = build_config.config_id)[0]
            self.assertEqual(sonarqube.type, INTEGRATION_SONARQUBE)
            self.assertEqual(
                sonarqube.integration_instance_id,
                build_config_of_json_source["sonarqube"]["integration_instance_id"])
            self.assertEqual(
                sonarqube.build_integration_config['code_encoding'],
                build_config_of_json_source["sonarqube"]["build_integration_config"]['code_encoding']
            )
            self.assertEqual(
                sonarqube.build_integration_config['code_scan_path'],
                build_config_of_json_source["sonarqube"]["build_integration_config"]['code_scan_path']
            )
            self.assertEqual(
                sonarqube.build_integration_config['code_language'],
                build_config_of_json_source["sonarqube"]["build_integration_config"]['code_language']
            )
            self.assertEqual(
                sonarqube.build_integration_config['project_key'],
                build_config_id_of_created
            )
            self.assertEqual(
                sonarqube.build_integration_config['project'],
                build_config_of_json_source["namespace"]+":"+build_config_of_json_source["name"]
            )

    @mock.patch.object(WeblabClient, "prefetch")
    @mock.patch.object(FURION_CLIENT, 'list_regions')
    @mock.patch.object(MedusaClient, "get_schedule_event_config_list")
    @mock.patch.object(MedusaClient, "create_schedule_event_config")
    @mock.patch.object(MedusaClient, "get_schedule_event_config")
    def test_get_config_list(self, medusa_get_mock, medusa_create_mock, medusa_get_list_mock, furion_list_mock, weblab_prefetch_mock):
        medusa_create_mock.return_value={
            "id":"test-id",
            "secret_key":"key"
        }
        medusa_get_mock.return_value={
            "id":"test-id",
            "secret_key":"key"
        }
        medusa_get_list_mock.return_value=[]
        furion_list_mock.return_value=[]
        weblab_prefetch_mock.return_value = WebLabResult()
        response_list = []
        response_list.append(self.call_api_create_config())
        response_list.append(self.call_api_create_config())
        response_list.reverse()
        uuids = '{},{}'.format(response_list[0].data['config_id'],
                               response_list[1].data['config_id'])
        response = self.call_api_get_config_list(uuids)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.data), len(response_list))
        for i in range(len(response.data)):
            self.assertEquals(response.data[i]['config_id'], response_list[i].data['config_id'])
            self.assertEquals(response.data[i]['image_repo_id'],
                              json_create_build_config['image_repo_id'])
            self.assertEquals(response.data[i]['code_repo']['code_repo_path'],
                              json_create_build_config['code_repo']['code_repo_path'])
            self.assertEquals(response.data[i]['code_repo']['code_repo_type'],
                              json_create_build_config['code_repo']['code_repo_type'])
            self.assertEquals(response.data[i]['code_repo']['code_repo_type_value'],
                              json_create_build_config['code_repo']['code_repo_type_value'])
    
    @mock.patch.object(MedusaClient, "delete_schedule_event_config")
    @mock.patch.object(WeblabClient, "prefetch")
    @mock.patch.object(MedusaClient, "get_schedule_event_config")
    @mock.patch.object(MedusaClient, "create_schedule_event_config")
    def test_get_config_detail(self, medusa_create_mock, medusa_get_mock, weblab_perfetch_mock, medusa_delete_mock):
        medusa_create_mock.return_value={
            "id":"test-id",
            "secret_key":"key"
        }
        medusa_get_mock.return_value={
            "id":"test-id",
            "secret_key":"key"
        }
        weblab_perfetch_mock.return_value=WebLabResult()
        medusa_delete_mock.return_value={}
        data = [
            json_create_build_config, json_create_build_config_sonarqube
        ]
        for json in data:
            create_response = self.call_api_create_config(json)
            response = self.call_api_get_config_detail(create_response.data['config_id'])
            self.assertEquals(response.status_code, 200)
            self.assertEquals(create_response.data['config_id'], response.data['config_id'])
            json['config_id'] = create_response.data['config_id']
            self._verfy_get_build_config(response.data, json)

    def _verfy_get_build_config(self, created, json_source):
        self.assertEquals(created['customize_tag'],
                          json_source['customize_tag'])
        self.assertEquals(created['auto_tag_type'],
                          json_source['auto_tag_type'])
        self.assertEquals(created['image_cache_enabled'],
                          json_source['image_cache_enabled'])
        self.assertEquals(created['auto_build_enabled'],
                          json_source['auto_build_enabled'])
        self.assertEquals(created['notification_id'],
                          json_source['notification_id'])
        self.assertEquals(created['image_repo_id'],
                          json_source['image_repo_id'])
        self.assertEquals(created['code_repo']['code_repo_path'],
                          json_source['code_repo']['code_repo_path'])
        self.assertEquals(created['code_repo']['code_repo_type'],
                          json_source['code_repo']['code_repo_type'])
        self.assertEquals(created['code_repo']['code_repo_type_value'],
                          json_source['code_repo']['code_repo_type_value'])
        if json_source.get("sonarqube"):
            sonarqube = created['sonarqube']
            self.assertEqual(
                sonarqube['integration_instance_id'],
                json_source["sonarqube"]["integration_instance_id"])
            self.assertEqual(
                sonarqube['build_integration_config']['code_encoding'],
                json_source["sonarqube"]["build_integration_config"]['code_encoding']
            )
            self.assertEqual(
                sonarqube['build_integration_config']['code_language'],
                json_source["sonarqube"]["build_integration_config"]['code_language']
            )
            self.assertEqual(
                sonarqube['build_integration_config']['code_scan_path'],
                json_source["sonarqube"]["build_integration_config"]['code_scan_path']
            )
            self.assertEqual(
                sonarqube['build_integration_config']['project_key'],
                json_source['config_id']
            )
            self.assertEqual(
                sonarqube['build_integration_config']['project'],
                json_source['namespace']+":"+json_source['name']
            )

    @mock.patch.object(SonarClient, 'reset_settings')
    @mock.patch.object(DavionClient, 'get_integration')
    @mock.patch.object(WeblabClient, "prefetch")
    @mock.patch.object(MedusaClient, "create_schedule_event_config")
    @mock.patch.object(MedusaClient, "delete_schedule_event_config")
    def test_update_config_detail(self, medusa_delete_mock, medusa_create_mock, weblab_prefetch_mock, davion_get_mock, sonar_reset_mock):
        medusa_create_mock.return_value={
            "id":"test-id",
            "secret_key":"key"
        }
        medusa_delete_mock.return_value=True
        weblab_prefetch_mock.return_value=WebLabResult()
        davion_get_mock.return_value = {
            "enabled": True,
            "fields":{
                "endpoint": "http://sonarqube.alauda.org",
                "token": "dsaa-342sx"
            }
        }
        sonar_reset_mock.return_value={}
        data = [
            (json_create_build_config, json_update_build_config),
            (json_create_build_config_sonarqube, json_update_build_config),
            (json_create_build_config_sonarqube, json_update_build_config_sonarqube)
        ]
        for item in data:
            json_create = item[0]
            json_update = item[1]
            json_update["name"] = json_create["name"]
            create_response = self.call_api_create_config(json_create)
            response = self.call_api_update_config_detail(create_response.data['config_id'], json_update)
            self.assertEquals(response.status_code, 204)
            json_update['config_id'] = create_response.data['config_id']
            build_config = BuildConfig.objects.get(config_id=create_response.data['config_id'])
            self._verfy_update_build_config(build_config, json_update)

    def _verfy_update_build_config(self, build_config, json_updated):
        self.assertEquals(str(build_config.image_repo_id),
                          json_updated['image_repo_id'])
        self.assertEquals(build_config.customize_tag,
                          json_updated['customize_tag'])
        self.assertEquals(build_config.auto_tag_type,
                          json_updated['auto_tag_type'])
        self.assertEquals(build_config.image_cache_enabled,
                          json_updated['image_cache_enabled'])
        self.assertEquals(build_config.auto_build_enabled,
                          json_updated['auto_build_enabled'])
        self.assertEquals(str(build_config.notification_id),
                          json_updated['notification_id'])
        build_config_code = BuildConfigCode.objects\
            .get(build_config_id=build_config.config_id)
        self.assertEquals(build_config_code.code_repo_type,
                          json_updated['code_repo']['code_repo_type'])
        self.assertEquals(build_config_code.code_repo_type_value,
                          json_updated['code_repo']['code_repo_type_value'])
        self.assertEquals(build_config_code.build_context_path,
                          json_updated['code_repo']['build_context_path'])
        if json_updated.get("sonarqube"):
            sonarqube = BuildConfigIntegration.objects.all().filter(
                type=INTEGRATION_SONARQUBE,
                build_config_id=build_config.config_id
            )[0]
            self.assertEqual(
                sonarqube.integration_instance_id,
                json_updated["sonarqube"]["integration_instance_id"])
            self.assertEqual(
                sonarqube.build_integration_config['code_encoding'],
                json_updated["sonarqube"]["build_integration_config"]['code_encoding']
            )
            self.assertEqual(
                sonarqube.build_integration_config['code_language'],
                json_updated["sonarqube"]["build_integration_config"]['code_language']
            )
            self.assertEqual(
                sonarqube.build_integration_config['code_scan_path'],
                json_updated["sonarqube"]["build_integration_config"]['code_scan_path']
            )
            self.assertEqual(
                sonarqube.build_integration_config['project_key'],
                json_updated['config_id']
            )
            self.assertEqual(
                sonarqube.build_integration_config['project'],
                json_updated['namespace']+":"+json_updated['name']
            )

    @mock.patch.object(WeblabClient, "prefetch")
    @mock.patch.object(MedusaClient, "get_schedule_event_config")
    @mock.patch.object(MedusaClient, "create_schedule_event_config")
    @mock.patch.object(MedusaClient, "delete_schedule_event_config")
    def test_get_detail_by_webhook(self, medusa_create_mock, medusa_get_mock, weblab_prefetch_mock, medusa_delete_mock):
        medusa_create_mock.return_value={
            "id":"test-id",
            "secret_key":"key"
        }
        medusa_get_mock.return_value={
            "id":"test-id",
            "secret_key":"key"
        }
        weblab_prefetch_mock.return_value=WebLabResult()
        medusa_delete_mock.return_value={}
        create_response = self.call_api_create_config()
        response = self.call_api_get_config_detail(create_response.data['config_id'])
        response_by_hook = self.call_api_get_config_detail_by_hook(
            response.data['code_repo']['code_repo_webhook_code'])
        self.assertEquals(response_by_hook.status_code, 200)
        self.assertEquals(response.data["config_id"], response_by_hook.data["config_id"])
        self.assertEquals(response.data['code_repo']['code_repo_webhook_code'],
                             response_by_hook.data["code_repo"]['code_repo_webhook_code'])
        self.assertEquals(response.data['code_repo']['code_id'], 
                            response_by_hook.data['code_repo']['code_id'])

    @mock.patch.object(SonarClient, 'remove_project_webhook')
    @mock.patch.object(DavionClient, 'get_integration')
    @mock.patch.object(WeblabClient, "prefetch")
    @mock.patch.object(MedusaClient, "create_schedule_event_config")
    @mock.patch.object(MedusaClient, "delete_schedule_event_config")
    def test_delete_build_config(self, medusa_delete_mock, medusa_create_mock, weblab_perfetch_mock, davion_get_mock, sonar_remove_mock):
        sonar_remove_mock.return_value={}
        medusa_create_mock.return_value = {
            "id":"test-id",
            "secret_key":"key"
        }
        medusa_delete_mock.return_value = True
        weblab_perfetch_mock.return_value = WebLabResult()
        davion_get_mock.return_value = {
            "enabled": True,
            "fields":{
                "endpoint": "http://sonarqube.alauda.org",
                "token": "dsaa-342sx"
            }
        }
        data =[
            json_create_build_config,
            json_create_build_config_sonarqube
        ]
        for json in data:
            create_response = self.call_api_create_config(json)
            response = self.call_api_delete_config_detail(create_response.data['config_id'], json["namespace"])
            self.assertEquals(response.status_code, 204)
            self._verfy_delete_build_config(json, create_response.data['config_id'])

    def _verfy_delete_build_config(self, json_created, config_id):
        is_exists = BuildConfig.objects.filter(config_id=config_id).exists()
        self.assertFalse(is_exists)
        is_exists_code = BuildConfigCode.objects.filter(
            build_config=config_id).exists()
        self.assertFalse(is_exists_code)
        is_exists_sonarqube = BuildConfigIntegration.objects.filter(
            build_config_id=config_id, type=INTEGRATION_SONARQUBE).exists()
        self.assertFalse(is_exists_sonarqube)