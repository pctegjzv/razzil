from django.conf.urls import url

from commons.constants import APPLICATION_TEMPLATE_URL_REGEX
from private_builds import views


urlpatterns = [
    url(r'(?P<namespace>{})/sonarqube/(?P<integration_id>{})/languages/?$'.format(
            APPLICATION_TEMPLATE_URL_REGEX, APPLICATION_TEMPLATE_URL_REGEX),
        views.PrivateBuildsViewset.as_view({
            'get': 'list_sonarqube_languages'
        })),
    url(r'(?P<namespace>{})/sonarqube/(?P<integration_id>{})/qualitygates/?$'.format(
            APPLICATION_TEMPLATE_URL_REGEX, APPLICATION_TEMPLATE_URL_REGEX),
        views.PrivateBuildsViewset.as_view({
            'get': 'list_sonarqube_qualitygates'
        })),
]
