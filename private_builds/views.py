import json
import logging
import string
import time
import datetime
import traceback
import uuid
import pytz

from alauda_redis_client import RedisClientFactory
from django.db import transaction
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.db.models import Q, Count
from django.conf import settings

from rest_framework import status, viewsets
from rest_framework.response import Response

from alauda.tracing import tracing_response_time, send_counter_metrics

from commons.docker_build_exception import DockerBuildException
from commons.furion_client import FurionUtil
from commons.util import RazzilUtil
from commons.util import get_now_cur_time_zone
from commons.util import convert_string_to_bool
from commons.util import covert_string_to_datatime
from private_build_configs.cache import AlaudaCache
from private_build_configs.models import (AUTO_TAG_TYPE_TIME,
                                          AUTO_TAG_TYPE_COMMIT,
                                          BuildConfigCode,
                                          CODE_REPO_CLIENT_SIMPLE_SVN,
                                          INTEGRATION_SONARQUBE)
from private_builds.client import FurionClient
from private_builds.log_client.log_client import ElasticSearchClient
from private_builds.models import (Build, BuildCode, BuildEndpoint, STATUS_BLOCKED,
                                   STATUS_DELETED, STATUS_IN_PROGRESS, STATUS_SUCCEEDED,
                                   STATUS_WEIGHTS, COMPLETE_STATUS, PUBLIC_ENDPOINT_NAME,
                                   STATUS_FAILED, COMPLETE_STATUS,
                                   BuildIntegration, STATUS_WAITING, BuildImageTag,
                                   SONARQUBE_STEP_VALUE, SONARQUBE_STEP_ANALYSIS,
                                   SONARQUBE_STEP_COMPLETE, SONARQUBE_STEP_COLLECT)
from private_builds.notification_client.notification_client import NotificationClient
from private_builds.serializers import (BuildSerializer, BuildCodeSerializer,
                                        BuildIncludeCodeSerializer, BuildEndpointeSerializer,
                                        BuildSimpleEndpointSerializer, BuildIntegrationSerializer)
from private_builds.build_client.tresdin_client import TresdinClient
from commons.medusa_client import MedusaClient
from commons.weblab import Weblab
from commons.davion_client import DavionClient
from commons.sonarqube_client import SonarClient
from django.db.models import Q
from private_builds.models import delete_artifacts
from commons.oss_client import OSS
from mekansm.exceptions import MekAPIException
from private_builds.locker import BuildSameCommitLocker


LOG = logging.getLogger(__name__)
TIMESPAN_LIMIT = 60 * 60 * 24 * 3
MAX_STATUS_SYNC_FAIL_COUNT = 2  # continuous count
MAX_SONAR_STATUS_SYNC_FAIL_COUNT = 3 # sonar max fail count for medusa

DASHBOARD_OK="OK"
DASHBOARD_ERROR="ERROR"
DASHBOARD_OTHER="OTHER"

class PrivateBuildsViewset(viewsets.ViewSet):
    build_query_set = Build.objects.all()
    build_code_query_set = BuildCode.objects.all()
    build_endpoint_query_set = BuildEndpoint.objects.all()
    build_image_tag_set = BuildImageTag.objects.all()
    build_integration_set = BuildIntegration.objects.all()

    @tracing_response_time()
    def copy_image_tag_info(self, request):
        """
        this method is invoked after image transfered
        :param request:
        :return:
        """
        source_repo_id = request.data.get('source_repo_id')
        tag = request.data.get('tag')
        dest_id = request.data.get('dest_id')

        if not source_repo_id or not tag or not dest_id:
            raise DockerBuildException(
                'miss_args',
                message_params='source_repo_id, tag, dest_id')

        try:
            image_tag = BuildImageTag.objects.get(image_repo_id=source_repo_id, tag_name=tag)

            # dest repository may exist or not exist
            new_image_tag, created = BuildImageTag.objects.get_or_create(
                image_repo_id=dest_id, tag_name=tag)
            new_image_tag.namespace = image_tag.namespace
            new_image_tag.user_token = image_tag.user_token
            new_image_tag.build_id = image_tag.build_id
            new_image_tag.artifact_upload_enabled = image_tag.artifact_upload_enabled
            new_image_tag.allow_artifact_upload_fail = image_tag.allow_artifact_upload_fail
            new_image_tag.save()
            AlaudaCache.del_image_tag(dest_id, tag)

        except BuildImageTag.DoesNotExist:
            LOG.error("source_repo_id: {}, tag: {} does not exits in database.".
                      format(source_repo_id, tag))
            raise DockerBuildException('build_image_tag_not_exists')

        return Response(status=status.HTTP_204_NO_CONTENT)

    @tracing_response_time()
    def tag_artifacts_count(self, request):
        repository_id = request.query_params.get('repository_id')
        tag_names = request.query_params.get('tag_names', '')
        if not repository_id or not tag_names:
            raise DockerBuildException(
                'miss_args',
                message_params='repository_id, tag_names')

        token = self.get_token(request)
        tag_names = string.split(tag_names, ',')
        result = []
        if tag_names:
            # todo how to improve the first forloop before cached
            for tag_name in tag_names:
                try:
                    data = self.get_image_tag_data(repository_id, tag_name, token)
                except:  # can not be interrupted here
                    data = None
                result.append({
                    'tag_name': tag_name,
                    'artifacts_count': data.get('artifacts_count') if data else 0,
                })

        return Response(result)

    @tracing_response_time()
    def tag_artifacts(self, request):
        # this is changed after transfered
        repository_id = request.query_params.get('repository_id')
        tag_name = request.query_params.get('tag_name')
        if not repository_id or not tag_name:
            raise DockerBuildException(
                'miss_args',
                message_params='repository_id, tag_name')

        LOG.info('repository_id: {}, tag_name: {}'.
                 format(repository_id, tag_name))
        try:
            data = self.get_image_tag_data(repository_id, tag_name, self.get_token(request))
        except:
            raise DockerBuildException("get_image_tag_failed")

        result = {
            'tag_name': tag_name,
            'build_id': data.get('build_id') if data else None,
            'build_at': data.get('build_at') if data else None,
            'artifacts_count': data.get('artifacts_count') if data else 0,
            'artifacts': data.get('artifacts') if data else [],
        }

        return Response(result)

    def get_image_tag_data(self, repository_id, tag_name, token):
        image_tag = AlaudaCache.get_image_tag(repository_id, tag_name)
        if not image_tag:
            image_tag = self.cache_result(repository_id, tag_name, token)
        image_tag = json.loads(image_tag)
        return image_tag

    def cache_result(self, repository_id, tag_name, token):
        build_image_tag, created = self.build_image_tag_set.get_or_create(
            image_repo_id=repository_id,
            tag_name=tag_name)

        # upward compatibility, build history may be deleted
        builds = self.build_query_set.filter(
            Q(image_repo_id=repository_id) &
            (Q(docker_repo_version_tag=tag_name) |
                Q(docker_repo_label_tag=tag_name) |
                Q(docker_repo_tag=tag_name))
        ).order_by('-created_at')
        if created and builds.count() > 0:
            build = builds[0]
            build_image_tag.build_id = build.build_id
            build_image_tag.namespace = build.namespace
            build_image_tag.user_token = build.user_token
            build_image_tag.artifact_upload_enabled = build.is_artifact_enabled
            build_image_tag.allow_artifact_upload_fail = build.allow_artifact_upload_fail
            build_image_tag.save()
        result = AlaudaCache.save_image_tag(build_image_tag, token)
        return result

    def delete_endpoint(self, request, namespace):
        region_id = request.query_params.get('region_id', '')
        if not region_id:
            raise DockerBuildException('invalid_args')

        query_set = self.build_endpoint_query_set.filter(
            region_id=region_id, namespace=namespace)
        if query_set.exists():
            query_set.delete()
        else:
            LOG.error('not found any endpoint can be deleted. region_id:{} namespace:{}'
                      .format(region_id, namespace))
        return Response(status=status.HTTP_204_NO_CONTENT)

    @tracing_response_time()
    def list_endpoint(self, request, namespace):
        # get all endPoints
        query_set = self.build_endpoint_query_set.filter(
            Q(namespace=namespace) | Q(is_public=True)).order_by('is_public')
        endpoint_list = BuildSimpleEndpointSerializer(query_set, many=True).data

        # get region id to region display name map
        region_id_to_name_map = FurionUtil.get_region_name_map(namespace)
        for endpoint in endpoint_list:
            if endpoint.get('is_public'):
                endpoint['endpoint_name'] = PUBLIC_ENDPOINT_NAME
            elif endpoint.get('region_id'):
                endpoint['endpoint_name'] = region_id_to_name_map.get(endpoint.get('region_id'))

            if not endpoint.get('endpoint_name'):
                endpoint['endpoint_name'] = endpoint['endpoint']

            endpoint.pop('endpoint')

        return Response(endpoint_list)

    @tracing_response_time()
    def create_endpoint(self, request, namespace):
        LOG.info('Creating private build endpoint with data: {}'.format(request.data))

        # check whether already have endpoint
        query_args = {'namespace': namespace}
        if request.data.get('region_id'):
            query_args['region_id'] = request.data.get('region_id')

        query_set = self.build_endpoint_query_set.filter(**query_args)
        if query_set.exists():
            raise DockerBuildException('build_endpoint_already_exists')

        # get current builder image
        endpoint_instance = self.build_endpoint_query_set.first()
        request.data['builder_image'] = endpoint_instance.builder_image
        request.data['namespace'] = namespace

        # save the endpoint
        serializer = BuildEndpointeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @tracing_response_time()
    def list_artefacts(self, request):
        results = []
        start_time_str = request.query_params.get('start_time')
        end_time_str = request.query_params.get('end_time')
        bucket_name = request.query_params.get('bucket_name')
        user_token = request.query_params.get('user_token')

        if not (user_token and bucket_name):
            raise MekAPIException('invalid_args',
                                  message='user_token or bucket_name is empty'
                                  )

        if start_time_str and end_time_str:
            start_time = covert_string_to_datatime(start_time_str)
            end_time = covert_string_to_datatime(end_time_str)
            build_list_query_set = self.build_query_set.filter(
                Q(status=STATUS_SUCCEEDED) & Q(artifact_upload_enabled=True) &
                Q(created_at__gte=start_time) & Q(created_at__lte=end_time))
        elif not start_time_str and end_time_str:
            end_time = covert_string_to_datatime(end_time_str)
            build_list_query_set = self.build_query_set.filter(
                Q(status=STATUS_SUCCEEDED) & Q(artifact_upload_enabled=True) &
                Q(created_at__lte=end_time))
        elif start_time_str and not end_time_str:
            start_time = covert_string_to_datatime(start_time_str)
            build_list_query_set = self.build_query_set.filter(
                Q(status=STATUS_SUCCEEDED) & Q(artifact_upload_enabled=True) &
                Q(created_at__gte=start_time))
        else:
            build_list_query_set = self.build_query_set.filter(
                Q(status=STATUS_SUCCEEDED) & Q(artifact_upload_enabled=True))

        oss_client = OSS(settings.OSS_ENDPOINT, user_token)
        artifacts = oss_client.list_objects(bucket_name, prefix='build/')

        artifact_count = {}
        for artifact in artifacts:
            build_id = artifact['key'].lstrip('/').split('/')[0]
            if build_id not in artifact_count:
                artifact_count[build_id] = 0
            artifact_count[build_id] += 1
        for build in build_list_query_set:
            if build.build_id in artifact_count:
                item = {}
                item['build_id'] = build.build_id
                item['artifacts_count'] = artifact_count[build.build_id]
                results.append(item)

        return Response(results)

    @tracing_response_time()
    def multiple_report(self, request):
        date_s = request.query_params.get('date')
        date_d = datetime.datetime.strptime(date_s, '%Y-%m-%d')
        row_set = self.build_query_set.filter(ended_at__gte=date_d,
            ended_at__lt=date_d + datetime.timedelta(days=1)) \
            .values('status', 'config__project_name').annotate(status_count=Count('status')) \
            .order_by('config__project_name')

        results = []
        result = {}
        for row in row_set:
            if row['config__project_name'] == "":
                continue
            if not result:
                result = {
                            "project" : row['config__project_name'],
                            "date" : date_s,
                            "success" : 0,
                            "failure" : 0,
                            "total" : 0,
                        }
                result = self._add_status(result, row['status'], row['status_count'])
                continue
            if result['project'] != row['config__project_name']:
                results.append(result)
                result = {
                            "project" : row['config__project_name'],
                            "date" : date_s,
                            "success" : 0,
                            "failure" : 0,
                            "total" : 0,
                        }
            result = self._add_status(result, row['status'], row['status_count'])
        if result:
            results.append(result)

        return Response(results)

    def _add_status(self, result, status, status_count):
        if status == "S":
            result['success'] += status_count
            result['total'] += status_count
        elif status == "F":
            result['failure'] += status_count
            result['total'] += status_count
        return result

    @tracing_response_time()
    def delete_artifacts(self, request, private_build_id):
        build = get_object_or_404(self.build_query_set, build_id=private_build_id)
        if build.is_artifact_enabled:
            delete_artifacts(build.namespace, build.build_id, build.user_token)

        if build.build_image_enabled:
            image_tag_set = self.build_image_tag_set.filter(build_id=private_build_id)
            for image_tag in image_tag_set:
                AlaudaCache.del_image_tag(image_tag.image_repo_id, image_tag.tag_name)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @tracing_response_time()
    def list(self, request):
        namespace = request.GET.get('namespace')
        config_id_list_str = request.query_params.get('uuids', '')
        config_id_list = config_id_list_str.strip(' ,').split(',')
        image_repo_id = request.query_params.get('image_repo_id', '')

        build_list_query_set = self.build_query_set.filter(config_id__in=config_id_list)
        if image_repo_id:
            build_list_query_set = build_list_query_set.filter(image_repo_id=image_repo_id)

        page = int(request.query_params.get('page', '0'))
        if page < 1:
            page = 1

        sub_build_list_query_set = build_list_query_set.exclude(
            status=STATUS_DELETED)[20 * (page - 1):20 * page]
        total_count = build_list_query_set.exclude(status=STATUS_DELETED).count()

        sub_build_list_serializer = BuildIncludeCodeSerializer(
            sub_build_list_query_set, many=True)
        sub_build_list = RazzilUtil.append_endpoint_name(sub_build_list_serializer.data)
        for build in sub_build_list:
            build['config_id'] = build['config']

        if Weblab.CI_INTEGRATION_SONARQUBE(namespace):
            self._append_sonarqube_build(sub_build_list)

        num_pages = total_count / 20
        if total_count % 20 != 0:
            num_pages += 1
        build_list_response = {
            'page_size': 20,
            'num_pages': num_pages,
            'result_list': sub_build_list
        }
        return Response(build_list_response)

    def _append_sonarqube_build(self, sub_build_list):
        ids = []
        for build in sub_build_list:
            ids.append(build['build_id'])
        query = self.build_integration_set.filter(build_id__in=ids, type=INTEGRATION_SONARQUBE)
        dic = {}
        for sonar_int in query:
            sonar_int_data = BuildIntegrationSerializer(sonar_int).data
            dic[sonar_int_data['build_id']] = sonar_int_data
        for build in sub_build_list:
            if dic.get(build['build_id']):
                build['sonarqube'] = dic.get(build['build_id'])

    @tracing_response_time()
    def retrieve(self, request, private_build_id):
        build = get_object_or_404(self.build_query_set, build_id=private_build_id)
        build_serializer = BuildIncludeCodeSerializer(build)
        result = RazzilUtil.append_endpoint_name(build_serializer.data)
        result['config_id'] = result['config']
        result['can_removed'] = Build.can_removed(
            result['status'], result['step_at'])
        if result['status'] in COMPLETE_STATUS:
            if result['artifact_upload_enabled']:
                try:
                    token = self.get_token(request)
                    artifacts = build.retrieve_artifacts(token)
                except Exception as ex:
                    LOG.error("Error happens when get build's artifacts: {}".
                              format(ex.message))
                    artifacts = []
                result['artifacts'] = artifacts
        if Weblab.CI_INTEGRATION_SONARQUBE(build.namespace):
            sonarqube = self._retrive_sonarqube_build_integration(private_build_id, build.namespace)
            if sonarqube:
                result['sonarqube'] = sonarqube
                if not result['can_removed'] and BuildIntegration.can_removed(
                    sonarqube['execution_status'], sonarqube['step_at']
                ):
                    result['can_removed'] = True # allow to remove when sonar in progress

        return Response(result)

    def _retrive_sonarqube_build_integration(self, private_build_id, namespace):
        query = self.build_integration_set.filter(
            build_id=private_build_id, type=INTEGRATION_SONARQUBE)
        if not query.exists():
            return
        sonarqube_build_integration = query.get()

        step_at = sonarqube_build_integration.step_at

        if step_at is not None and not SONARQUBE_STEP_VALUE.has_key(step_at):
            LOG.warning("unkonw sonarqube step_at:{}, build_id:{}".format(
                step_at, private_build_id))

        def need_pull_analysis_result():
            return step_at is not None and step_at == SONARQUBE_STEP_ANALYSIS \
             and not sonarqube_build_integration.integration_execution.get('analysis_result')

        if need_pull_analysis_result():
            LOG.info('''pull sonarqube analysis status, build_id:{}'''.format(private_build_id))
            with transaction.atomic():
                self._pull_sonarqube_alalysis_result(private_build_id, sonarqube_build_integration)

        data = BuildIntegrationSerializer(sonarqube_build_integration).data      
        try:
            integration = DavionClient.get_integration(
                sonarqube_build_integration.integration_instance_id)
            endpoint = integration['fields'].get('endpoint')
            if not endpoint:
                LOG.warning('endpoint should not be empty, integration_instance_id:{}'.format(
                    sonarqube_build_integration.integration_instance_id
                ))
            else:
                data['integration_execution']['report_url'] = "{}/dashboard?id={}".format(
                    endpoint, data['build_integration_config'].get('project_key'))
        except Exception as ex:
            LOG.warning("get integration from davion error:{}".format(traceback.format_exc()))

        return data

    def _pull_sonarqube_alalysis_result(self, private_build_id, sonarqube_build_integration):
        result, err = self._get_sonarqube_analysis_result(sonarqube_build_integration)
        if err:
            LOG.warning("build_id:{}, pull analysis result from sonarqube error:{}".format(
                private_build_id, err))
        if result and result.get('status'):
            LOG.info('Pulled analysis result:{} voluntary from sonarqube, build_id:{}'.format(
                result["status"], private_build_id))
            if result["status"] == "FAILED":
                sonarqube_build_integration.integration_execution['error'] = err
                sonarqube_build_integration.execution_status = STATUS_FAILED
                sonarqube_build_integration.step_at = SONARQUBE_STEP_COMPLETE
                sonarqube_build_integration.save()
                return
            if result["status"] == "SUCCESS":
                result.pop("status")
                sonarqube_build_integration.execution_status = STATUS_SUCCEEDED
                sonarqube_build_integration.step_at = SONARQUBE_STEP_COMPLETE
                sonarqube_build_integration.integration_execution['analysis_result'] = result
                sonarqube_build_integration.integration_execution['quality_gate_value'] = \
                    SonarClient.parsed_alert_status(result)
                sonarqube_build_integration.save()
                return

    def _get_sonarqube_analysis_result(self, sonarqube_build_integration):
        if not sonarqube_build_integration.integration_execution.get('task_id') \
            or not sonarqube_build_integration.build_integration_config.get('project_key'):
            return None, "sonarqube_build_integration error lost task_id or project_key"
        try:
            integration = DavionClient.get_integration(
                sonarqube_build_integration.integration_instance_id)
            client = RazzilUtil.new_sonar_client(integration)
            task_id = sonarqube_build_integration.integration_execution.get('task_id')
            data = client.get_analysis_task_info(task_id)
            if not data:
                LOG.warn("Pulled Nothing of SonarQube task info, build_id:{}, task_id:{}".format(
                    sonarqube_build_integration.build_id, task_id))
                return None, None
            if not data.get('task') or not data.get('task').get('status'):
                LOG.warn("Pulled sonarqube analysis task info data unexpected,\
                 build_id:{}, data: {}".format(sonarqube_build_integration.build_id, data))
                return None, None
            if data['task']['status'] == 'FAILED':
                return {"status":'FAILED'}, "SonarQube Analysis Failed:{}".format(
                    data['task']['errorMessage'])
            elif data['task']['status'] == "SUCCESS":
                result = client.get_analysis_result(
                    sonarqube_build_integration.build_integration_config['project_key'])
                if result:
                    result['status'] = "SUCCESS"
                    return result, None
                else:
                    return None, None
            elif data['task']['status'] == "IN_PROGRESS":
                LOG.info("Pulled sonarqube analysis task info status:in_progress, build_id:{}".format(
                    sonarqube_build_integration.build_id
                ))
                return None, None
            else:
                LOG.warn("Pulled sonarqube analysis task info data unexpected,\
                 build_id:{}, data: {}".format(sonarqube_build_integration.build_id, data))
                return None, None
        except Exception as ex:
            err = "Pulled sonarqube analysis result error:{}".format(ex)
            return None, err

    @tracing_response_time()
    def create(self, request):
        """
        Trigger build job
        """
        data = request.data
        if data.get('build_image_enabled') == True and \
                (not data.get('created_by') or
                 not data.get('docker_repo_path') or
                 not data.get('registry_index')):
            raise DockerBuildException(
                'miss_args',
                message_params='created_by, docker_repo_path, registry_index')

        if data.get('is_auto_created'):
            if not data.get('auto_build_enabled') or not data.get('external_data'):
                raise DockerBuildException(
                    'invalid_args',
                    message_params='auto_build_enabled={}'.format(data.get('auto_build_enabled')))
            RazzilUtil.parse_and_fufill_web_hook_data(data)

        self._set_runtime_params(data)
        
        filter_field = {'image_repo_id': data['image_repo_id']} \
            if data.get('build_image_enabled', True) else {'config__config_id': data['config_id']}

        build_locker = BuildSameCommitLocker(data, filter_field)
        if not build_locker.lock():
            build_id = build_locker.wait()
            # if wait last build timeout, we will allow current build
            if build_id:
                raise self.duplicate_commit_build_exception(build_id, data['code_repo']['code_head_commit'])

        # add build to queue
        build_list = RazzilUtil.get_not_finished_builds(filter_field)
        LOG.debug('not finished build list is {}'.format(build_list))
        is_new, old_build_id = self._is_new_build_valid(data, build_list)
        if is_new:
            with transaction.atomic():
                build = self._save_build(data)
                build_locker.unlock(build['build_id'])
            message = '[Init][Build] > create build {} success, will schedule it'.format(
                build['build_id']
            )
            log_client = self.get_es_client(namespace=build.get('namespace'),
                                            endpoint_id=build.get('endpoint_id'))
            log_client.save_build_log(build['build_id'], message=message)
        else:
            build_locker.unlock(old_build_id)
            raise self.duplicate_commit_build_exception(old_build_id, data['code_repo']['code_head_commit'])

        with transaction.atomic():
            RazzilUtil.handle_build_list(data)

        return Response({'build_id': build['build_id']}, status=status.HTTP_201_CREATED)

    def duplicate_commit_build_exception(self, build_id, commit_id):
        err = DockerBuildException('duplicate_commit_build',
                                        message_params=(build_id, commit_id),
                                )
        err.fields = [{'build_id': build_id}]
        return err

    @tracing_response_time()
    def retrieve_config(self, request, private_build_id):
        build = get_object_or_404(self.build_query_set, build_id=private_build_id)
        build_data = BuildSerializer(build).data

        config_data = {
            'config_id': build_data['config']
        }
        if build_data.get('ci_steps'):
            config_data['ci_steps'] = build_data['ci_steps']

        # get extra dockerfile steps
        if build.extra_dockerfile_enabled:
            build_steps = build.config.dockerfile.build_steps
            config_data['build_steps'] = build_steps

        return Response(config_data)

    @tracing_response_time()
    @transaction.atomic
    def update(self, request, private_build_id):
        build_status = request.data.get('status')
        step_at = request.data.get('step_at')
        dockerfile_content = request.data.get('dockerfile_content')
        image_tag = request.data.get('image_tag')
        docker_repo_label_tag = request.data.get('docker_repo_label_tag')
        code_head_commit = request.data.get('code_head_commit')
        ci_steps = request.data.get('ci_steps')
        image_id = request.data.get('image_id')
        build = get_object_or_404(self.build_query_set, build_id=private_build_id)
        build_code = get_object_or_404(self.build_code_query_set, build_id=private_build_id)
        LOG.info("updating build={} to status={} step_at={}".format(private_build_id, build_status, step_at))

        sonar_weblab = Weblab.CI_INTEGRATION_SONARQUBE(build.namespace)
        if sonar_weblab:
            sonar_data = request.data.get('sonarqube')
            if sonar_data:
                self._set_sonar_sync_job(private_build_id, sonar_data)
                with transaction.atomic():
                    self._update_sonarqube_build_integration(private_build_id,
                        request.data.pop('sonarqube'))
                return Response({},status=status.HTTP_200_OK)      
        # when customer didn't input commit id to build.
        # the gundar will call back to update the commit_id
        # update the build_code
        if code_head_commit:
            build_code.code_head_commit = code_head_commit
            build_code.save()
        build.docker_repo_version_tag = image_tag
        if docker_repo_label_tag:
            build.docker_repo_label_tag = docker_repo_label_tag
        if build_status or step_at:
            if build.status in COMPLETE_STATUS:
                build_status = None
                step_at = None
            elif STATUS_WEIGHTS[build_status] < STATUS_WEIGHTS[build.status] \
                    or step_at < build.step_at:
                build_status = None
                step_at = None
        # update the build

        if build_status:
            build.status = build_status
            if build_status in COMPLETE_STATUS:
                build.ended_at = timezone.now()
                if build_status != STATUS_DELETED:
                    RazzilUtil.delete_build_to_gundar(build.namespace,
                                                      str(build.build_id),
                                                      str(build.endpoint_id))
                    if build.notification_id:
                        NotificationClient.send_message(str(build.notification_id),
                                                        build_status, str(build.build_id))
                if build_status == STATUS_SUCCEEDED:
                    send_counter_metrics('build.succeeded', 1, {})
                elif build_status == STATUS_FAILED:
                    send_counter_metrics('build.failed', 1, {})

        if step_at:
            build.step_at = step_at

        if dockerfile_content:
            build.dockerfile_content = dockerfile_content

        if image_id:
            build.image_id = image_id

        if ci_steps:
            build.ci_steps = ci_steps 
        build.save()
        
        if build_status == STATUS_FAILED and sonar_weblab:
            RazzilUtil.check_to_set_sonarqube_fail(build.build_id, build.step_at)

        if build.status in COMPLETE_STATUS:
            with transaction.atomic():
                RazzilUtil.handle_build_list(build)
        LOG.info("updated build={} to status={}, step_at={}".format(private_build_id, build.status, build.step_at))
        return Response({"docker_repo_version_tag": build.docker_repo_version_tag},
                        status=status.HTTP_200_OK)
    
    def _set_sonar_sync_job(self, build_id, data):
        execution_status = data.get('execution_status')
        step_at = data.get('step_at')

        if execution_status and execution_status == STATUS_IN_PROGRESS and \
            step_at and step_at == SONARQUBE_STEP_ANALYSIS:
            
            sonarqube_build_integration = get_object_or_404(self.build_integration_set,
                build_id=build_id, type=INTEGRATION_SONARQUBE)
            if sonarqube_build_integration.sonar_status_sync_schedule_id:
                LOG.info('sonar status sync task with config id {} has existed, skip'.format(
                    sonarqube_build_integration.sonar_status_sync_schedule_id
                ))
                return

            integration_info = DavionClient.get_integration(
                sonarqube_build_integration.integration_instance_id
            )
            client = RazzilUtil.new_sonar_client(integration_info)
            if client.is_version60():
                LOG.info('set sonar sync task, build id {}, sonar version {}, sonar step {}'.format(
                    build_id,
                    client.version,
                    step_at
                ))
                try:
                    result = RazzilUtil._add_sonar_status_sync_scheduler(build_id)
                    config_id = result.get("id")
                    if config_id:
                        sonarqube_build_integration.sonar_status_sync_schedule_id = config_id
                        sonarqube_build_integration.save()
                    else:
                        err_msg = "result of create sonar sync task does not have id, result is {}".format(
                            result
                        )
                        LOG.error(err_msg)
                        raise Exception(err_msg)
                except Exception as ex:
                    LOG.error("add sonar sync scheduler error! build_id={} error ={}".format(
                        build_id,
                        ex
                    ))
                    raise

    def _update_sonarqube_build_integration(self, build_id, data):
        sonarqube_build_integration = get_object_or_404(self.build_integration_set,
            build_id=build_id, type=INTEGRATION_SONARQUBE)
        if sonarqube_build_integration.execution_status in COMPLETE_STATUS:
            LOG.warn("build:{} sonarqube is completed:{}, no need to update status: {}".format(
                build_id, sonarqube_build_integration.execution_status, data
            ))
            return

        execution_status = data.get('execution_status')
        project_key = sonarqube_build_integration.build_integration_config['project_key']
        step_at = data.get('step_at')

        if execution_status:
            if execution_status == STATUS_IN_PROGRESS  \
                and sonarqube_build_integration.execution_status == STATUS_WAITING:
                sonarqube_build_integration.started_at = get_now_cur_time_zone()
            if execution_status in COMPLETE_STATUS \
                and not sonarqube_build_integration.ended_at:
                sonarqube_build_integration.ended_at = get_now_cur_time_zone()
            if execution_status == STATUS_SUCCEEDED \
                and not sonarqube_build_integration.integration_execution.get("analysis_result"):
                # if we got exception here , we should keep it crash
                # it will pull analysis result at time of next build retriving
                integration = DavionClient.get_integration(
                    sonarqube_build_integration.integration_instance_id)
                client = RazzilUtil.new_sonar_client(integration)
                LOG.info("getting sonarqube analysis result build_id:{}".format(build_id))
                result = client.get_analysis_result(project_key)
                sonarqube_build_integration.integration_execution['analysis_result'] = result
                sonarqube_build_integration.integration_execution['quality_gate_value'] = \
                    SonarClient.parsed_alert_status(result)

            sonarqube_build_integration.execution_status = execution_status

        if step_at:
            sonarqube_build_integration.step_at = step_at
        if data.get('integration_execution'):
            execution = data.get('integration_execution')
            def _try_set_value(field_name):
                if execution.get(field_name):
                    sonarqube_build_integration.integration_execution[field_name] = execution.get(
                        field_name)
            
            _try_set_value('task_id')
            # there is no need to update gate value here
            # it will update when got alalysis result
            _try_set_value('error')
            _try_set_value('analysed_at')
        
        sonarqube_build_integration.save()

    @tracing_response_time()
    @transaction.atomic
    def create_build_integration(self, request, private_build_id):
        LOG.debug("create  sonarqube integration is :{}".format(request.data))
        build = get_object_or_404(self.build_query_set, build_id=private_build_id)
        sonarqube = request.data.get("sonarqube")
        if not sonarqube:
            return Response(status=status.HTTP_204_NO_CONTENT)
        query = self.build_integration_set.filter(
            build_id=private_build_id, type=INTEGRATION_SONARQUBE)
        if query.exists():
            raise DockerBuildException('build_integration_already_exist',
                message_params="sonarqube")
        data = sonarqube
        data["build_id"] = private_build_id
        data["type"] = INTEGRATION_SONARQUBE
        serializer = BuildIntegrationSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @tracing_response_time()
    def delete(self, request, private_build_id):
        build = get_object_or_404(self.build_query_set, build_id=private_build_id)
        RazzilUtil.delete_in_progress_build_to_gundar(build.status, build.namespace,
                                                      str(build.build_id), build.endpoint_id)
        sonar_weblab = Weblab.CI_INTEGRATION_SONARQUBE(build.namespace)
        with transaction.atomic():
            build.status = STATUS_DELETED
            build.ended_at = get_now_cur_time_zone()
            build.save()
            if sonar_weblab:
                query = self.build_integration_set.filter(build_id=private_build_id, type=INTEGRATION_SONARQUBE)
                if query.exists():
                    sonarqube_build_integration = query.get()
                    sonarqube_build_integration.execution_status = STATUS_DELETED
                    sonarqube_build_integration.ended_at = get_now_cur_time_zone()
                    sonarqube_build_integration.save()
        with transaction.atomic():
            RazzilUtil.handle_build_list(build)
        if build.status_sync_schedule_id:
            try:
                MedusaClient.delete_schedule_event_config(build.status_sync_schedule_id)
            except Exception as ex:
                LOG.error('''delete sync schedule_event_config error ,
                build_id = {}, config_id ={}, err ={}'''.format(
                    private_build_id, build.status_sync_schedule_id, ex
                ))
        return Response(status=status.HTTP_204_NO_CONTENT)

    @tracing_response_time()
    def retrieve_logs(self, request, private_build_id):
        build = get_object_or_404(self.build_query_set, build_id=private_build_id)
        start_time = request.query_params.get('start_time')
        end_time = request.query_params.get('end_time')
        limit = int(request.query_params.get('limit', 2000))
        if not start_time and not end_time:
            start_time = time.mktime(build.created_at.timetuple()) - 3600
            end_time = start_time + TIMESPAN_LIMIT
        else:
            if not start_time:
                start_time = end_time - TIMESPAN_LIMIT
            elif not end_time:
                end_time = start_time + TIMESPAN_LIMIT

        client = self.get_es_client(namespace=build.namespace, 
                                     endpoint_id=build.endpoint_id)

        type = request.query_params.get('type')
        if not type:
            _, logs_list = client.get_build_log(build_id=private_build_id,
                                                start_time=int(start_time),
                                                end_time=int(end_time),
                                                limit=limit)
            return Response(logs_list)
        if type == "sonarqube":
            _, logs_list = client.get_sonarqube_build_log(build_id=private_build_id,
                                                          start_time=int(start_time),
                                                          end_time=int(end_time),
                                                          limit=limit)
            return Response(logs_list)
        raise DockerBuildException('invalid_args', message_params='type')

    # save a build based on build_config with the input status
    def _save_build(self, data):
        code_repo = data.pop('code_repo')
        runtime_params = data.pop('runtime_params')

        # auto trigger by pipeline or others.
        auto_trigger = data.get('auto_trigger')
        if auto_trigger:
            data['is_auto_created'] = True

        data['status'] = STATUS_BLOCKED
        data['docker_repo_version_tag'] = self._generate_auto_tag(
            data.get('auto_tag_type'), code_repo.get('code_head_commit'))
        data['docker_repo_tag'] = data.get('customize_tag')
        data['build_cache_enabled'] = data.get('image_cache_enabled')
        data['config'] = data['config_id']
        data.pop('config_id')
        build_serializer = BuildSerializer(data=data)
        build_serializer.is_valid(raise_exception=True)
        build_serializer.save()

        code_repo['build'] = build_serializer.data['build_id']
        if runtime_params and runtime_params.get("code_repo_type_value"):
            code_repo["code_repo_type_value"] = runtime_params["code_repo_type_value"]
        build_code_serializer = BuildCodeSerializer(data=code_repo)
        build_code_serializer.is_valid(raise_exception=True)
        build_code_serializer.save()

        if data.get('sonarqube'):
            sonarqube = data.pop('sonarqube')
            sonarqube['type'] = INTEGRATION_SONARQUBE
            sonarqube['build_id'] = build_serializer.data['build_id']
            sonarqube['execution_status'] = STATUS_BLOCKED
            sonarqube['integration_execution'] = {}
            build_integration_serizlizer = BuildIntegrationSerializer(data=sonarqube)
            build_integration_serizlizer.is_valid(raise_exception=True)
            build_integration_serizlizer.save()

        return build_serializer.data

    def _generate_auto_tag(self, auto_tag_type, code_head_commit):
        if auto_tag_type == AUTO_TAG_TYPE_TIME:
            now = get_now_cur_time_zone()
            return 'v{}'.format(now.strftime("%Y%m%d.%H%M%S"))
        elif auto_tag_type == AUTO_TAG_TYPE_COMMIT:
            return code_head_commit
        else:
            return ''

    def _is_new_build_valid(self, data, build_list):
        new_build_commit = data['code_repo'].get('code_head_commit')
        if not new_build_commit:
            return True, None
        for build in build_list:
            build_commit = build['code_repo'].get('code_head_commit')
            if not build_commit:
                continue
            # svn code_head_commit in data is type of INT
            if str(new_build_commit) == build_commit:
                return False, build.get('build_id')

        return True, None

    def _set_runtime_params(self, data):
        if data.get("code_repo").get("code_repo_client") == CODE_REPO_CLIENT_SIMPLE_SVN:
            data["runtime_params"] = {
                "code_repo_type_value":None
            }
            return
        # valid code_repo_type_value is match code_repo_type_value in build_config
        runtime_code_repo_type_value = data.get("runtime_params").get("code_repo_type_value") if data.get("runtime_params") is not None else None
        valid, runtime_code_repo_type_value, err = BuildConfigCode.valid_runtime_code_repo_type_value(
            data.get("code_repo").get("code_repo_type_value"), runtime_code_repo_type_value)
        if not valid:
            raise DockerBuildException('match_branch_wilcard_error', message_params=err)
        # set runtime_code_repo_type_value    
        if data.get("runtime_params") is None:
            data["runtime_params"] = {}
        data['runtime_params']['code_repo_type_value'] = runtime_code_repo_type_value

    def get_token(self, request):
        token = request.META.get('HTTP_TOKEN')
        return token

    @tracing_response_time()
    def sync_sonar_task_status(self, request, private_build_id):
        """This method aims to sync sonar analysis task result to db.
        It is used for sonar 6.0.0
        """
        return_data = ""
        LOG.info("sync sonar task status , build_id = {}".format(private_build_id))
        if not self.build_query_set.filter(build_id = private_build_id).exists():
            return_data = '''sync sonar task status:  DIRTY DATA !!
             sync_status_scheduler should be deleted when build job is deleted'''
            LOG.error(return_data)
            return Response(data=return_data, status=status.HTTP_410_GONE)
        build = get_object_or_404(self.build_query_set, build_id = private_build_id)
        
        sonar_build_integration = get_object_or_404(
            self.build_integration_set,
            build_id=private_build_id,
            type=INTEGRATION_SONARQUBE)
        integration = DavionClient.get_integration(
                    sonar_build_integration.integration_instance_id)

        # if sonarqube crashed long time, razzil will response 500 and medusa will schedule for loop,
        # we add max fail count to control medusa loop time. fail count > max count will mark task fail
        # and delete medusa schedule.
        status_sonar_sync_fail_count = sonar_build_integration.status_sonar_sync_fail_count
        try:
            client = RazzilUtil.new_sonar_client(integration)
        except Exception as e:
            status_sonar_sync_fail_count = status_sonar_sync_fail_count + 1
            self.build_integration_set.filter(build_id=private_build_id, type=INTEGRATION_SONARQUBE).update(
                status_sonar_sync_fail_count = status_sonar_sync_fail_count
            )
            if status_sonar_sync_fail_count > MAX_SONAR_STATUS_SYNC_FAIL_COUNT:
                return_data = '''new sonar client fail up to max count {},
                set build {} status to fail, return 410 to delete medusa task'''.format(
                    MAX_SONAR_STATUS_SYNC_FAIL_COUNT,
                    private_build_id
                )
                err_msg = '''new sonar client fail up to max count {},
                set build {} status to fail'''.format(
                    MAX_SONAR_STATUS_SYNC_FAIL_COUNT,
                    private_build_id
                )
                LOG.error(return_data)
                # mark sonar task status fail
                data = {
                    "integration_execution": {
                        "error": err_msg,
                    },
                    "execution_status": STATUS_FAILED
                }
                self._update_sonarqube_build_integration(private_build_id, data)
                return Response(data=return_data, status = status.HTTP_410_GONE)
            return_data = 'new sonar client error {}, wait next schedule'.format(e)
            LOG.error(return_data)
            return Response(data=return_data, status=status.HTTP_200_OK)

        if status_sonar_sync_fail_count != 0:
            self.build_integration_set.filter(build_id=private_build_id, type=INTEGRATION_SONARQUBE).update(
                status_sonar_sync_fail_count=0
            )

        sonar_build_status = sonar_build_integration.execution_status
        if sonar_build_status in [STATUS_BLOCKED, STATUS_WAITING]: #['B', 'W']
            return_data = 'sonar task status is {}, build id {} wait next schedule'.format(
                sonar_build_status, private_build_id)
            LOG.info(return_data)
            return Response(data=return_data, status=status.HTTP_200_OK)
        elif sonar_build_status in COMPLETE_STATUS: #['D', 'F', 'S']
            return_data = 'sonar task status is {}, build id {}, stop schedule'.format(
                sonar_build_status, private_build_id)
            LOG.info(return_data)
            return Response(data=return_data, status=status.HTTP_410_GONE)
        elif sonar_build_status in [STATUS_IN_PROGRESS]:
            sonar_step = sonar_build_integration.step_at
            if sonar_step in [SONARQUBE_STEP_COLLECT, SONARQUBE_STEP_ANALYSIS]: #['COLLECT-DATA', 'ANALYSIS']
                # task_id has been updated in db
                task_id = sonar_build_integration.integration_execution.get('task_id', "")
                if not task_id:
                    return_data = 'task id from sonar build integration {} should not be empty'.format(
                        sonar_build_integration
                    )
                    LOG.error(return_data)
                    return Response(data=return_data, status=status.HTTP_200_OK)
                
                data = self.retrieve_sonarqube_analyais_result(private_build_id, task_id, client)
                if data['execution_status'] == "PENDING":
                    return_data = 'sonar task status is {}, build id {}, wait next schedule'.format(
                        sonar_task_status, private_build_id)
                    LOG.info(return_data)
                    return Response(data=return_data, status=status.HTTP_200_OK)

                self._update_sonarqube_build_integration(private_build_id, data)
                return_data = '''sync sonar task status complete, told medusa to delete task.
                build id {}, task id {}, execution_status {}, err_msg {}'''.format(
                    private_build_id, task_id, execution_status, err_msg
                )
                LOG.info(return_data)
                return Response(data=return_data, status=status.HTTP_410_GONE)
            else:
                return_data = 'sonar task step is {}, wait next schedule'.format(sonar_step)
                LOG.info(return_data)
                return Response(data=return_data, status=status.HTTP_200_OK)
        else:
            return_data = 'sonar task status {} is unknown, stop schedule'.format(
                sonar_build_status)
            LOG.error(return_data)
            return Response(data=return_data, status=status.HTTP_410_GONE)

    @tracing_response_time()
    def retrieve_sonarqube_analyais_result(self, build_id, task_id, sonar_client):
        task_info = sonar_client.get_analysis_task_info(task_id)['task']
        err_msg, quality_gate_value = None, ""
        sonar_task_status = task_info["status"]
        
        if sonar_task_status == "PENDING":
            execution_status = "PENDING"
            return {
                "integration_execution": {
                    "task_id": task_id
                },
                "execution_status": execution_status,
            }
        elif sonar_task_status == "SUCCESS":
            execution_status = STATUS_SUCCEEDED # 'S'
            quality_gate_value = "OK"
        elif sonar_task_status == "FAILED":
            execution_status = STATUS_FAILED # 'F'
            err_msg = "SonarQube Analysis Error"
        else:
            execution_status = sonar_task_status
            return_data = err_msg = "sonar task status is unknown: {}".format(sonar_task_status)
            LOG.error(err_msg)

        data = {
            "integration_execution": {
                "quality_gate_value": quality_gate_value,
                "error": err_msg,
                "analysed_at": task_info["executedAt"],
                "task_id": task_id
            },
            "execution_status": execution_status,
            "step_at": SONARQUBE_STEP_COMPLETE
        }

        return data

    @tracing_response_time()
    def sync_status(self, request, private_build_id):
        LOG.info("sync status , build_id = {}".format(private_build_id))
        if not self.build_query_set.filter(build_id = private_build_id).exists():
            LOG.error(''' sync status:  DIRTY DATA !!
             sync_status_scheduler should be deleted when build job is deleted''')
            return Response(status=status.HTTP_410_GONE)

        build = get_object_or_404(self.build_query_set, build_id = private_build_id)
        if build.status in COMPLETE_STATUS:
            LOG.info(
                "sync status, build job {} complete ({}), told medusa to delete task ".format(
                    private_build_id, build.status))
            return Response(status=status.HTTP_410_GONE)

        LOG.info("sync status, job is still running, build_id={}, status={}, step_at={}".format(
            private_build_id, build.status, build.step_at
        ))
        #  ci build is still running 
        status_sync_fail_count = build.status_sync_fail_count
        try:
            endpoint = RazzilUtil.get_endpoint(build.namespace, build.endpoint_id)
            params = {}
            if not endpoint.is_public:
                params['region_id'] = endpoint.region_id
            job = TresdinClient.retrieve('private_build_' + private_build_id, params=params)
        except Exception as ex:
            # we should update instead of save to prevent current `build` keeping some overdue data
            status_sync_fail_count = status_sync_fail_count + 1
            self.build_query_set.filter(build_id=private_build_id).update(
                status_sync_fail_count=status_sync_fail_count
            )

            LOG.warning('''sync status retrieve build={} job status fail continuous, count = {}
            fail exception {} '''.format(build.build_id, status_sync_fail_count, ex))
            if status_sync_fail_count >= MAX_STATUS_SYNC_FAIL_COUNT:
                LOG.warning('''sync status retrieve job status fail up to max count {}, 
                set build {} status to fail, return 410 to delete medusa task'''.format(
                    MAX_STATUS_SYNC_FAIL_COUNT,
                    private_build_id
                    ))
                with transaction.atomic():
                    self._set_to_fail(build)
                with transaction.atomic():
                    RazzilUtil.handle_build_list(build)
                return Response(status = status.HTTP_410_GONE)
            return Response("tresdin job on fail",status=status.HTTP_200_OK)

        if status_sync_fail_count != 0:
            self.build_query_set.filter(build_id=private_build_id).update(
                status_sync_fail_count=0
            )

        # job is completed but build is not completed
        if is_complete_job_stage(job.get('status')):
            LOG.info('''sync status, tresdin job is completed , 
            force to set build {} to completed, return 410 told medus del task'''.format(private_build_id))
            with transaction.atomic():
                self._set_to_fail(build)
            with transaction.atomic():
                RazzilUtil.handle_build_list(build)
            return Response(status=status.HTTP_410_GONE)
        
        # tresdin job is running and build is running also
        res = RazzilUtil._remove_timeout_build(BuildSerializer(build).data)
        LOG.info("sync status, build job timeout ? = {} ".format(res))
        with transaction.atomic():
            RazzilUtil.handle_build_list(build)
        return Response("normal", status=status.HTTP_200_OK)

    def _set_to_fail(self, db_build):
        self.build_query_set.filter(build_id = db_build.build_id).exclude(status__in=COMPLETE_STATUS).update(
            status = STATUS_FAILED,
            ended_at = get_now_cur_time_zone()
        )
        if Weblab.CI_INTEGRATION_SONARQUBE(db_build.namespace):
            RazzilUtil.check_to_set_sonarqube_fail(db_build.build_id, db_build.step_at)

        try:
            RazzilUtil.delete_build_to_gundar(
                db_build.namespace,
                db_build.build_id,
                db_build.endpoint_id
            )
        except Exception as ex:
            LOG.error(
                "delete build to gundar error, build_id={} error = {}".format(
                    db_build.build_id, ex))

    @tracing_response_time()
    def list_sonarqube_languages(self, request, namespace, integration_id):
        integration = DavionClient.get_integration(integration_id)
        client = RazzilUtil.new_sonar_client(integration)
        data = client.get_language_list()
        return Response(data, status=status.HTTP_200_OK)

    @tracing_response_time()
    def list_sonarqube_qualitygates(self, request, namespace, integration_id):
        integration = DavionClient.get_integration(integration_id)
        client = RazzilUtil.new_sonar_client(integration)
        data = client.get_qualitygates_list()
        return Response(data, status=status.HTTP_200_OK)
    
    @tracing_response_time()
    def dashboard(self, request):
        start_time = request.GET.get('start_time')
        end_time = request.GET.get('end_time')
        config_id_list_str = request.query_params.get('uuids', '')
        config_id_list = config_id_list_str.strip(' ,').split(',')

        if not end_time:
            end_time = time.time()
        else:
            end_time = int(end_time)
        end_datetime = datetime.datetime.utcfromtimestamp(end_time).replace(
            tzinfo=pytz.utc).astimezone(pytz.timezone(settings.CUR_TIME_ZONE))
        if not start_time:
            start_date = (end_datetime - datetime.timedelta(days=6)).date()
            start_datetime = datetime.datetime.strptime(str(start_date), '%Y-%m-%d').replace(
                tzinfo=pytz.timezone(settings.CUR_TIME_ZONE))
        else:
            start_time = int(start_time)
            start_datetime = datetime.datetime.utcfromtimestamp(start_time).replace(
                tzinfo=pytz.utc).astimezone(pytz.timezone(settings.CUR_TIME_ZONE))

        result ={}
        all_count = 0
        if config_id_list:
            query_set = self.build_query_set.values("status", "created_at").filter(
                config_id__in=config_id_list,
                created_at__gte=start_datetime,
                created_at__lte=end_datetime).exclude(
                    status=STATUS_DELETED)
            logging.info(query_set.query)
            all_count = query_set.count()
            for build in query_set:
                date = build["created_at"].astimezone(pytz.timezone(settings.CUR_TIME_ZONE)).date()
                if not result.get(date):
                    result[date] = {
                        DASHBOARD_OK: 0,
                        DASHBOARD_ERROR: 0,
                        DASHBOARD_OTHER: 0
                    }
                if build["status"] == STATUS_SUCCEEDED:
                    result[date][DASHBOARD_OK] = result[date][DASHBOARD_OK] + 1
                elif build["status"] == STATUS_FAILED:
                    result[date][DASHBOARD_ERROR] = result[date][DASHBOARD_ERROR] + 1
                else:
                    result[date][DASHBOARD_OTHER] = result[date][DASHBOARD_OTHER]+1

        count_result = []
        days = RazzilUtil.split_as_days(start_datetime, end_datetime)
        for day in days:
            date = day["date"]
            count_result.append({
                DASHBOARD_OK:{
                    "custom_status": STATUS_SUCCEEDED,
                    "count": 0 if not result.get(date) else result[date][DASHBOARD_OK]
                },
                DASHBOARD_ERROR:{
                    "custom_status": STATUS_FAILED,
                    "count": 0 if not result.get(date) else result[date][DASHBOARD_ERROR]
                },
                DASHBOARD_OTHER:{
                    "custom_status": "other",
                    "count": 0 if not result.get(date) else result[date][DASHBOARD_OTHER]
                },
                "start": day["start"].strftime("%Y-%m-%dT%H:%M:%SZ"),
                "end": day["end"].strftime("%Y-%m-%dT%H:%M:%SZ")
            })
        return Response({
            "count": all_count,
            "unit": "day",
            "count_result": count_result
        }, status=status.HTTP_200_OK)

    def get_es_client(self, namespace, endpoint_id=None):
        """
        Return elasticsearch client by config.

        :param namespace: namespace of build
        :type namespace: str
        :param endpoint_id: endpoint_id
        :type endpint_id: str
        :returns: elasticsearch client
        :rtype: ElasticSearchClient
        """
        data = []
        try:
            endpoint_result = RazzilUtil.get_endpoint(namespace=namespace,
                                                      endpoint_id=endpoint_id)
            if not endpoint_result.is_public:
                data = FurionClient.get_log_source(endpoint_result.region_id)['data']['read_log_source']
        except Exception as e:
            msg = 'get log source info error {}'.format(e.message)
            LOG.error(msg)

        # here we get three log sources, their priority: data(furion) > endpoint_result(razzil buildendpoint) > settings
        if data:
            if data[0]['type'] == 'default':
                return ElasticSearchClient()
            
            return ElasticSearchClient(data[0].get('query_address', None),
                                       data[0].get('username', None),
                                       data[0].get('password', None))

        if endpoint_result.es_host:
            return ElasticSearchClient(endpoint_result.es_host,
                                       endpoint_result.es_username,
                                       endpoint_result.es_password)

        # if endpoint_result = RazzilUtil.get_endpoint() get exception
        # we will miss settings config
        return ElasticSearchClient()

    @tracing_response_time()
    def retrieve_ci_report(self, request, private_build_id):
        LOG.info('Get ci_report private_build_id: {}'.format(private_build_id))
        build = get_object_or_404(self.build_query_set, build_id=private_build_id)
        if not build.ci_report:
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(json.loads(build.ci_report))

    @tracing_response_time()
    def ci_report(self, request, private_build_id):
        LOG.info('creat_ci_report private_build_id: {}, data: {}'
                 .format(private_build_id, request.data))
        build = get_object_or_404(self.build_query_set, build_id=private_build_id)
        build.ci_report = json.dumps(request.data)
        build.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

def is_complete_job_stage(tresdin_job_status):
    return tresdin_job_status == TresdinClient.STATUS_SUCCEEDED \
        or tresdin_job_status == TresdinClient.STATUS_FAILED
