from commons.docker_build_exception import DockerBuildException
from private_build_configs.models import (CODE_REPO_CLIENT_SIMPLE_GIT,
                                          CODE_REPO_CLIENT_SIMPLE_SVN,
                                          CODE_REPO_CLIENT_GITHUB,
                                          CODE_REPO_CLIENT_BITBUCKET,
                                          CODE_REPO_CLIENT_OSCHINA)

from .bitbucket_client import BitbucketClient
from .github_client import GitHubClient
from .simple_git_client import SimpleGitClient
from .simple_svn_client import SimpleSVNClient
from .oschina_client import OSChinaClient


class CodeClientFactory(object):

    @staticmethod
    def create_code_client(code_client_name, namespace=None):
        if code_client_name == CODE_REPO_CLIENT_SIMPLE_GIT:
            return SimpleGitClient()
        elif code_client_name == CODE_REPO_CLIENT_SIMPLE_SVN:
            return SimpleSVNClient()
        elif code_client_name == CODE_REPO_CLIENT_GITHUB:
            github_client = GitHubClient()
            if namespace:
                github_client.load(namespace)
            return github_client
        elif code_client_name == CODE_REPO_CLIENT_BITBUCKET:
            bitbucket_client = BitbucketClient()
            if namespace:
                bitbucket_client.load(namespace)
            return bitbucket_client
        elif code_client_name == CODE_REPO_CLIENT_OSCHINA:
            oschina_client = OSChinaClient()
            if namespace:
                oschina_client.load(namespace)
            return oschina_client
        else:
            raise DockerBuildException(
                'invalid_code_client', message_params=code_client_name)
