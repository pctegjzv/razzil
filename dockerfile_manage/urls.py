from django.conf.urls import url

from dockerfile_manage.views import DockerfileViewSet
from commons.constants import APPLICATION_TEMPLATE_URL_REGEX

urlpatterns = [
    url(r'^/?$', DockerfileViewSet.as_view({
        'get': 'list',
        'post': 'create'
    })),
    url(r'^(?P<file_name>{})/?$'.format(APPLICATION_TEMPLATE_URL_REGEX),
        DockerfileViewSet.as_view({
            'put': 'update',
            'delete': 'delete'
        }))
]
