# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-07 03:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('private_build_configs', '0014_auto_20170327_1054'),
    ]

    operations = [
        migrations.AddField(
            model_name='buildconfigcode',
            name='head_commit_id',
            field=models.CharField(max_length=128, null=True),
        ),
    ]
