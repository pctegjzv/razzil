import uuid
from django.core.management.base import BaseCommand
from private_builds.models import BuildEndpoint
from config.settings import RATTLETRAP_IMAGE


class Command(BaseCommand):
    help = 'Setup razzil build_mage. python manage.py setup_rattletrap '\
           '[--image rattletrap_image]'

    def add_arguments(self, parser):
        parser.add_argument('--image', type=str, help='Rattletrap image')

    def handle(self, *args, **options):
        image = options['image'] if options.get('image') else RATTLETRAP_IMAGE
        self._valid_image(image)
        image = image.strip()
        endpoints = BuildEndpoint.objects.all()
        if not endpoints.exists():
            BuildEndpoint.objects.create(
                endpoint_id=str(uuid.uuid4()),
                namespace='private_build',
                endpoint='http://0.0.0.0:4400',
                builder_image=image,
                es_host='',
                es_username='',
                es_password='',
                proxy_config='{}',
                region_id=str(uuid.uuid4()),
                is_public=False
            )
            print "Init endpoint success."
            print "Current rattletrap image: {}".format(image)
        else:
            current_image = endpoints[0].builder_image
            print "> Current rattletrap image is {}".format(current_image)
            print "> The new image is {}".format(image)
            endpoints.update(builder_image=image)
            print "Update all builder_image success"

    @staticmethod
    def _valid_image(image):
        '''valid image'''
        if not image:
            raise Exception('rattletrap image not specific')
