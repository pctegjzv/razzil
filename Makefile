OS = Linux

CURDIR = $(shell pwd)
SOURCEDIR = $(CURDIR)

ECHO = echo
RM = rm -rf
MKDIR = mkdir
FLAKE8 = flake8
PEP8-NAMING = pep8-naming
PIP_INSTALL = pip install
RUN_MOCK_TESTS = python manage.py test

.PHONY: setup build test help

all: setup build test

setup:
	$(PIP_INSTALL) $(FLAKE8) $(PEP8-NAMING)

lint:
	$(FLAKE8) $(SOURCEDIR) --show-source --statistics --count

test:
	$(RUN_MOCK_TESTS)

help:
	@$(ECHO) "Targets:"
	@$(ECHO) "all     - setup, build and test"
	@$(ECHO) "setup   - set up prerequisites for build"
	@$(ECHO) "lint    - perform static analysis"
