# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-11-13 09:46
from __future__ import unicode_literals

from django.db import migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('private_builds', '0008_auto_20161101_0728'),
    ]

    operations = [
        migrations.AddField(
            model_name='buildendpoint',
            name='proxy_config',
            field=jsonfield.fields.JSONField(blank=True, default={}),
        ),
    ]
