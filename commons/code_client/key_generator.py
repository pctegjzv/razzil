from paramiko.rsakey import RSAKey
from paramiko.py3compat import u, encodebytes


class KeyGenerator:
    def __init__(self):
        pass

    @staticmethod
    def create_ssh_key(bits):
        key = RSAKey.generate(bits=bits)
        private_key = KeyGenerator._get_private_key(key)
        public_key = KeyGenerator._get_public_key(key)
        return {
            'private_key': private_key,
            'public_key': public_key
        }

    @staticmethod
    def _get_private_key(key):
        s = u(encodebytes(key._encode_key()))
        # re-wrap to 64-char lines
        s = ''.join(s.split('\n'))
        s = '\n'.join([s[i: i + 64] for i in range(0, len(s), 64)])
        private_key_format = '-----BEGIN RSA PRIVATE KEY-----\n{}\n-----END RSA PRIVATE KEY-----'
        return private_key_format.format(s)

    @staticmethod
    def _get_public_key(key):
        return '{} {}'.format(key.get_name(), key.get_base64())
