# 如何使用
## 数据备份
- 备份 razzildb  的 private_build_configs_buildconfig 表数据

## 参数
-s  指定原来的CI镜像名称， 例如 java:jdk7  
-t  指定想要修改成的CI镜像名称，例如java:jdk7-scan

执行 `python change_ci_image.py -s java:jdk7 -t java:jdk7-scan`

# 执行过程

- 每次遍历20条数据，进行修改
- 修改过程会提示修了哪些数据
- 最后会询问是否 提交事务，写入数据库。输入Y即可。其他字符则回滚事务。
- 事务提交后，会自动check 一遍修改的数据，输出检查结果。
- 如果二次检查后发现仍有旧数据，则手动在run一遍即可。

日志示例如下 (调试使用，每次遍历 3条数据)
```
root@e4b89840356c:/razzil/scripts# python change_ci_image.py -s node:6.4 -t node:6.3
2017-10-31 02:09:09,846 [INFO][MainThread][__main__:129] Begin change ci image name node-> node  6.4 -> 6.3 in build config
Continue Y/N?Y
2017-10-31 02:09:14,509 [INFO][MainThread][__main__:52] Migrate: 0 - 2
2017-10-31 02:09:14,544 [INFO][MainThread][__main__:62] 47afb1a8-c046-4746-8387-4ddfc9608b48 lang node->node runtime_name 6.4->6.3
2017-10-31 02:09:14,579 [INFO][MainThread][__main__:62] 7731b01d-5c83-49d8-a426-04fa55ce2bb1 lang node->node runtime_name 6.4->6.3
2017-10-31 02:09:14,613 [INFO][MainThread][__main__:62] 942fe585-0051-4be7-8ebb-4232a2585a86 lang node->node runtime_name 6.4->6.3
2017-10-31 02:09:14,680 [INFO][MainThread][__main__:52] Migrate: 3 - 5
2017-10-31 02:09:14,715 [INFO][MainThread][__main__:62] 1c7c0df1-4337-433a-a48b-701e3224ddef lang node->node runtime_name 6.4->6.3
2017-10-31 02:09:14,782 [INFO][MainThread][__main__:52] Migrate: 6 - 8
Commit Transaction Y/N ?Y
2017-10-31 02:09:26,912 [INFO][MainThread][root:72] Commiting...
2017-10-31 02:09:26,965 [INFO][MainThread][root:89] Begin check migrating result
2017-10-31 02:09:27,068 [INFO][MainThread][__main__:96] Check done: unpass_count=0, SUCCESS
2017-10-31 02:09:27,068 [INFO][MainThread][__main__:134] All Data Migrated Success
```

