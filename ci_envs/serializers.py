from rest_framework import serializers

from .models import CIEnvsCatalog


class CIEnvsCatalogSerializer(serializers.ModelSerializer):

    class Meta:
        model = CIEnvsCatalog
        exclude = ('image', 'tag')
