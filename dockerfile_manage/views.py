import logging

from rest_framework import status, viewsets
from commons.serializers import UUIDListSerializer
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from commons.docker_build_exception import DockerBuildException

from .models import Dockerfile
from dockerfile_manage.serializers import (DockerfileSerializer,
                                           DockerfileExcludeSerializer,
                                           DockerfileUpdateSerializer)

logger = logging.getLogger(__name__)


class DockerfileViewSet(viewsets.ViewSet):
    dockerfile_query_set = Dockerfile.objects.all()

    def list(self, request):
        uuid_querystr = request.query_params.get('uuids', '')
        logger.info("get dockerfile list uuids: {}".format(uuid_querystr))
        filter_args = {}
        if uuid_querystr:
            dockerfile_uuids = uuid_querystr.strip(' ,').split(',')
            serializer = UUIDListSerializer(data={'uuids': dockerfile_uuids})
            serializer.is_valid(raise_exception=True)
            dockerfile_uuids = serializer.data['uuids']
            filter_args['uuid__in'] = dockerfile_uuids
        dockerfiles = self.dockerfile_query_set.filter(**filter_args)
        serializer = DockerfileExcludeSerializer(dockerfiles, many=True)
        return Response(serializer.data)

    def create(self, request):
        logger.info('save data is: {}'.format(request.data))
        serializer = DockerfileSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({'uuid': serializer.data.get('uuid')}, status=status.HTTP_201_CREATED)

    def update(self, request, file_name):
        logger.info('update dockerfile with file_name: {}'.format(file_name))
        dockerfile = get_object_or_404(self.dockerfile_query_set, name=file_name)
        serializer = DockerfileUpdateSerializer(dockerfile, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def delete(self, request, file_name):
        logger.info('delete dockerfile with file_name: {}'.format(file_name))
        if self.dockerfile_query_set.filter(name=file_name).exists():
            dockerfile = self.dockerfile_query_set.filter(name=file_name).first()
            if dockerfile.buildconfig_set.exists():
                build_configs = dockerfile.buildconfig_set.values_list('config_id', flat=True)
                raise DockerBuildException('dockerfile_already_used',
                                           message_params=(file_name, build_configs))
            dockerfile.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
