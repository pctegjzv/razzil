class WebHookData(object):

    def __init__(self, code_repo_type, code_repo_type_value, code_head_commit):
        self.code_repo_type = code_repo_type
        self.code_repo_type_value = code_repo_type_value
        self.code_head_commit = code_head_commit

    def get_code_repo_type(self):
        return self.code_repo_type

    def get_code_repo_type_value(self):
        return self.code_repo_type_value

    def get_code_head_commit(self):
        return self.code_head_commit
