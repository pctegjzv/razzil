from rest_framework.test import APITestCase
from private_builds.models import Build, BuildCode, BuildEndpoint, STATUS_WAITING, BuildIntegration, SONARQUBE_STEP_ANALYSIS
from private_build_configs.models import BuildConfig, BuildConfigCode
from private_builds.build_client.tresdin_client import TresdinClient
from private_builds.notification_client.notification_client import NotificationClient
from commons.furion_client import FurionUtil
from commons.medusa_client import MedusaClient
from alauda.weblab import WeblabClient
from tests.api.common import WebLabResult
from private_build_configs.models import INTEGRATION_SONARQUBE
from private_builds.serializers import BuildIntegrationSerializer
from commons.util import RazzilUtil
from commons.davion_client import DavionClient
import mock

json_create_build_config = {
    "namespace": "test",    
    "name":"json_create_build_config",
    "image_repo_id": "798f19e7-f99c-452a-82a5-66728c54113e",
    "code_repo": {
        "code_repo_client": "SIMPLE_GIT",
        "code_repo_path": "git@52.24.16.115:root/testCodeRepo.git",
        "code_repo_type": "BRANCH",
        "code_repo_type_value": "master",
        "dockerfile_location": "/",
        "build_context_path": "/"
    },
    "created_by": "liaoj",
    "customize_tag": "latest",
    "auto_tag_type": "TIME",
    "image_cache_enabled": True,
    "auto_build_enabled": True,
    "notification_id": "e98f19e7-f99c-452a-82a5-66728c54113e"
}

json_create_build_config_wilcard = {
    "namespace": "test",
    "name": "json_create_build_config_wilcard",
    "image_repo_id": "698f19e7-f99c-452a-82a5-66728c54113e",
    "code_repo": {
        "code_repo_client": "SIMPLE_GIT",
        "code_repo_path": "git@52.24.16.115:root/testCodeRepo.git",
        "code_repo_type": "BRANCH",
        "code_repo_type_value": "master/*",
        "dockerfile_location": "/",
        "build_context_path": "/"
    },
    "created_by": "liaoj",
    "customize_tag": "latest",
    "auto_tag_type": "TIME",
    "image_cache_enabled": True,
    "auto_build_enabled": True,
    "notification_id": "e98f19e7-f99c-452a-82a5-66728c54113e"
}

json_create_build_config_sonarqube = {
    "namespace": "test",
    "name":"json_create_build_config_sonarqube",
    "image_repo_id": "698f19e7-f99c-452a-82a5-66728c54113e",
    "code_repo": {
        "code_repo_client": "SIMPLE_GIT",
        "code_repo_path": "git@52.24.16.115:root/testCodeRepo.git",
        "code_repo_type": "BRANCH",
        "code_repo_type_value": "master/*",
        "dockerfile_location": "/",
        "build_context_path": "/"
    },
    "created_by": "liaoj",
    "customize_tag": "latest",
    "auto_tag_type": "TIME",
    "image_cache_enabled": True,
    "auto_build_enabled": True,
    "notification_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "sonarqube":{
        "integration_instance_id": "xxx-xxxx",
        "build_integration_config": {
            "quality_gate": "default",
            "code_language": "java",
            "code_encoding": "utf8",
            "code_scan_path": "/src"
        }
    }
}

json_create_build_by_time = {
    "config_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "image_repo_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "docker_repo_path": "test_repo",
    "registry_index": "http://liaojian.alauda.cn:5000",
    "status": "B",
    "auto_tag_type": "TIME",
    "namespace": "test",
    "customize_tag": "liaojian",
    "user_token": "abcdefg",
    "code_repo": {
        "code_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
        "code_repo_client": "SIMPLE_GIT",
        "code_repo_path": "git@52.24.16.115:root/testCodeRepo.git",
        "code_repo_type": "BRANCH",
        "code_repo_type_value": "master",
        "dockerfile_location": "/",
        "build_context_path": "/",
        "code_head_commit": "abc123",
        "code_repo_private_key": "code_repo_private_key"
    },
    "created_by": "liaoj",
    "is_auto_created": False,
    "image_cache_enabled": True,
    "auto_build_enabled": True,
    "notification_id": "e98f19e7-f99c-452a-82a5-66728c54113e"
}

json_create_build_by_commit = {
    "config_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "image_repo_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "docker_repo_path": "test_repo",
    "registry_index": "http://liaojian.alauda.cn:5000",
    "status": "B",
    "auto_tag_type": "COMMIT",
    "customize_tag": "liaojian",
    "user_token": "abcdefg",
    "namespace": "test",
    "code_repo": {
        "code_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
        "code_repo_client": "SIMPLE_GIT",
        "code_repo_path": "git@52.24.16.115:root/testCodeRepo.git",
        "code_repo_type": "BRANCH",
        "code_repo_type_value": "master",
        "dockerfile_location": "/",
        "build_context_path": "/",
        "code_head_commit": "",
        "code_repo_private_key": "code_repo_private_key"
    },
    "created_by": "liaoj",
    "is_auto_created": False,
    "image_cache_enabled": False,
    "auto_build_enabled": True,
    "notification_id": "e98f19e7-f99c-452a-82a5-66728c54113e"
}

json_create_build_for_wilcard = {
    "config_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "image_repo_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "docker_repo_path": "test_repo",
    "registry_index": "http://liaojian.alauda.cn:5000",
    "status": "B",
    "auto_tag_type": "COMMIT",
    "customize_tag": "liaojian",
    "user_token": "abcdefg",
    "namespace": "test",
    "code_repo": {
        "code_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
        "code_repo_client": "SIMPLE_GIT",
        "code_repo_path": "git@52.24.16.115:root/testCodeRepo.git",
        "code_repo_type": "BRANCH",
        "code_repo_type_value": "master/*",
        "dockerfile_location": "/",
        "build_context_path": "/",
        "code_head_commit": "",
        "code_repo_private_key": "code_repo_private_key"
    },
    "created_by": "liaoj",
    "is_auto_created": False,
    "image_cache_enabled": False,
    "auto_build_enabled": True,
    "notification_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "runtime_params":{
        "code_repo_type_value": "master/m1"
    }
}

json_create_build_for_sonarqube = {
    "config_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "image_repo_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "docker_repo_path": "test_repo",
    "registry_index": "http://liaojian.alauda.cn:5000",
    "status": "B",
    "auto_tag_type": "COMMIT",
    "customize_tag": "liaojian",
    "user_token": "abcdefg",
    "namespace": "test",
    "code_repo": {
        "code_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
        "code_repo_client": "SIMPLE_GIT",
        "code_repo_path": "git@52.24.16.115:root/testCodeRepo.git",
        "code_repo_type": "BRANCH",
        "code_repo_type_value": "master/*",
        "dockerfile_location": "/",
        "build_context_path": "/",
        "code_head_commit": "",
        "code_repo_private_key": "code_repo_private_key"
    },
    "created_by": "liaoj",
    "is_auto_created": False,
    "image_cache_enabled": False,
    "auto_build_enabled": True,
    "notification_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "runtime_params":{
        "code_repo_type_value": "master/m1"
    },
    "sonarqube":{
        "integration_instance_id": "xxx-xxxx",
        "build_integration_config": {
            "quality_gate": "default",
            "code_language": "java",
            "code_encoding": "utf8",
            "code_scan_path": "/src"
        }
    }
}

json_create_build_for_wilcard_2 = {
    "config_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "image_repo_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "docker_repo_path": "test_repo",
    "registry_index": "http://liaojian.alauda.cn:5000",
    "status": "B",
    "auto_tag_type": "COMMIT",
    "customize_tag": "liaojian",
    "user_token": "abcdefg",
    "namespace": "test",
    "code_repo": {
        "code_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
        "code_repo_client": "SIMPLE_GIT",
        "code_repo_path": "git@52.24.16.115:root/testCodeRepo.git",
        "code_repo_type": "BRANCH",
        "code_repo_type_value": "master/*",
        "dockerfile_location": "/",
        "build_context_path": "/",
        "code_head_commit": "",
        "code_repo_private_key": "code_repo_private_key"
    },
    "created_by": "liaoj",
    "is_auto_created": False,
    "image_cache_enabled": False,
    "auto_build_enabled": True,
    "notification_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "runtime_params":{
        "code_repo_type_value": "master/m2"
    }
}

json_create_build_for_wilcard_not_match = {
    "config_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "image_repo_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "docker_repo_path": "test_repo",
    "registry_index": "http://liaojian.alauda.cn:5000",
    "status": "B",
    "auto_tag_type": "COMMIT",
    "customize_tag": "liaojian",
    "user_token": "abcdefg",
    "namespace": "test",
    "code_repo": {
        "code_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
        "code_repo_client": "SIMPLE_GIT",
        "code_repo_path": "git@52.24.16.115:root/testCodeRepo.git",
        "code_repo_type": "BRANCH",
        "code_repo_type_value": "master/*",
        "dockerfile_location": "/",
        "build_context_path": "/",
        "code_head_commit": "",
        "code_repo_private_key": "code_repo_private_key"
    },
    "created_by": "liaoj",
    "is_auto_created": False,
    "image_cache_enabled": False,
    "auto_build_enabled": True,
    "notification_id": "e98f19e7-f99c-452a-82a5-66728c54113e",
    "runtime_params":{
        "code_repo_type_value": "master/m1/m2"
    }
}

json_update_build = {
    "code_head_commit": "5678905",
    "status": "I",
    "step_at": 30,
    "dockerfile_content": "dockerfile_content",
    "image_id": "image_id"
}

json_update_build_sonarqube = {
    "sonarqube":{
        "execution_status": "I",
        "step_at": "STEP_ANALYSIS",
        "integration_execution":{
            "task_id":"dsf923-dfa3",
            "quality_gate_value": "PASS",
            "analysis_result": {
                "measures": [
                {
                    "metric": "duplicated_lines_density",
                    "value": "0.0"
                },
                {
                    "metric": "duplicated_blocks",
                    "value": "0"
                }
                ]
            }
        }
    }
}

json_create_endpoint = {
    "namespace": "test_create_endpoint",
    "endpoint": "http://1.1.1.1:6666",
    "builder_image": "index.alauda.cn/mathildedev/rattletrap:latest"
}


class PrivateBuildTest(APITestCase):

    @mock.patch.object(DavionClient, 'get_integration')
    @mock.patch.object(WeblabClient, "prefetch")
    @mock.patch.object(MedusaClient, "create_schedule_event_config")
    def setUp(self, medusa_create_mock, weblab_prefetch_mock, davion_get_mock):
        medusa_create_mock.return_value={
            "id":"test-id",
            "secret_key":"key"
        }
        weblab_prefetch_mock.return_value = WebLabResult()
        davion_get_mock.return_value = {
            "enabled": True,
            "fields":{
                "endpoint": "http://sonarqube.alauda.org",
                "token": "dsaa-342sx"
            }
        }
        endpoint = BuildEndpoint(namespace='test',
                                 builder_image="index.alauda.cn/mathildedev/rattletrap:latest",
                                 endpoint='http://liaojian.alauda.cn')
        endpoint.save()
        self._init_build(endpoint, json_create_build_config, [json_create_build_by_time, json_create_build_by_commit])
        self._init_build(endpoint, json_create_build_config_wilcard, [ json_create_build_for_wilcard, 
                                                            json_create_build_for_wilcard_2,
                                                            json_create_build_for_wilcard_not_match ])
        self._init_build(endpoint, json_create_build_config_sonarqube, [json_create_build_for_sonarqube ])

    def _init_build(self, endpoint, _json_create_build_config, json_create_builds):
        response = self.call_api_create_config(_json_create_build_config)
        _json_create_build_config['config_id'] = response.data['config_id']

        for json_create_build in json_create_builds:
            # dynamic change json_create_build
            json_create_build['endpoint_id'] = endpoint.endpoint_id
            # beacuse build.config is a ForeignKey, we cannot special a value at will
            build_config_code = BuildConfigCode.objects.get(build_config_id=response.data['config_id'])
            self.assertEquals(response.status_code, 201)
            # just using what we created
            json_create_build['config_id'] = response.data['config_id']
            json_create_build['code_repo']['code_id'] = build_config_code.code_id
            json_create_build['build_config_name'] = _json_create_build_config['name']
            if json_create_build.get('sonarqube'):
                json_create_build['sonarqube']['build_integration_config']['project_key'] = response.data['config_id']
                json_create_build['sonarqube']['build_integration_config']['project'] = _json_create_build_config['namespace'] +":"+ _json_create_build_config['name']

    def call_api_create_config(self, _json_create_build_config):
        response = self.client.post('/v1/private_build_configs/',
                                    _json_create_build_config,
                                    format='json')
        return response        

    def call_api_create_endpoint(self, namespace, json):
        return self.client.post('/v1/private_build_endpoints/{}'.format(namespace),
                                json, format='json')

    def call_app_get_endpoint(self, namespace):
        return self.client.get('/v1/private_build_endpoints/{}'.format(namespace))

    def call_api_create(self, json):
        FurionUtil.get_region_name_map = mock.Mock(return_value={})
        return self.client.post('/v1/private_builds/',
                                json,
                                format='json')

    def call_api_get_list(self, config_id, image_repo_id):
        return self.client.get('/v1/private_builds/?uuids={}&image_repo_id={}&page=1'
                               .format(config_id, image_repo_id))

    def call_api_get_detail(self, build_id):
        return self.client.get('/v1/private_builds/{}/'.format(build_id))

    def call_api_update_detail(self, json_update, build_id):
        return self.client.put('/v1/private_builds/{}/'.format(build_id),
                               json_update, format='json')

    def call_api_delete_detail(self, build_id):
        return self.client.delete('/v1/private_builds/{}/'.format(build_id))


    def tearDown(self):
        pass    

    @mock.patch.object(DavionClient, 'get_integration')
    @mock.patch.object(MedusaClient, "create_schedule_event_config")
    @mock.patch.object(WeblabClient, "prefetch")
    @mock.patch.object(TresdinClient, 'create')
    @mock.patch.object(NotificationClient, 'send_message')
    @mock.patch.object(FurionUtil, 'get_display_name_by_endpoint_id')
    @mock.patch.object(FurionUtil, 'get_region_name_map')
    def test_api_get_list(self, f_get_rname_mock, f_get_mock, send_message_mock, create_build_job_mock,
            weblab_prefetch_mock, medusa_create_mock, davion_get_mock):
        f_get_rname_mock.return_value = {}
        f_get_mock.return_value = "test"
        create_build_job_mock.return_value = True
        send_message_mock.return_value = True
        medusa_create_mock.return_value = {
            "id":"test-id",
            "secret_key":"key"
        }
        davion_get_mock.return_value = {
            "enabled": True,
            "fields":{
                "endpoint": "http://sonarqube.alauda.org",
                "token": "dsaa-342sx"
            }
        }
        weblab_prefetch_mock.return_value = WebLabResult()
        config_id = json_create_build_config['config_id']
        image_repo_id = 'e98f19e7-f99c-452a-82a5-66728c54113e'

        self.call_api_create(json_create_build_by_time)
        self.call_api_create(json_create_build_by_commit)
        response_list = self.call_api_get_list('{},{}'.format(config_id,config_id), image_repo_id)
        self.assertEquals(response_list.status_code, 200)
        self.assertEquals(response_list.data['page_size'], 20)
        self.assertEquals(response_list.data['num_pages'], 1)
        self.verify_build(response_list.data['result_list'][0], json_create_build_by_commit,
                          response_list.data['result_list'][0]['code_repo']['code_head_commit'])
        self.verify_build_code(response_list.data['result_list'][0]['code_repo'],
                               json_create_build_by_commit)
        self.verify_build(response_list.data['result_list'][1], json_create_build_by_time,
                          response_list.data['result_list'][1]['code_repo']['code_head_commit'])
        self.verify_build_code(response_list.data['result_list'][1]['code_repo'],
                               json_create_build_by_time)
        # verfy build with sonarqube
        config_id = json_create_build_config_sonarqube['config_id']
        self.call_api_create(json_create_build_for_sonarqube)
        response_list = self.call_api_get_list('{},{}'.format(config_id,config_id), image_repo_id)
        self.verify_build(response_list.data['result_list'][0], json_create_build_for_sonarqube,
                          response_list.data['result_list'][0]['code_repo']['code_head_commit'])
        self.verify_build_code(response_list.data['result_list'][0]['code_repo'],
                               json_create_build_for_sonarqube)
        self.verify_build_sonarqube_integration(response_list.data['result_list'][0], json_create_build_for_sonarqube,)

    @mock.patch.object(DavionClient, 'get_integration')
    @mock.patch.object(MedusaClient, 'create_schedule_event_config')
    @mock.patch.object(WeblabClient, "prefetch")
    @mock.patch.object(TresdinClient, 'create')
    @mock.patch.object(FurionUtil, 'get_region_name_map')
    def test_create_build(self, f_get_rname_mock, create_build_job_mock, weblab_prefetch_mock, medusa_create_mock, davion_get_mock):
        f_get_rname_mock.return_value = {}
        create_build_job_mock.return_value = True
        weblab_prefetch_mock.return_value = WebLabResult()
        medusa_create_mock.return_value = {
            "id":"test-id",
            "secret_key":"key"
        }
        davion_get_mock.return_value = {
            "enabled": True,
            "fields":{
                "endpoint": "http://sonarqube.alauda.org",
                "token": "dsaa-342sx"
            }
        }
        data = [
            json_create_build_by_commit,
            json_create_build_by_time,
            json_create_build_for_wilcard,
            json_create_build_for_sonarqube
        ]
        for json_create_build in data:
            response_time = self.call_api_create(json_create_build)
            self.assertEquals(response_time.status_code, 201)
            self.verify_create(response_time.data['build_id'], json_create_build)
        data2 = [
            json_create_build_for_wilcard_not_match
        ]
        for json_create_build in data2:
            response_time = self.call_api_create(json_create_build)
            self.assertEqual(response_time.status_code, 400)

    @mock.patch.object(DavionClient, 'get_integration')
    @mock.patch.object(MedusaClient, 'create_schedule_event_config')
    @mock.patch.object(WeblabClient, 'prefetch')
    @mock.patch.object(FurionUtil, 'get_region_name_map')
    @mock.patch.object(TresdinClient, 'create')
    def test_get_build_detail(self, create_build_job_mock, f_rname_mock, weblab_prefetch_mock, medusa_create_mock, davion_get_mock):
        f_rname_mock.return_value = {}
        create_build_job_mock.return_value = True
        weblab_prefetch_mock.return_value = WebLabResult()
        medusa_create_mock.return_value = {
            "id":"test-id",
            "secret_key":"key"
        }
        davion_get_mock.return_value = {
            "enabled": True,
            "fields":{
                "endpoint": "http://sonarqube.alauda.org",
                "token": "dsaa-342sx"
            }
        }
        response_time = self.call_api_create(json_create_build_by_time)
        detail = self.call_api_get_detail(response_time.data['build_id'])
        self.assertEquals(detail.status_code, 200)
        self.verify_build(detail.data, json_create_build_by_time,
                          detail.data['code_repo']['code_head_commit'])
        self.verify_build_code(detail.data['code_repo'], json_create_build_by_time)

    @mock.patch.object(DavionClient, 'get_integration')
    @mock.patch.object(MedusaClient, 'create_schedule_event_config')
    @mock.patch.object(WeblabClient, 'prefetch')
    @mock.patch.object(FurionUtil, 'get_region_name_map')
    @mock.patch.object(TresdinClient, 'create')
    @mock.patch.object(NotificationClient, 'send_message')
    def test_update_build(self, send_message_mock, create_build_job_mock, f_rname_mock, weblab_prefetch_mock, medusa_create_mock, davion_get_mock):
        f_rname_mock.return_value = {}
        send_message_mock.return_value = True
        create_build_job_mock.return_value = True
        weblab_prefetch_mock.return_value = WebLabResult()
        medusa_create_mock.return_value = {
            "id":"test-id",
            "secret_key":"key"
        }
        davion_get_mock.return_value = {
            "enabled": True,
            "fields":{
                "endpoint": "http://sonarqube.alauda.org",
                "token": "dsaa-342sx"
            }
        }
        response_time = self.call_api_create(json_create_build_by_commit)
        update = self.call_api_update_detail(json_update_build, response_time.data['build_id'])
        self.assertEquals(update.status_code, 200)
        self.verify_update(response_time.data['build_id'], json_create_build_by_commit, json_update_build)

        resp1 = self.call_api_create(json_create_build_for_wilcard)
        update = self.call_api_update_detail(json_update_build, resp1.data['build_id'])
        self.assertEquals(update.status_code, 200)
        self.verify_update(resp1.data['build_id'], json_create_build_for_wilcard, json_update_build)
        # creat another job for test
        resp2 = self.call_api_create(json_create_build_for_wilcard_2)
        ori_head_commit = json_update_build['code_head_commit']
        json_update_build['code_head_commit'] = "22334455"
        update = self.call_api_update_detail(json_update_build, resp2.data['build_id'])
        build = Build.objects.get(build_id=resp2.data['build_id'])
        build_config_code = BuildConfigCode.objects.get(build_config_id=build.config_id)
        commit_infoes = BuildConfigCode.parse_head_commit_id(json_create_build_config_wilcard['code_repo']['code_repo_type_value'],
                 build_config_code.head_commit_id)
        branch = json_create_build_for_wilcard['runtime_params']['code_repo_type_value']
        commit = ori_head_commit
        self.assertEqual(commit_infoes[branch]["commit_id"], commit)
        branch = json_create_build_for_wilcard_2['runtime_params']['code_repo_type_value']
        commit = json_update_build['code_head_commit']
        self.assertEqual(commit_infoes[branch]["commit_id"], commit)

        resp = self.call_api_create(json_create_build_for_sonarqube)
        update = self.call_api_update_detail(json_update_build_sonarqube, resp.data['build_id'])
        self.assertEqual(update.status_code, 200)
        self.verify_update(resp.data['build_id'], json_create_build_for_sonarqube, json_update_build_sonarqube)
    
    def verify_update(self, build_id, json_create, json_update):
        build = Build.objects.get(build_id=build_id)
        build_code = BuildCode.objects.get(build_id=build_id)
        build_config_code = BuildConfigCode.objects.get(build_config_id=build.config_id)
        if json_update.get('sonarqube'):
            # verfy sonarqube
            json_sonarqube_update = json_update.get('sonarqube')
            sonarqube = BuildIntegration.objects.all().filter(type=INTEGRATION_SONARQUBE, build_id=build_id).get()
            self.assertEqual(sonarqube.execution_status, json_sonarqube_update['execution_status'])
            self.assertEqual(sonarqube.step_at,
                json_sonarqube_update['step_at'])
            self.assertEqual(sonarqube.integration_execution['task_id'],
                json_sonarqube_update['integration_execution']['task_id'])
            self.assertEqual(sonarqube.integration_execution['analysis_result'],
                json_sonarqube_update['integration_execution']['analysis_result'])
            self.assertEqual(sonarqube.integration_execution['quality_gate_value'],
                json_sonarqube_update['integration_execution']['quality_gate_value'])
            return
        self.assertEquals(build.image_id, json_update['image_id'])
        self.assertEquals(build.step_at, json_update['step_at'])
        self.assertEquals(build.status, json_update['status'])
        self.assertEquals(build_code.code_head_commit, json_update['code_head_commit'])
        self.assertEquals(build.dockerfile_content, json_update['dockerfile_content'])
        branch = json_create['code_repo']['code_repo_type_value']
        if json_create.get('runtime_params') and  json_create['runtime_params'].get('code_repo_type_value'):
            branch = json_create['runtime_params']['code_repo_type_value']
        head_commit_id = "{}:{}".format(
            branch,
            json_update['code_head_commit']
        )
        self.assertEqual(build_config_code.head_commit_id, head_commit_id)

    @mock.patch.object(DavionClient, 'get_integration')
    @mock.patch.object(MedusaClient, 'delete_schedule_event_config')
    @mock.patch.object(MedusaClient, 'create_schedule_event_config')
    @mock.patch.object(WeblabClient, 'prefetch')
    @mock.patch.object(TresdinClient, 'create')
    @mock.patch.object(TresdinClient, 'delete')
    @mock.patch.object(FurionUtil, 'get_region_name_map')
    def test_delete_build(self, f_get_rname_mock, delete_build_job_mock, create_build_job_mock,
            weblab_prefetch_mock, medusa_create_mock, medusa_delete_mock, davion_get_mock):
        f_get_rname_mock.return_value = {}
        create_build_job_mock.return_value = True
        delete_build_job_mock.return_value = True
        weblab_prefetch_mock.return_value = WebLabResult()
        medusa_create_mock.return_value = {
            "id":"test-id",
            "secret_key":"key"
        }
        medusa_delete_mock.return_value = {}
        davion_get_mock.return_value = {
            "enabled": True,
            "fields":{
                "endpoint": "http://sonarqube.alauda.org",
                "token": "dsaa-342sx"
            }
        }
        response_time = self.call_api_create(json_create_build_by_commit)
        response = self.call_api_delete_detail(response_time.data['build_id'])
        self.assertEquals(response.status_code, 204)
        build = Build.objects.get(build_id=response_time.data['build_id'])
        self.assertEquals(build.status, 'D')
        response_time = self.call_api_create(json_create_build_for_sonarqube)
        response = self.call_api_delete_detail(response_time.data['build_id'])
        self.assertEquals(response.status_code, 204)
        build = Build.objects.get(build_id=response_time.data['build_id'])
        self.assertEquals(build.status, 'D')
        sonarqube_int = BuildIntegration.objects.get(build_id=response_time.data['build_id'], type=INTEGRATION_SONARQUBE)
        self.assertEqual(sonarqube_int.execution_status, 'D')

    def verify_create(self, build_id, json):
        build_code = BuildCode.objects.get(build_id=build_id)
        self.verify_build_code(build_code, json)
        build = Build.objects.get(build_id=build_id)
        self.verify_build(build, json, build_code.code_head_commit)
        self.verify_build_sonarqube_integration(build, json)

    def get_value(self, data, field):
        if str(type(data)).lower().find('dict') != -1:
            return data.get(field)
        else:
            return getattr(data, field, None)

    def verify_build(self, build, json, commit):
        self.assertEquals(str(self.get_value(build, 'image_repo_id')), json['image_repo_id'])
        self.assertEquals(self.get_value(build, 'docker_repo_path'), json['docker_repo_path'])
        self.assertEquals(self.get_value(build, 'registry_index'), json['registry_index'])
        self.assertEquals(self.get_value(build, 'docker_repo_tag'), json['customize_tag'])
        if json['auto_tag_type'] == 'TIME' or (json['auto_tag_type'] == 'COMMIT' and commit):
            self.assertIsNotNone(self.get_value(build, 'docker_repo_version_tag'))
        self.assertEquals(self.get_value(build, 'created_by'), json['created_by'])
        self.assertEquals(self.get_value(build, 'build_cache_enabled'), json['image_cache_enabled'])
        self.assertEquals(str(self.get_value(build, 'notification_id')), json['notification_id'])
    
    def verify_build_sonarqube_integration(self, build, json):
        if not json.get('sonarqube'):
            return
        sonarqube_build_integration = BuildIntegration.objects.all().filter(build_id=self.get_value(build, 'build_id'), type=INTEGRATION_SONARQUBE).get()
        sonarqube_json = json.get('sonarqube')
        self.assertEqual(self.get_value(sonarqube_build_integration, 'integration_instance_id'), sonarqube_json.get('integration_instance_id'))
        self.assertEqual(self.get_value(sonarqube_build_integration, 'type'), INTEGRATION_SONARQUBE)
        build_integration_config = self.get_value(sonarqube_build_integration, 'build_integration_config')
        self.assertEqual(
            self.get_value(build_integration_config, 'project_key'), self.get_value(json, 'config_id'))
        self.assertEqual(
            self.get_value(build_integration_config, 'project'), self.get_value(json, 'namespace') +":"+ self.get_value(json, 'build_config_name'))
        self.assertEqual(
            self.get_value(build_integration_config, 'code_scan_path'), self.get_value(sonarqube_json['build_integration_config'], 'code_scan_path'))
        self.assertEqual(
            self.get_value(build_integration_config, 'code_encoding'), self.get_value(sonarqube_json['build_integration_config'], 'code_encoding'))
        self.assertEqual(
            self.get_value(build_integration_config, 'quality_gate_value'), self.get_value(sonarqube_json['build_integration_config'], 'quality_gate_value'))
        
    def verify_build_code(self, build_code, json):
        self.assertEquals(self.get_value(build_code, 'code_repo_client'),
                          json['code_repo']['code_repo_client'])
        self.assertEquals(self.get_value(build_code, 'code_repo_path'),
                          json['code_repo']['code_repo_path'])
        self.assertEquals(self.get_value(build_code, 'code_repo_type'),
                          json['code_repo']['code_repo_type'])
        if not BuildConfigCode.is_wildcard_of_repo_type_value(json['code_repo']['code_repo_type_value']):
            self.assertEquals(self.get_value(build_code, 'code_repo_type_value'),
                          json['code_repo']['code_repo_type_value'])
        else:
            match = BuildConfigCode.is_match_repo_type_value_wildcard(json['code_repo']['code_repo_type_value'],
                          self.get_value(build_code, 'code_repo_type_value'))
            self.assertTrue(match)
        self.assertEquals(self.get_value(build_code, 'dockerfile_location'),
                          json['code_repo']['dockerfile_location'])
        self.assertEquals(self.get_value(build_code, 'build_context_path'),
                          json['code_repo']['build_context_path'])
        self.assertEquals(self.get_value(build_code, 'code_repo_private_key'),
                          json['code_repo']['code_repo_private_key'])
        self.assertEquals(self.get_value(build_code, 'code_head_commit'),
                          json['code_repo']['code_head_commit'])

    def test_create_private_build_endpoint(self):
        namespace = json_create_endpoint.get("namespace")
        post_endpoint = json_create_endpoint.get("endpoint")
        img = json_create_endpoint.get("builder_image")
        response = self.call_api_create_endpoint(namespace, json_create_endpoint)
        self.assertEquals(response.status_code, 201)
        endpoint = BuildEndpoint.objects.get(namespace=namespace)
        self.assertEqual(endpoint.namespace, namespace)
        self.assertEqual(endpoint.endpoint, post_endpoint)
        self.assertEqual(endpoint.builder_image, img)
