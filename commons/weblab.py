from alauda.weblab import WeblabClient

CI_INTEGRATION_SONARQUBE = "CI_INTEGRATION_SONARQUBE"


class Weblab(object):

    @staticmethod
    def _is_enabled(namespace, key):
        weblab = WeblabClient.prefetch(namespace)
        return weblab.get(key)

    @staticmethod
    def CI_INTEGRATION_SONARQUBE(namespace):
        return Weblab._is_enabled(namespace, CI_INTEGRATION_SONARQUBE)

