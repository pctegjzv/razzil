import logging

from commons.code_client.code_client_base import CodeClientBase
from commons.code_client.web_hook_data import WebHookData
from private_build_configs.models import CODE_REPO_TYPE_DIR


LOG = logging.getLogger(__name__)


class SimpleSVNClient(CodeClientBase):
    def __init__(self):
        super(SimpleSVNClient, self).__init__()

    def parse_web_hook_post_data(self, post_data, code_repo):
        code_repo_type = CODE_REPO_TYPE_DIR
        code_repo_type_value = self._get_branch(post_data['repos'])
        code_head_commit = post_data['rev']
        web_hook_data = WebHookData(code_repo_type,
                                    code_repo_type_value,
                                    code_head_commit)
        return web_hook_data

    def _get_branch(self, ref):
        ref_array = ref.split('/')
        return ref_array[len(ref_array) - 1]

    def create_deploy_key(self, code_repo_path, repo_id, description=None):
        from commons.util import RazzilUtil
        if RazzilUtil.is_svn_and_ssh_protocol(code_repo_path):
            return super(SimpleSVNClient, self).create_deploy_key(code_repo_path, repo_id, description)
        else:
            return {
                'id': None,
                'private_key': None,
                'public_key': None
            }
