import types
from rest_framework import serializers

from commons.furion_client import FurionUtil
from private_build_configs.models import BuildConfig, BuildConfigCode, BuildConfigIntegration

STATUS_DELETED = 'D'


class BuildConfigCodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = BuildConfigCode
        read_only_fields = ('code_id', 'created_at', 'updated_at')


class BuildConfigSerializer(serializers.ModelSerializer):
    ci_envs = serializers.JSONField(required=False)
    ci_steps = serializers.JSONField(required=False)
    image_repo_id = serializers.CharField(required=False)
    build_filters = serializers.JSONField(required=False)

    class Meta:
        model = BuildConfig
        read_only_fields = ('created_at', 'updated_at',
                            'code_cache_enabled')

    def to_representation(self, instance):
        ret = super(BuildConfigSerializer, self).to_representation(instance)
        ret["exec_cmd_enabled"] = instance.exec_cmd_enabled
        return ret

class BuildConfigCodeIncludeConfigSerializer(serializers.ModelSerializer):
    build_config = BuildConfigSerializer()

    class Meta:
        model = BuildConfigCode
        read_only_fields = ('code_id', 'created_at', 'updated_at')


class BuildConfigCodeExcludePrivateKeySerializer(serializers.ModelSerializer):
    class Meta:
        model = BuildConfigCode
        read_only_fields = ('code_id', 'created_at', 'updated_at')
        exclude = ('code_repo_private_key',)


class BuildConfigCodeForUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BuildConfigCode
        read_only_fields = ('code_id', 'created_at', 'updated_at', 'code_repo_webhook_id',
                            'code_repo_webhook_code', 'code_repo_key_id', 'code_repo_private_key',
                            'code_repo_public_key', 'repo_id', 'code_repo_client', 'build_config')


class BuildConfigIncludeCodeSerializer(serializers.ModelSerializer):
    code_repo = BuildConfigCodeSerializer(read_only=True)
    ci_envs = serializers.JSONField(required=False)
    ci_steps = serializers.JSONField(required=False)
    build_filters = serializers.JSONField(required=False)
    endpoint_name = serializers.CharField(required=False)
    build_info = serializers.SerializerMethodField()
    dockerfile_info = serializers.SerializerMethodField()

    def to_representation(self, instance):
        ret = super(BuildConfigIncludeCodeSerializer, self).to_representation(instance)
        return ret

    class Meta:
        model = BuildConfig
        read_only_fields = ('config_id', 'created_at', 'updated_at',
                            'code_cache_enabled')
        exclude = ('dockerfile',)

    # get latest build history info
    def get_build_info(self, obj):
        build_info = {
            'latest_version': None,
            'status': None,
            'elapsed': None
        }
        build_sets = obj.build_set.exclude(status=STATUS_DELETED)
        if build_sets.exists():
            build_obj = build_sets.order_by('-updated_at').first()
            if build_obj.started_at and build_obj.ended_at:
                elapsed_time = int((build_obj.ended_at - build_obj.started_at).total_seconds())
                build_info['elapsed'] = elapsed_time
            docker_repo_version_tag = build_obj.docker_repo_version_tag or ''
            build_info['latest_version'] = build_obj.docker_repo_tag + ',' + docker_repo_version_tag
            build_info['status'] = build_obj.status
        return build_info

    # get dockerfile info
    def get_dockerfile_info(self, obj):
        dockerfile_info = dict()
        dockerfile_obj = obj.dockerfile
        if dockerfile_obj:
            dockerfile_info['uuid'] = dockerfile_obj.uuid
            dockerfile_info['name'] = dockerfile_obj.name
            dockerfile_info['display_name'] = dockerfile_obj.display_name
            dockerfile_info['description'] = dockerfile_obj.description
        return dockerfile_info


class BuildConfigForUpdateSerializer(serializers.ModelSerializer):
    ci_envs = serializers.JSONField(required=False)
    ci_steps = serializers.JSONField(required=False)
    build_filters = serializers.JSONField(required=False)

    class Meta:
        model = BuildConfig
        read_only_fields = ('config_id', 'created_at', 'updated_at',
                            'code_cache_enabled')


class BuildConfigIntegrationSerializer(serializers.ModelSerializer):
    build_integration_config = serializers.JSONField(required=False)

    class Meta:
        model = BuildConfigIntegration
        read_only_fields = ('created_at', 'updated_at')


class BuildConfigIntegrationForUpdateSerializer(serializers.ModelSerializer):
    build_integration_config = serializers.JSONField(required=False)

    class Meta:
        model = BuildConfigIntegration
        read_only_fields = ('type', 'build_config_id', 'created_at')


class BuildConfigIntegrationForRespSerializer(serializers.ModelSerializer):
    build_integration_config = serializers.JSONField(required=False)

    class Meta:
        model = BuildConfigIntegration
        fields = (
            'integration_instance_id',
            'build_integration_config',
            'created_at',
            'updated_at')
