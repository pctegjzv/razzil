import random
import uuid

from django.conf import settings
from urllib import quote

from commons.code_client.key_generator import KeyGenerator


class CodeClientBase(object):
    def __init__(self):
        self.code_seed = 'A0B1C2D3E4F5G6H7I8J9KLMNOPQRSTUVWXYZ'
        self.webhook_code_len = 12
        self.bits = 1024
        self.code_client_user = {}

    def gen_webhook_code(self):
        return ''.join(random.sample(self.code_seed, self.webhook_code_len))
        
    def create_webhook(self, code_repo_path, namespace, repo_id):
        code = ''.join(random.sample(self.code_seed, self.webhook_code_len))
        data = {
            'id': str(uuid.uuid4()),
            'code': code,
            'password': None,
            'url': '{}/{}/private-builds/{}/auto/{}/'.format(
                settings.JAKIRO_API_ENDPOINT,
                settings.JAKIRO_API_VERSION, namespace, code)
        }

        return data

    def create_deploy_key(self, code_repo_path, repo_id, description=None):
        key_pairs = KeyGenerator.create_ssh_key(self.bits)
        if not description:
            description = 'alauda deploy key'

        return {
            'id': None,
            'private_key': key_pairs['private_key'],
            'public_key': key_pairs['public_key'],
            'description': description
        }

    def parse_web_hook_payload(self, hook_data):
        pass

    def parse_web_hook_post_data(self, hook_data, code_repo):
        pass

    def get_auth_url(self, redirect_uri):
        pass

    def get_access_token(self, request_data):
        pass

    def get_client_login(self, user_name):
        pass

    def get_client_orgs(self, *args, **kwargs):
        pass

    def get_repo_list(self, code_client_org, *args, **kwargs):
        pass

    def get_full_path(self, repo_path, username, password):
        if username and password:
            username = quote(username)
            password = quote(password)
            repo_path = repo_path.replace('//', '//{}:{}@'.format(username, password))
        return repo_path

    def set_code_client_user(self, data):
        self.code_client_user = data

    def delete_deploy_key(self, code_repo_path, id, repo_id):
        pass

    def delete_webhook(self, code_repo_path, id, repo_id):
        pass

    def load(self, namespace):
        pass

        