# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging

import requests
import xmltodict
from mekansm.exceptions import MekAPIException


logger = logging.getLogger(__name__)


class OSSException(MekAPIException):
    def __init__(self, http_status, error_code, message):
        self._status_code = http_status
        self.error_code = error_code
        self.error_message = message

    @property
    def status_code(self):
        return self._status_code

    @property
    def data(self):
        return {
            'code': self.error_code,
            'message': self.error_message,
            'source': 1031
        }


class OSS(object):
    timeout = 10

    def __init__(self, endpoint, token):
        self._endpoint = endpoint.rstrip('/')
        self._token = token

    def _is_code_ok(self, status_code):
        if status_code < 300:
            return True
        return False

    def _is_code_not_found(self, status_code):
        if status_code == 404:
            return True
        return False

    def _url_open(self, path, method, data=None, params=None):
        uri = '{}/{}'.format(self._endpoint, path.lstrip('/'))
        headers = {
            'Authorization': 'token {}'.format(self._token)
        }
        logger.info('requesting url={}, method={}, header={}, params={}'.format(
            uri, method, headers, params))

        args = {'headers': headers, 'timeout': self.timeout}
        if data is not None:
            args['data'] = data
        if params is not None:
            args['params'] = params

        if method == 'PUT':
            resp = requests.put(uri, **args)
        elif method == 'GET':
            resp = requests.get(uri, **args)
        elif method == 'HEAD':
            resp = requests.head(uri, **args)
        elif method == 'DELETE':
            resp = requests.delete(uri, **args)
        return resp

    def list_objects(self, bucket_name, prefix='', data=None, marker=None):
        params = {}
        if prefix:
            params['prefix'] = prefix
        if marker:
            params["marker"] = marker
        resp = self._url_open(bucket_name, 'GET', params=params)
        if self._is_code_not_found(resp.status_code):
            return []
        if not self._is_code_ok(resp.status_code):
            code, msg = OSS.parse_error_resp(resp)
            raise OSSException(resp.status_code, code, msg)

        result_data = xmltodict.parse(resp.text)
        if not data:
            data = []
        if 'Contents' in result_data['ListBucketResult']:
            content = result_data['ListBucketResult']['Contents']
            if type(content) == list:
                for item in content:
                    data.append({
                        'key': item['Key'][len(prefix):],
                        'size': item['Size'],
                        'last_modified': item['LastModified']
                    })
            else:
                data.append({
                    'key': content['Key'][len(prefix):],
                    'size': content['Size'],
                    'last_modified': content['LastModified']
                })
        if result_data["ListBucketResult"].get("IsTruncated") == "true":
            marker = prefix + data[-1]["key"]
            return self.list_objects(bucket_name, prefix=prefix, data=data,
                                     marker=marker)
        return data

    def delete_objects(self, bucket_name, prefix=''):
        objects = self.list_objects(bucket_name, prefix)
        for item in objects:
            path = '{}/{}/{}'.format(bucket_name, prefix.rstrip('/'), item['key'])
            resp = self._url_open(path, 'DELETE')
            if self._is_code_not_found(resp.status_code):
                raise MekAPIException('resource_not_exist', message='{} is not exist'.format(path))
            if not self._is_code_ok(resp.status_code):
                raise OSSException('Delete objects from oss failed, err status {}, '
                                   'err info: {}'.format(resp.status_code, resp.text))
            else:
                logger.info('delete artifacts "{}" success'.format(path))

    @classmethod
    def parse_error_resp(cls, resp):
        try:
            resp_message = resp.json()
            code = resp_message['errors'][0]['code']
            error_message = resp_message['errors'][0]['message']
            logger.info('error code:{}, msg: {}'.format(code, error_message))
        except:
            code = 'unknown_issue'
            error_message = resp.text
        return code, error_message
