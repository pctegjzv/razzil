from django.conf import settings

from mekansm.exceptions import MekAPIException


class DockerBuildException(MekAPIException):
    errors_map = {
        'miss_args': {
            'source': settings.SOURCE,
            'message': 'the argument ({}) is required',
            'type': 'bad_request'
        },
        'invalid_args': {
            'source': settings.SOURCE,
            'message': 'the argument ({}) is not valid',
            'type': 'bad_request'
        },
        'invalid_username_or_password': {
            'source': settings.SOURCE,
            'message': 'the repo path is not started with http,'
                       ' did not need username and password',
            'type': 'bad_request'
        },
        'invalid_code_client': {
            'source': settings.SOURCE,
            'message': 'The code client ({})  is invalid',
            'type': 'bad_request'
        },
        'code_client_already_authed': {
            'source': settings.SOURCE,
            'message': 'The code client ({})  is authed',
            'type': 'bad_request'
        },
        'duplicate_commit_build': {
            'source': settings.SOURCE,
            'message': 'Duplicate build ({}) with same commit ({})',
            'type': 'bad_request'
        },
        'wrong_auth_callback_data': {
            'source': settings.SOURCE,
            'message': 'get invalid data from third party code client',
            'type': 'bad_request'
        },
        'build_failed': {
            'source': settings.SOURCE,
            'message': 'build_failed',
            'type': 'server_error'
        },
        'token_expired': {
            'source': settings.SOURCE,
            'message': 'customer token is expired',
            'type': 'server_error'
        },
        'get_token_failed': {
            'source': settings.SOURCE,
            'message': 'get token failed: {}',
            'type': 'server_error'
        },
        'git_hub_error': {
            'source': settings.SOURCE,
            'message': 'call git hub failed',
            'type': 'server_error'
        },
        'bitbucket_error': {
            'source': settings.SOURCE,
            'message': 'call bitbucket failed',
            'type': 'server_error'
        },
        'is_already_authed': {
            'source': settings.SOURCE,
            'message': 'the namespace is already authed for the code repo client',
            'type': 'server_error'
        },
        'call_furion_failed': {
            'source': settings.SOURCE,
            'message': 'call furion failed',
            'type': 'server_error'
        },
        'build_endpoint_already_exists': {
            'source': settings.SOURCE,
            'message': 'build endpoint already exists',
            'type': 'bad_request'
        },
        'build_endpoint_not_exists': {
            'source': settings.SOURCE,
            'message': 'build endpoint not exists',
            'type': 'bad_request'
        },
        'os_china_error': {
            'source': settings.SOURCE,
            'message': 'call os_china_error',
            'type': 'server_error'
        },
        'os_china_login_error': {
            'source': settings.SOURCE,
            'message': 'the email or password is error',
            'type': 'bad_request'
        },
        'call_chronos_failed': {
            'source': settings.SOURCE,
            'message': 'call chronose failed',
            'type': 'server_error'
        },
        'invalid_container_size': {
            'source': settings.SOURCE,
            'message': 'the build container size is wrong, please check cpu and memory',
            'type': 'bad_request'
        },
        'invalid_image_tag_rule': {
            'source': settings.SOURCE,
            'message': 'the image_tag_rule is wrong, please check the image_tag_rule',
            'type': 'bad_request'
        },
        'build_image_tag_not_exists': {
            'source': settings.SOURCE,
            'message': 'build image tag not exists',
            'type': 'bad_request'
        },
        'match_branch_wilcard_error': {
            'source': settings.SOURCE,
            'message': 'match wilcard of code_repo_type_value error : {0}',
            'type': 'bad_request'
        },
        'get_image_tag_failed': {
            'source': settings.SOURCE,
            'message': 'get image tag failed',
            'type': 'bad_request'
        },
        'build_integration_already_exist': {
            'source': settings.SOURCE,
            'message': 'The build integration ({})  is already exist',
            'type': 'bad_request'
        },
        'dockerfile_already_used': {
            'source': settings.SOURCE,
            'message': 'The dockerfile: {} has been used as follows: {}',
            'type': 'bad_request'
        },
        'name_already_exist': {
            'source': settings.SOURCE,
            'message': 'The name: {} has already exist.',
            'type': 'bad_request'
        },
    }
