from rest_framework import serializers

from private_code_clients.models import CodeClientUser


class CodeClientUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = CodeClientUser
        read_only_fields = ('id',)
