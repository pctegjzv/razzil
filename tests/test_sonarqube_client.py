import unittest
import os
from commons import sonarqube_client
from commons.sonarqube_client import BaseClient, SonarClient

sonar_endpoint = os.getenv('SONAR_ENDPOINT')
sonar_token = os.getenv('SONAR_TOKEN')
sonar_project_key = os.getenv('SONAR_PROJECT_KEY')

def check_sonar_args():
        """Check if args valid
        """
        if not sonar_endpoint or not sonar_token:
            print('skip test, because args endpoint:{} and token:{} are needed'.\
                format(sonar_endpoint, sonar_token))
            return None
        return {
            'endpoint': sonar_endpoint,
            'token': sonar_token,
        }

class BaseSonarClientTest(unittest.TestCase):
    def test_system_status(self):
        kwargs = check_sonar_args()
        if not kwargs:
            return
        client = sonarqube_client.BaseClient(**kwargs)
        status = client.system_status()

        assert isinstance(status, dict)
        assert 'version' in status
        print('result of system_status: {}'.format(status))

    def test_get_version(self):
        kwargs = check_sonar_args()
        if not kwargs:
            return
        client = sonarqube_client.BaseClient(**kwargs)
        assert len(client.version) == 5

    def test_get_language_list(self):
        kwargs = check_sonar_args()
        if not kwargs:
            return
        client = sonarqube_client.BaseClient(**kwargs)
        
        languages_list = client.get_language_list()
        assert isinstance(languages_list, dict)
        print('result of get_language_list: {}'.format(languages_list))

    def test_get_qualitygates_list(self):
        kwargs = check_sonar_args()
        if not kwargs:
            return
        client = sonarqube_client.BaseClient(**kwargs)

        qualitygates_list = client.get_qualitygates_list()
        print('result of get_qualitygates_list: {}'.format(qualitygates_list))

    def test_get_analysis_result(self):
        kwargs = check_sonar_args()
        if not kwargs:
            return
        client = sonarqube_client.BaseClient(**kwargs)

        analysis_result = client.get_analysis_result(sonar_project_key)
        assert isinstance(analysis_result, dict)
        assert 'measures' in analysis_result
        print('result analysis of {} is {}'.format(sonar_project_key, analysis_result))

    def test_get_component_tasks(self):
        kwargs = check_sonar_args()
        if not kwargs:
            return
        client = sonarqube_client.BaseClient(**kwargs)
        ret = client.get_component_tasks(sonar_project_key)

        assert isinstance(ret, dict)
        assert 'queue' in ret
        assert 'current' in ret

    def test_get_qualitygates_project_status(self):
        if not sonar_project_key:
            return

        kwargs = check_sonar_args()
        if not kwargs:
            return
        client = sonarqube_client.BaseClient(**kwargs)
        ret = client.get_qualitygates_project_status(sonar_project_key)
        
        assert isinstance(ret, dict)
        assert 'projectStatus' in ret

    def test_get_component(self):
        kwargs = check_sonar_args()
        if not kwargs:
            return
        client = sonarqube_client.BaseClient(**kwargs)

        project_key = sonar_project_key
        ret = client.get_component(project_key)
        
        assert isinstance(ret, dict)
        assert 'component' in ret

    def test_reset_settings(self):
        kwargs = check_sonar_args()
        if not kwargs:
            return
        client = sonarqube_client.BaseClient(**kwargs)

        key = "sonar.webhooks.project"
        ret = client.reset_settings(sonar_project_key, key)
        print(ret)