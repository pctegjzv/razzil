import logging

from django.conf import settings

from mekansm.request import MekRequest

logger = logging.getLogger(__name__)


class JakiroInternalRequest(MekRequest):
    endpoint = '{}/{}'.format(settings.JAKIRO_INTERNAL_ENDPOINT, settings.JAKIRO_INTERNAL_VERSION)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': 'razzil/v1.0'
    }


class JakiroInternalClient(object):
    @classmethod
    def get_resources(cls, resource_id):
        return JakiroInternalRequest.send('inner/resources/{}/'.format(resource_id))['data']
