import os


def str2bool(v):
    return v and v.lower() in ('yes', 'true', 't', '1')

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
COMPONENT_NAME = 'razzil'


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'g)0ogc*oowqm5h&(msgt4mq!)7&bjth6795jj$=*-t7u^%ipe5'


if 'DEBUG' in os.environ:
    TEMPLATE_DEBUG = DEBUG = os.getenv('DEBUG').lower() in ['true', 'yes', 'y']
else:
    TEMPLATE_DEBUG = DEBUG = False

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'django.contrib.contenttypes',
    'private_builds',
    'private_build_configs',
    'private_code_clients',
    'ci_envs',
    'dockerfile_manage'
]

MIDDLEWARE_CLASSES = [
    'django.middleware.common.CommonMiddleware',
    'mekansm.contrib.django.middleware.MekAPIMiddleware',
]

ROOT_URLCONF = 'config.urls'

WSGI_APPLICATION = 'config.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

def _get_db_engine(name):
    engines = {
        "postgresql": "django.db.backends.postgresql_psycopg2",
        "mysql": "django.db.backends.mysql"
    }
    if name in engines:
        return engines[name]
    else:
        return name

DATABASES = {'default': {
    'ENGINE': _get_db_engine(os.getenv('DB_ENGINE', 'django.db.backends.postgresql_psycopg2')),
    'NAME': os.getenv('DB_NAME'),
    'USER': os.getenv('DB_USER'),
    'HOST': os.getenv('DB_HOST'),
    'PASSWORD': os.getenv('DB_PASSWORD'),
    'PORT': os.getenv('DB_PORT', '5432'),
    'CONN_MAX_AGE': os.getenv('CONN_MAX_AGE', 3600),
}}

REGION_MANAGER = {
    'name': 'furion',
    'endpoint': os.getenv('FURION_URL'),
    'api_version': os.getenv('FURION_API_VERSION', 'v1'),
    'username': os.getenv('FURION_USERNAME'),
    'password': os.getenv('FURION_PASSWORD')
}

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'en-us'

CUR_TIME_ZONE = os.getenv('CUR_TIME_ZONE', 'UTC')
TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOG_PATH = '/var/log/mathilde/'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(asctime)s [%(levelname)s][%(threadName)s]' +
                      '[%(name)s:%(lineno)d] %(message)s'
        },
        'colored': {
            '()': 'colorlog.ColoredFormatter',
            'format': '%(log_color)s%(levelname)-8s%(reset)s %(blue)s%(message)s'
        }
    },
    'filters': {
        'require-debug-true': {
            '()': 'django.utils.log.RequireDebugTrue'
        },
        'require-debug-false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'console-dev': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'colored',
            'filters': ['require-debug-true']
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
            'filters': ['require-debug-false']
        },
        'file-info': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filters': ['require-debug-false'],
            'filename': LOG_PATH + 'razzil.info.log',
            'maxBytes': 1024 * 1024 * 1024,
            'backupCount': 1
        },
        'file-error': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filters': ['require-debug-false'],
            'filename': LOG_PATH + 'razzil.error.log',
            'maxBytes': 1024 * 1024 * 1024,
            'backupCount': 1
        },
        'file-debug': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filters': ['require-debug-true'],
            'filename': LOG_PATH + 'razzil.debug.log',
            'maxBytes': 1024 * 1024 * 1024,
            'backupCount': 1
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console-dev', 'console', 'file-info', 'file-error'],
            'level': 'INFO',
            'propagate': False
        },
        '': {
            'handlers': ['console-dev', 'console', 'file-info', 'file-error', 'file-debug'],
            'level': 'DEBUG' if DEBUG else 'INFO'
        },
    }
}


REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (),
    'EXCEPTION_HANDLER': 'mekansm.contrib.rest_framework.exception_handler.exception_handler',
    'UNAUTHENTICATED_USER': None
}

JAKIRO_API_ENDPOINT = os.getenv('JAKIRO_API_ENDPOINT')
JAKIRO_API_VERSION = os.getenv('JAKIRO_API_VERSION', 'v1')
JAKIRO_INTERNAL_ENDPOINT = os.getenv('JAKIRO_INTERNAL_ENDPOINT')
JAKIRO_INTERNAL_VERSION = os.getenv('JAKIRO_INTERNAL_VERSION', 'v1')
LUCIFER_API_ENDPOINT = os.getenv('LUCIFER_API_ENDPOINT')
LUCIFER_API_VERSION = os.getenv('LUCIFER_API_VERSION', 'v1')
DOCKER_BUILD_TIMEOUT = int(os.getenv('DOCKER_BUILD_TIMEOUT', 60))
SOURCE = os.getenv('SOURCE')
PUBLIC_ENDPOINT_NAME = os.getenv('PUBLIC_ENDPOINT_NAME', 'PUBLIC')
OSS_ENDPOINT = os.getenv('OSS_ENDPOINT')
MEDUSA_ENDPOINT = os.getenv('MEDUSA_ENDPOINT')
MEDUSA_API_VERSION = os.getenv('MEDUSA_API_VERSION', 'v1')
DAVION_ENDPOINT = os.getenv('DAVION_ENDPOINT')
DAVION_API_VERSION = os.getenv('DAVION_API_VERSION', 'v1')
ALAUDA_SONARSCANNER_IMAGE = os.getenv('ALAUDA_SONARSCANNER_IMAGE')
RATTLETRAP_IMAGE = os.getenv('ALAUDA_RATTLETRAP_IMAGE')
SONARQUBE_SCAN_TIMEOUT = int(os.getenv('SONARQUBE_SCAN_TIMEOUT', 60))
FURION_URL = os.getenv('FURION_URL')
LOGGING_STORAGE = {
    'host': os.getenv('LOGGING_STORAGE_HOST') or '52.11.35.84',
    'port': os.getenv('LOGGING_STORAGE_PORT') or '9200',
    'username': os.getenv('LOGGING_STORAGE_USERNAME') or 'sys_admin',
    'password': os.getenv('LOGGING_STORAGE_PASSWORD') or '07Apples'
}

GITHUB = {
    'client_id': os.getenv('GITHUB_CLIENT_ID'),
    'client_secret': os.getenv('GITHUB_CLIENT_SECRET')
}

BITBUCKET = {
    'client_id': os.getenv('BITBUCKET_CLIENT_ID'),
    'client_secret': os.getenv('BITBUCKET_CLIENT_SECRET')
}

GIT_IMAGE_TAG_RULE = {
    'branch_name',
    'commit_hash',
    'author_name',
    'committer_name',
    'author_date',
    'commit_date',
    'version'
}

SVN_IMAGE_TAG_RULE = {
    'commit_author',
    'commit_date',
    'commit_revision',
    'repository_uuid',
    'version'
}

TRESDIN_CLIENT = {
    'endpoint': os.getenv('TRESDIN_ENDPOINT'),
    'api_version': os.getenv('TRESDIN_API_VERSION')
}

CUSTOM_ENVVAR_PREFIX = os.getenv('CUSTOM_ENVVAR_PREFIX')
CI_WORKER_HOST_MODE = str2bool(os.getenv('CI_WORKER_HOST_MODE', 'f'))
SVN_TRUST_SERVER_CERT_FAILURES = str2bool(os.getenv('SVN_TRUST_SERVER_CERT_FAILURES', 'f'))

CUSTOM_CODE_REPO_CLIENT_LIST = os.getenv('CUSTOM_CODE_REPO_CLIENT_LIST', '')
CACHE_EXPIRED_TIME = 86400
CI_REPORT_ENABLED = os.getenv('CI_REPORT_ENABLED', False)

REDIS_CLIENT_KEY = 'READER'

CI_CPU = os.getenv("CI_CPU")
CI_MEMORY = os.getenv("CI_MEMORY")
