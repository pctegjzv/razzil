import logging
import time
import traceback
from alauda_redis_client import RedisClientFactory

from django.conf import settings


class BuildSameCommitLocker(object):
  KEY_EXPIRE = 20
  OWNER_KEY_EXPIRE = 60
  WAIT_OWNER_TIMEOUT = 10

  redis_client = RedisClientFactory.get_client_by_key(settings.REDIS_CLIENT_KEY)

  def __init__(self, build, scope_filter):
      self.build = build
      self.scope_filter = scope_filter
      self.commit_id = self._get_commit()
      self.lock_key = self._get_lock_key(self.commit_id)
      self.owner_key = "{}__owner".format(self.lock_key)
  
  def _get_lock_key(self, commit_id):
    key = ""
    if self.scope_filter.get('image_repo_id'):
      key = "build_locker__image__{}__{}".format(
          self.scope_filter.get('image_repo_id'),
          commit_id
      )
    else:
      key = "build_locker__config__{}__{}".format(
          self.scope_filter.get('config__config_id'),
          commit_id
      )
    return key

  def _get_commit(self):
    return self.build['code_repo'].get('code_head_commit')

  def lock(self):
    # allow empty commit to get locker any time
    if not self.commit_id:
      return True

    result = BuildSameCommitLocker.redis_client.incr(self.lock_key)
    if result == 1:
      BuildSameCommitLocker.redis_client.expire(self.lock_key, BuildSameCommitLocker.KEY_EXPIRE)
      logging.info("BuildLock: got lock {} succeed".format(self.lock_key))
      return True

    logging.info("BuildLock: got lock {} fail".format(self.lock_key))
    return False

  def wait(self):
    logging.info("BuildLock: waiting lock {}".format(self.lock_key))

    interval = 0.1
    timeout_counts = BuildSameCommitLocker.WAIT_OWNER_TIMEOUT/interval
    count =0

    while(count<timeout_counts):
      count = count+1
      build_id = self._get_unlock_owner()
      if not build_id:
        time.sleep(interval)
        continue

      logging.info("BuildLock: waited lock {}, got owner:{}".format(
        self.lock_key, build_id))
      return build_id

    logging.warn("BuildLock: waited lock key:{}'s owner timeout, lock_key:{}".format(self.lock_key))
    return None

  def _set_unlock_owner(self, build_id):
    BuildSameCommitLocker.redis_client.set(
      self.owner_key, build_id,
      ex=BuildSameCommitLocker.OWNER_KEY_EXPIRE)

  def _get_unlock_owner(self):
    return BuildSameCommitLocker.redis_client.get(self.owner_key)

  def unlock(self, build_id):
    # allow empty commit to unlock any time
    if not self.commit_id:
      return True
    logging.info("BuildLock: deleting lock key:{}".format(self.lock_key))
    BuildSameCommitLocker.redis_client.delete(self.lock_key)

    logging.info("BuildLock: seting lock key owner, key:{} owner:{}".format(self.lock_key, build_id))
    self._set_unlock_owner(build_id)
    logging.info("BuildLock: released lock {} succeed, owner:{}".format(self.lock_key, build_id))