# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import traceback
import uuid
import re
import logging
from jsonfield import JSONField

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from commons.docker_build_exception import DockerBuildException
from commons.fields import EncryptedCharField
from private_build_configs.cache import AlaudaCache
from dockerfile_manage.models import Dockerfile


LOG = logging.getLogger(__name__)
AUTO_TAG_TYPE_TIME = 'TIME'
AUTO_TAG_TYPE_COMMIT = 'COMMIT'
AUTO_TAG_TYPE_CUSTOM_RULE = 'CUSTOM_RULE'
AUTO_TAG_TYPE_NONE = 'NONE'
AUTO_TAG_TYPES = ((AUTO_TAG_TYPE_TIME, 'TIME'),
                  (AUTO_TAG_TYPE_COMMIT, 'COMMIT'),
                  (AUTO_TAG_TYPE_CUSTOM_RULE, 'CUSTOM_RULE'),
                  (AUTO_TAG_TYPE_NONE, 'NONE')
                  )

R_ONE_ASTERISK = r"(?<!\*)\*{1}(?!\*)"
R_WORDS = r"[A-Za-z0-9_-]*"
R_MULTI_ASTERISK = r"\*{2,}"
R_ANY_CHAR = r"\S*"

LOG = logging.getLogger(__name__)

@python_2_unicode_compatible
class BuildConfig(models.Model):
    config_id = models.CharField(
        max_length=36, primary_key=True, default=uuid.uuid4)
    dockerfile = models.ForeignKey(Dockerfile, null=True)
    image_repo_id = models.CharField(max_length=36, null=True, blank=True,default='')
    customize_tag = models.CharField(max_length=32, null=True, blank=True, default='')
    auto_tag_type = models.CharField(max_length=16,
                                     blank=True, null=True,
                                     choices=AUTO_TAG_TYPES,
                                     default=AUTO_TAG_TYPE_NONE)
    customize_tag_rule = models.CharField(max_length=255, default='', blank=True)
    code_cache_enabled = models.BooleanField(default=False)
    image_cache_enabled = models.BooleanField(default=False)
    auto_build_enabled = models.BooleanField(default=False)
    notification_id = models.CharField(max_length=36, null=True, blank=True)
    ci_enabled = models.BooleanField(default=False)
    build_image_enabled = models.BooleanField(default=True)
    build_enabled = models.BooleanField(default=True)
    artifact_upload_enabled = models.BooleanField(default=False)
    allow_artifact_upload_fail = models.BooleanField(default=False)
    endpoint_id = models.CharField(max_length=64, default='', blank=True)
    ci_envs = JSONField(blank=True, default={})
    ci_image_name = models.CharField(max_length=512, blank=True, default='')
    ci_image_tag = models.CharField(max_length=128, blank=True, default='')
    ci_config_file_location = models.CharField(max_length=255, blank=True)
    ci_steps = JSONField(blank=True, default=[])
    schedule_config_id = models.CharField(max_length=36, default='', blank=True)
    schedule_config_secret_key = models.CharField(max_length=16, default='', blank=True)
    cpu = models.FloatField(default=0.5)
    memory = models.IntegerField(default=512)
    build_filters = JSONField(blank=True, default={})
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    project_name = models.CharField(max_length=128, null=True, default='')

    class Meta:
        ordering = ('-updated_at',)

    def __str__(self):
        return self.config_id

    @property
    def exec_cmd_enabled(self):
        return self.ci_enabled and len(self.ci_steps) > 0

    def save(self, *args, **kwargs):
        if self.cpu < 0.25 or self.cpu > 1 or self.memory < 256 or self.memory > 2048:
            raise DockerBuildException("invalid_container_size")
        super(BuildConfig, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        try:
            code = BuildConfigCode.objects.get(build_config__config_id=self.config_id)
            if code.is_auto_build():
                AlaudaCache.delete(code.code_id)
        except:
            LOG.error(traceback.format_exc())
        finally:
            super(BuildConfig, self).delete(*args, **kwargs)

CODE_REPO_CLIENT_SIMPLE_GIT = 'SIMPLE_GIT'
CODE_REPO_CLIENT_SIMPLE_SVN = 'SIMPLE_SVN'
CODE_REPO_CLIENT_GITHUB = 'GITHUB'
CODE_REPO_CLIENT_BITBUCKET = 'BITBUCKET'
CODE_REPO_CLIENT_OSCHINA = 'OSCHINA'

CODE_REPO_WITHOUT_WEBHOOK = (CODE_REPO_CLIENT_SIMPLE_GIT, CODE_REPO_CLIENT_SIMPLE_SVN)
CODE_REPO_CLIENT_LIST = [
    CODE_REPO_CLIENT_GITHUB,
    CODE_REPO_CLIENT_BITBUCKET,
    CODE_REPO_CLIENT_OSCHINA,
]
CODE_REPO_CLIENT_ALL_LIST = CODE_REPO_CLIENT_LIST + [CODE_REPO_CLIENT_SIMPLE_GIT,CODE_REPO_CLIENT_SIMPLE_SVN]
CODE_REPO_CLIENT = (
    (CODE_REPO_CLIENT_SIMPLE_GIT, 'SIMPLE_GIT'),
    (CODE_REPO_CLIENT_SIMPLE_SVN, 'SIMPLE_SVN'),
    (CODE_REPO_CLIENT_GITHUB, 'GITHUB'),
    (CODE_REPO_CLIENT_BITBUCKET, 'BITBUCKET'),
    (CODE_REPO_CLIENT_OSCHINA, 'OSCHINA')
)

CODE_REPO_TYPE_BRANCH = 'BRANCH'
CODE_REPO_TYPE_DIR = 'DIR'
CODE_REPO_TYPE = (
    (CODE_REPO_TYPE_BRANCH, 'BRANCH'),
    (CODE_REPO_TYPE_DIR, 'DIR')
)


@python_2_unicode_compatible
class BuildConfigCode(models.Model):
    code_id = models.CharField(
        max_length=36, primary_key=True, default=uuid.uuid4, editable=False)
    build_config = models.OneToOneField(BuildConfig, on_delete=models.CASCADE,
                                        related_name='code_repo')
    code_repo_client = models.CharField(max_length=20, choices=CODE_REPO_CLIENT)
    repo_id = models.IntegerField(null=True)
    code_repo_path = models.CharField(max_length=512)
    code_repo_username = EncryptedCharField(max_length=120, blank=True)
    code_repo_password = EncryptedCharField(max_length=128, blank=True)
    code_repo_type = models.CharField(max_length=16, choices=CODE_REPO_TYPE)
    code_repo_type_value = models.CharField(max_length=64)
    dockerfile_location = models.CharField(max_length=128)
    dockerfile_content = models.TextField(null=True, blank=True)
    build_context_path = models.CharField(max_length=128)
    code_repo_webhook_id = models.CharField(max_length=128, null=True, blank=True)
    code_repo_webhook_code = models.CharField(max_length=16, null=True, blank=True, db_index=True)
    code_repo_key_id = models.IntegerField(null=True, blank=True)
    code_repo_private_key = models.TextField(null=True, blank=True)
    code_repo_public_key = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    head_commit_id = models.TextField(null=True, blank=True)
    ''' version 1，save  commit id of latest build
     version 2, save multi value that like master:commit_id^develop:commit_id ,
       we should save all branch changes that match the wildcard of code_repo_type_value''' 

    class Meta:
        ordering = ('-updated_at',)

    def __str__(self):
        return self.code_id

    def is_auto_build(self):
        return self.code_repo_client in CODE_REPO_WITHOUT_WEBHOOK \
               and self.build_config.auto_build_enabled

    def save(self, *args, **kwargs):
        self.refresh_head_commit_id()
        if self.is_auto_build():
            AlaudaCache.save_config(config_obj=self)
        else:
            AlaudaCache.delete(self.code_id)
        super(BuildConfigCode, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.is_auto_build():
            AlaudaCache.delete(self.code_id)
        super(BuildConfigCode, self).delete(*args, **kwargs)

    def refresh_head_commit_id(self):
        '''
        filter unusing `branch:commit_id` in field of `head_commit_id`
        case : when updated code_repo_type_valud from feature/* to feature/abc
               we should only save feature/abc:xxxx
        '''
        commit_infoes = BuildConfigCode.parse_head_commit_id(self.code_repo_type_value, 
                                                            self.head_commit_id)
        def fmt_filter(branch):
            valid, _1, _2 = BuildConfigCode.valid_runtime_code_repo_type_value(self.code_repo_type_value, branch)
            return valid
        self.head_commit_id = BuildConfigCode.format_commit_infoes(commit_infoes, fmt_filter)

    @staticmethod
    def valid_runtime_code_repo_type_value(code_repo_type_value, runtime_code_repo_type_value):
        '''
            valid runtime code_repo_type_value
            according by code_repo_type_value that wildcard setted by user
        '''

        #if not wildcard ,we will ignore runtime_params,just using code_repo_type_value
        if not BuildConfigCode.is_wildcard_of_repo_type_value(code_repo_type_value):
            if runtime_code_repo_type_value is  None:
                return True, code_repo_type_value, None
            equal = code_repo_type_value == runtime_code_repo_type_value
            if not equal:
                return False, None, '{0} is not equal to {1}'.format(runtime_code_repo_type_value,
                                                    code_repo_type_value)
            else:
                return True, runtime_code_repo_type_value, None 

        # when we using wildcard for  code_repo_type_value in build config
        # we should ensure set code_repo_type_value in building runtime
        if runtime_code_repo_type_value is None:
            return False, None, "runtime code_repo_type_value should not be None "
        if BuildConfigCode.is_match_repo_type_value_wildcard(code_repo_type_value, runtime_code_repo_type_value):
            return True, runtime_code_repo_type_value, None
        else:
            return False, None, "({0})  is not match ({1})".\
                        format(runtime_code_repo_type_value,code_repo_type_value)

    @staticmethod
    def is_wildcard_of_repo_type_value(value):
        """
        judge current code_repo_type_value is wildcard
        refer to design of jenkins
        """
        if value is None:
            raise Exception("value should not be None!")
        return "*" in value

    @staticmethod
    def is_match_repo_type_value_wildcard(wildcard, value):
        '''
        is match code_repo_type_value wildcard
        '''
        reg = re.sub(R_ONE_ASTERISK, R_WORDS, wildcard)
        reg = re.sub(R_MULTI_ASTERISK, R_ANY_CHAR, reg)
        reg = "^" + reg + "$"

        return re.match(reg, value, re.I) is not None
   
    def set_head_commit_id(self, branch_name, commit_id):
        '''
            head_commit_id of `build_config_code` format
            is like: master:xxxxx^develop:xxxxx^feature/a1:xxxxx
            if we set special branch's commit id ,we should parse it first then update  it
        '''
        commit_infoes = BuildConfigCode.parse_head_commit_id(self.code_repo_type_value, 
                                                                self.head_commit_id)
        commit_infoes[branch_name] = {"commit_id": commit_id}
        self.head_commit_id = BuildConfigCode.format_commit_infoes(commit_infoes)

    @staticmethod    
    def format_commit_infoes(commit_infoes, fmt_filter=None):
        '''
        Args: 
            `commit_infoes`: { master: {commit_id: xxx}, develop: {commit_id: xxxx} }
        Returns: 
            <string> branch_name:commit_id^branch_name:commit_id

        '''
        if commit_infoes is None:
            return ""
        filter_list = filter(fmt_filter, commit_infoes.keys())
        # map to [ "master:xxx", "develop:xxx", "feature:xxxx" ]
        lst = map(lambda branch:"{}:{}".format(branch,
                                        commit_infoes[branch]["commit_id"] ),
                                        filter_list)                           
        return "^".join(lst)


    @staticmethod
    def parse_head_commit_id(config_code_repo_type_value, head_commit_id):
        '''
        Args: 
            config_code_repo_type_value: value of build config,may be wildcard 
            head_commit_id: branch_name:commit_id^branch_name:commit_id  
        Returns: 
            { master:{commit_id:xxx}, develop:{commit_id:xxxx} }
        '''
        if not head_commit_id:
            return {}
        #if head_commit_id is old format that only commit_id
        if "^" not in head_commit_id and ":" not in head_commit_id:
            if not BuildConfigCode.is_wildcard_of_repo_type_value(config_code_repo_type_value):
                return {config_code_repo_type_value:{"commit_id":head_commit_id}}
            else:
                LOG.warn('''code_repo_type_value={} or head_commit_id={}
                        is something wrong when parse head_commit_id'''.format(
                                 config_code_repo_type_value, head_commit_id))
                return {}
        # config_code_repo_type_value is wildcard
        data = {}
        segs = head_commit_id.split("^")
        for seg in segs:
            i = seg.index(":")
            if seg[0:i].strip() != '' and seg[i+1:].strip() != '':
                data[seg[0:i]] = {"commit_id":seg[i+1:]}

        return data

INTEGRATION_SONARQUBE = 'SONARQUBE'
INTEGRATION_TYPE = (
    (INTEGRATION_SONARQUBE, INTEGRATION_SONARQUBE),
)


@python_2_unicode_compatible
class BuildConfigIntegration(models.Model):
    id  = models.CharField(
        db_column="id", max_length=36, primary_key=True, default=uuid.uuid4, editable=False)
    type = models.CharField(max_length=32, choices=INTEGRATION_TYPE, null=False)
    integration_instance_id = models.CharField(max_length=36, null=False, blank=False)
    build_integration_config = JSONField(blank=True, default={})
    build_config_id = models.ForeignKey(
        BuildConfig, db_column="build_config_id", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-updated_at',)

    def __str__(self):
        return self.id
