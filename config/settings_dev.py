from .settings import *

DATABASES = {'default': {
    'ENGINE': 'django.db.backends.sqlite3',
    'NAME': 'razzil_dev.db',
    'USER': '',
    'HOST': "127.0.0.1",
    'PORT': "6432",
}}