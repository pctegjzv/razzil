from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


class CIEnvsCatalogManager(models.Manager):
    def get_ci_images(self, envs_dict):
        queryset = self.filter(language=envs_dict['language'])
        runtimes = envs_dict.get('runtimes', None)

        if runtimes is None:
            return [
                {"image": ci_env.image, "tag": ci_env.tag}
                for ci_env in queryset
                if not ci_env.has_addons
            ]

        ci_images = []
        for runtime in runtimes:
            ci_env = queryset.get(runtime=runtime['name'],
                                  has_addons=runtime.get('has_addons', False))
            ci_images.append({
                "image": ci_env.image,
                "tag": ci_env.tag
            })
        return ci_images


@python_2_unicode_compatible
class CIEnvsCatalog(models.Model):
    language = models.CharField(max_length=128)
    runtime = models.CharField(max_length=128)
    is_default = models.BooleanField()
    has_addons = models.BooleanField()
    image = models.CharField(max_length=128)
    tag = models.CharField(max_length=255)
    default_ci_steps = models.TextField(default='', blank=True)
    objects = CIEnvsCatalogManager()

    class Meta:
        ordering = ('language', 'runtime')
        unique_together = (
            ('language', 'runtime', 'has_addons'),
            ('image', 'tag')
        )

    def __str__(self):
        return '{}:{}'.format(self.language, self.runtime)
