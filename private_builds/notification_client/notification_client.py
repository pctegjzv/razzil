# -*- coding: utf-8 -*-

import logging

from django.conf import settings

from mekansm.request import MekRequest

from private_builds.models import STATUS_FAILED, STATUS_SUCCEEDED


LOG = logging.getLogger(__name__)


class LuciferRequest(MekRequest):
    endpoint = '{}/{}'.format(settings.LUCIFER_API_ENDPOINT, settings.LUCIFER_API_VERSION)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': 'razzil/v1.0'
    }


class NotificationClient(object):

    @classmethod
    def send_message(cls, notification_id, status, build_id):
        data = {
            "template_type": "build",
            "data": {
                "subject": str(build_id),
                "content": None
            }
        }
        if status == STATUS_FAILED:
            data['data']['content'] = 'Failed'
        elif status == STATUS_SUCCEEDED:
            data['data']['content'] = 'Succeeded'
        try:
            LuciferRequest.send('notifications/{}/messages/'.format(notification_id),
                                method='POST', data=data)
        except Exception, ex:
            LOG.error("Send notification message error:{}".format(ex))
