import json
import logging
import requests

from django.conf import settings

from mekansm.request import MekRequest
from mekansm.exceptions import MekServiceException

from commons.docker_build_exception import DockerBuildException
from private_code_clients.models import CodeClientUser
from private_code_clients.serializers import CodeClientUserSerializer
from private_build_configs.models import CODE_REPO_TYPE_BRANCH, CODE_REPO_CLIENT_BITBUCKET, BuildConfigCode

from .code_client_base import CodeClientBase
from .web_hook_data import WebHookData


LOG = logging.getLogger(__name__)


class BitbucketClient(CodeClientBase):
    CONFIG = {
        'bitbucket_auth_uri': 'https://bitbucket.org/site/oauth2',
        'bitbucket_api_base_uri': 'https://api.bitbucket.org/2.0',
        'bitbucket_api_v1_base_uri': 'https://api.bitbucket.org/1.0',
        'client_id': settings.BITBUCKET['client_id'],
        'client_secret': settings.BITBUCKET['client_secret'],
    }

    def __init__(self):
        super(BitbucketClient, self).__init__()
        self.bits = 2048 # bitbucket request length of key should be at least 2048 bits

    def load(self, namespace):
        try:
            code_client_user = CodeClientUser.objects.get(
                code_repo_client=CODE_REPO_CLIENT_BITBUCKET,
                user_name=namespace)
        except CodeClientUser.DoesNotExist:
            LOG.warning('load client user {}/{} failed, '
                        'not found in db'.format(CODE_REPO_CLIENT_BITBUCKET, namespace))
            return False
        self.code_client_user = CodeClientUserSerializer(code_client_user).data
        return True

    def get_auth_url(self, state):
        auth_url = '{}/authorize?client_id={}&state={}'.format(
            self.CONFIG['bitbucket_auth_uri'], self.CONFIG['client_id'],
            state
        )
        return auth_url

    def get_access_token(self, request_data={}):
        if self.code_client_user.get('access_token'):
            return self.code_client_user['access_token']
        client_user_id = request_data.get('state')
        provider_code = request_data.get('code')
        redirect_uri = request_data.get('redirect_uri')
        if not client_user_id or not provider_code or not redirect_uri:
            raise DockerBuildException('wrong_auth_callback_data')
        data = {
            'grant_type': 'authorization_code',
            'code': provider_code,
            'redirect_uri': redirect_uri
        }
        access_token_result = requests.post(
            '{}/access_token'.format(self.CONFIG['bitbucket_auth_uri']),
            data=data, auth=(self.CONFIG['client_id'], self.CONFIG['client_secret'])
        ).json()
        if not access_token_result.get('access_token'):
            raise DockerBuildException('get_token_failed',
                                       message_params=json.dumps(access_token_result))
        access_token = access_token_result['access_token']
        refresh_token = access_token_result.get('refresh_token')
        return access_token, refresh_token

    @staticmethod
    def refresh_token(refresh_token):
        LOG.info('The original token is expired, refreshing the token...')
        data = {
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token
        }
        access_token_result = requests.post(
            '{}/access_token'.format(BitbucketClient.CONFIG['bitbucket_auth_uri']),
            data=data, auth=(BitbucketClient.CONFIG['client_id'],
                             BitbucketClient.CONFIG['client_secret'])
        ).json()
        access_token = access_token_result.get('access_token')
        refresh_token = access_token_result.get('refresh_token')
        return access_token, refresh_token

    def get_client_login(self, user_name):
        headers = self._get_headers()
        user_result = BitbucketAPIRequest.send_request(
            'user', method='GET', headers=headers, user_name=user_name)['data']
        return user_result['username']

    def get_client_orgs(self, *args, **kwargs):
        headers = self._get_headers()
        params = {
            'role': 'member',
            'page': kwargs.get("page", 1),
            'pagelen': kwargs.get("pagelen", 10)
        }
        org_list_result = BitbucketAPIRequest.send_request(
            'teams', method='GET', headers=headers, params=params,
            user_name=self.code_client_user['user_name'])['data']
        org_list = [
            {'name': org['username']} for org in org_list_result['values']
        ]
        org_list.insert(0, {'name': self.code_client_user['code_repo_client_user_name']})
        return {
            'size': org_list_result['size'],
            'page': org_list_result['page'],
            'pagelen': org_list_result['pagelen'],
            'values': org_list
        }

    def get_repo_list(self, code_client_org, *args, **kwargs):
        headers = self._get_headers()
        params = {
            'page': kwargs.get("page", 1),
            'pagelen': kwargs.get("pagelen", 10)
        }
        repo_list_result = BitbucketAPIRequest.send_request(
            'repositories/{}'.format(code_client_org),
            method='GET', headers=headers, params=params,
            user_name=self.code_client_user['user_name'])['data']
        repo_list = [
            {'code_repo_path': repo['name'].lower(), 'repo_id': ''}
            for repo in repo_list_result['values']
        ]
        return {
            'size': repo_list_result['size'],
            'page': repo_list_result['page'],
            'pagelen': repo_list_result['pagelen'],
            'values': repo_list
        }

    def create_deploy_key(self, code_repo_path, repo_id, description=None):
        deploy_key = super(BitbucketClient, self).create_deploy_key(code_repo_path, repo_id, description)

        data = {
            'label': deploy_key['description'],
            'key': deploy_key['public_key']
        }
        headers = self._get_headers()
        result = BitbucketAPIV1Request.send_request(
            'repositories/{}/deploy-keys'.format(code_repo_path),
            method='POST', headers=headers, data=data,
            user_name=self.code_client_user['user_name'])['data']

        if not result.get('pk'):
            raise DockerBuildException('failed_verify_deploy_keys')

        deploy_key['id'] = result['pk']
        return deploy_key

    def delete_deploy_key(self, code_repo_path, id, repo_id):
        headers = self._get_headers()
        result = BitbucketAPIV1Request.send_request(
            'repositories/{}/deploy-keys/{}'.format(code_repo_path, id),
            method='DELETE', headers=headers,
            user_name=self.code_client_user['user_name'])
        return result

    def create_webhook(self, code_repo_path, namespace, repo_id):
        webhook = super(BitbucketClient, self).create_webhook(code_repo_path, namespace, repo_id)

        data = {
            'description': 'alauda webhook for auto build',
            'active': True,
            'url': webhook['url'],
            'events': ['repo:push']
        }
        headers = self._get_headers()
        result = BitbucketAPIRequest.send_request(
            'repositories/{}/hooks'.format(code_repo_path),
            method='POST', headers=headers, data=data,
            user_name=self.code_client_user['user_name'])['data']

        webhook['id'] = result.get('uuid')
        return webhook

    def delete_webhook(self, code_repo_path, id, repo_id):
        headers = self._get_headers()

        result = BitbucketAPIRequest.send_request(
            'repositories/{}/hooks/{}'.format(code_repo_path, id),
            method='DELETE', headers=headers,
            user_name=self.code_client_user['user_name'])['data']
        return result

    def create_repo_webhook(self, code_repo_path, hook_url):
        payload = {
            "url": hook_url,
            "active": True,
            "events": ["repo:push"],
            "description": "webhook added by alauda-razzil"
        }
        headers = self._get_headers()

        result = BitbucketAPIRequest.send_request(
            'repositories/{}/hooks'.format(code_repo_path),
            method='POST', headers=headers, data=payload,
            user_name=self.code_client_user['user_name'])['data']
        if result.get('uuid'):
            result['uuid'] = result.get('uuid').replace('{', '').replace('}', '')
        return result

    def delete_repo_webhook(self, code_repo_path, hook_id):
        headers = self._get_headers()
        return BitbucketAPIRequest.send_request(
            'repositories/{}/hooks/{}'.format(code_repo_path, hook_id),
            method='DELETE', headers=headers,
            user_name=self.code_client_user['user_name'])

    def parse_web_hook_payload(self, payload):
        LOG.debug('Bitbucket webhook is comming: {}'.format(payload))
        if not payload['push']['changes'][0].get('new'):
            raise DockerBuildException(
                'invalid_args',
                message_params='webhokk payload does not contain new commit data')
        new_change = payload['push']['changes'][0]['new']
        code_repo_type_value = new_change['name']
        code_head_commit = new_change['target']['hash']
        web_hook_data = WebHookData(CODE_REPO_TYPE_BRANCH, code_repo_type_value, code_head_commit)
        return web_hook_data

    def parse_web_hook_post_data(self, post_data, code_repo):
        web_hook_data = self.parse_web_hook_payload(post_data)

        LOG.debug('Bitbucket webhook useful data, code_repo_type: {}, code_repo_type_value: {}, '
                  'code_head_commit: {}'.format(code_repo['code_repo_type'], web_hook_data.code_repo_type_value,
                                                web_hook_data.code_head_commit))

        if not CODE_REPO_TYPE_BRANCH == code_repo['code_repo_type']:
            raise DockerBuildException('invalid_args', "{}!={}".format(
                    CODE_REPO_TYPE_BRANCH, code_repo['code_repo_type']))
        valid, _1, err = BuildConfigCode.valid_runtime_code_repo_type_value(code_repo['code_repo_type_value'],
            web_hook_data.code_repo_type_value)
        if not valid:
            LOG.error('invalid_args {}'.format(err))
            raise DockerBuildException('invalid_args', err)

        return web_hook_data


    def get_full_path(self, code_repo_path, username, password):
        return 'git@bitbucket.org:{}.git'.format(code_repo_path)

    def _get_headers(self):
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'User-Agent': 'alauda'
        }
        if self.code_client_user.get('access_token'):
            headers['Authorization'] = 'Bearer ' + self.code_client_user['access_token']
        return headers


class BitbucketAPIRequest(MekRequest):
    endpoint = BitbucketClient.CONFIG['bitbucket_api_base_uri']

    @classmethod
    def send_request(cls, url, method='GET', headers=None, params=None, data=None, user_name=None):
        try:
            return cls.send(url, method=method, params=params, headers=headers, data=data)
        except MekServiceException as ex:
            if ex.status_code == 401:
                if not user_name:
                    raise DockerBuildException("token_expired")
                client_user = CodeClientUser.objects.get(
                    code_repo_client=CODE_REPO_CLIENT_BITBUCKET, user_name=user_name)
                if not client_user.refresh_token:
                    client_user.delete()
                    raise DockerBuildException("token_expired")
                access_token, refresh_token = BitbucketClient.refresh_token(
                    client_user.refresh_token)
                client_user.access_token = access_token
                client_user.refresh_token = refresh_token
                client_user.save()
                headers['Authorization'] = 'Bearer {}'.format(access_token)
                try:
                    return cls.send(url, method=method, params=params,
                                    headers=headers, data=data)
                except MekServiceException as err:
                    if err.status_code == 401:
                        client_user.delete()
                        raise DockerBuildException("token_expired")
                    else:
                        raise DockerBuildException(
                            'bitbucket_error', "bitbucket error code={}, message={}".format(
                                err.code, err.message))
            else:
                raise DockerBuildException(
                    'bitbucket_error', "bitbucket error code={}, message={}".format(
                        ex.code, ex.message))


class BitbucketAPIV1Request(BitbucketAPIRequest):
    endpoint = BitbucketClient.CONFIG['bitbucket_api_v1_base_uri']
