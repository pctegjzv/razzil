import unittest
import re
from private_build_configs.models import BuildConfigCode

class BuildConfigCodeTest(unittest.TestCase):

    def test_valid_runtime_code_repo_type_value(self):
        """
            test valid_runtime_params
        """
        data = [
            ["master", "master", True, "master"],
            ["master", "develop", False ],
            ["master", "foo/master", False ],
            ["feature/*", "feature/", True, "feature/"],
            ["feature/*", "feature/a", True, "feature/a"],
            ["feature/*", "feature/some", True, "feature/some"],
            ["feature/*", "feature/some1", True, "feature/some1"],
            ["feature/*", "abc/feature/some", False, "feature/some"],
            ["feature/*", "feature/some-foo_a1", True, "feature/some-foo_a1"],
            ["feature/*", "feature/some/foo", False ],
            ["feature/*", "foo/feature/some", False ],
            ["feature/**", "feature/", True, "feature/"],
            ["feature/**", "feature/a", True, "feature/a"],
            ["feature/**", "feature/foo-bar", True, "feature/foo-bar"],
            ["feature/**", "feature/foo/bar", True, "feature/foo/bar"],
            ["feature/**", "foo/feature/foo/bar", False ],
            ["**/feature", "foo/feature", True, "foo/feature"],
            ["**/feature", "foo/bar/feature", True, "foo/bar/feature"],
            ["feature/ci-**", "feature/ci-foo/bar", True, "feature/ci-foo/bar"],
            ["feature/ci-**", "foo/feature/ci-foo/bar", False ],

            ["feature/*/release", "feature/some/release/abc", False, "feature/some"],
            ["feature/*/release", "abc/feature/some/release/abc", False, "feature/some"],
        ]
        for item in data:
            valid, value, err = BuildConfigCode.valid_runtime_code_repo_type_value(item[0], item[1])
            self.assertEqual(valid, item[2], item[0]+" "+item[1]+" assert error")
            if valid:
                self.assertEqual(value, item[3])

    def test_set_head_commit_id(self):
        data = [
            [ BuildConfigCode(code_repo_type_value="master",head_commit_id=""), "master", "456",
                            "master:456" ],
            [ BuildConfigCode(code_repo_type_value="master",head_commit_id="123"), "master", "456",
                            "master:456" ],
            [ BuildConfigCode(code_repo_type_value="master/*",head_commit_id=""), "master/m1", "456",
                            "master/m1:456" ],
            [ BuildConfigCode(code_repo_type_value="master/*",head_commit_id="master/m1:123"), "master/m2", "456",
                            "master/m2:456^master/m1:123" ],
            [ BuildConfigCode(code_repo_type_value="master/*",head_commit_id="master/m1:123"), "master/m1", "456",
                            "master/m1:456" ],
            [ BuildConfigCode(code_repo_type_value="master/*",head_commit_id="master/m1:123^master/m2:456"), "master/m2", "222",    
                            "master/m2:222^master/m1:123" ],
            [ BuildConfigCode(code_repo_type_value="master/*",head_commit_id="master/m1:123^master/m2:456"), "master/m3", "333",    
                            "master/m3:333^master/m2:456^master/m1:123" ],
        ]
        for index, item in enumerate(data):
            model = item[0]
            branch_name = item[1]
            commit_id = item[2]
            res = item[3]
            model.set_head_commit_id(branch_name, commit_id)
            self.assertEqual(model.head_commit_id, res, "data[{0}] test failed".format(index))

    def test_refresh_commit_id(self):
        data = [
            [ BuildConfigCode(code_repo_type_value="master",head_commit_id=""),
                            "" ],
            [ BuildConfigCode(code_repo_type_value="master",head_commit_id="123"),
                            "master:123" ],
            [ BuildConfigCode(code_repo_type_value="master/*",head_commit_id=""),
                            "" ],
            [ BuildConfigCode(code_repo_type_value="master/*",head_commit_id="master/m1:123"),
                            "master/m1:123" ],
            [ BuildConfigCode(code_repo_type_value="master/*",head_commit_id="master/m1:123^develop:111^master/m2:222"),
                            "master/m2:222^master/m1:123" ],
            [ BuildConfigCode(code_repo_type_value="master/*",head_commit_id="master/m1:123^master/m2:456"),
                            "master/m2:456^master/m1:123" ],
            [ BuildConfigCode(code_repo_type_value="develop",head_commit_id="master/m1:123^master/m2:456^develop:111"), 
                            "develop:111" ],
        ]                           
        for index, item in enumerate(data):
            model = item[0]
            res = item[1]
            model.refresh_head_commit_id()
            self.assertEqual(model.head_commit_id, res, "data[{0}] test failed".format(index))            

if __name__ == '__main__':
    unittest.main()