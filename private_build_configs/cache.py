import json
import logging
import string

from alauda_redis_client import RedisClientFactory
from django.core.serializers.json import DjangoJSONEncoder
from django.conf import settings

GIT_PROTOCAL = ('git', 'ssh')
SVN_KEY = 'ALAUDA:SVN'
log = logging.getLogger(__name__)


class AlaudaCache(object):
    redis_client = RedisClientFactory.get_client_by_key(settings.REDIS_CLIENT_KEY)

    @classmethod
    def delete(cls, code_id):
        log.info('delete repo without webhook of {} from cache'.format(code_id))
        cls.redis_client.hdel(SVN_KEY, code_id)

    @classmethod
    def get_config_data(cls, config_obj):
        if config_obj:
            config_data = {
                'code_repo_path': config_obj.code_repo_path,
                'code_repo_username': config_obj.code_repo_username,
                'code_repo_password': config_obj.code_repo_password,
                'code_repo_webhook_code': config_obj.code_repo_webhook_code,
                'head_commit_id': config_obj.head_commit_id,
                "code_repo_client": config_obj.code_repo_client,  # SIMPLE_GIT / SIMPLE_SVN
                "code_repo_type_value": config_obj.code_repo_type_value,  # branch name
                # "code_repo_private_key": config_obj.code_repo_private_key,  # private key
            }
            return json.dumps(config_data)

    @classmethod
    def is_git_protocal(cls, path):
        path = path.lower()
        for protocal in GIT_PROTOCAL:
            if string.find(path, protocal) == 0:
                return True
        return False

    @classmethod
    def save_config(cls, queryset=None, config_obj=None):
        if config_obj and cls.redis_client.hgetall(SVN_KEY):
            # add config when save/update build config
            if not cls.is_git_protocal(config_obj.code_repo_path):
                code_id = config_obj.code_id
                data = cls.get_config_data(config_obj)
                cls.redis_client.hset(SVN_KEY, code_id, data)
                cls._print_repo_log(config_obj)
            return ''
        elif queryset:
            # add configs when expires in cache
            config_result = {}
            pipeline = cls.redis_client.pipeline()
            for q in queryset:
                if not cls.is_git_protocal(q.code_repo_path):
                    code_id = q.code_id
                    data = cls.get_config_data(q)
                    config_result[code_id] = data
                    pipeline.hset(SVN_KEY, code_id, data)
                    cls._print_repo_log(q)
            pipeline.expire(SVN_KEY, settings.CACHE_EXPIRED_TIME)
            pipeline.execute()
            return config_result

    @classmethod
    def _print_repo_log(cls, config_obj):
        repo_info = '{}:{}:{}'.format(config_obj.code_id, config_obj.code_repo_path,
                                      config_obj.code_repo_webhook_code)
        log.info('save repo without webhook of {} to cache'.format(repo_info))

    @classmethod
    def del_image_tag(cls, repository_id, tag_name):
        key_image_tag = '{}:{}'.format(repository_id, tag_name)
        cls.redis_client.delete(key_image_tag)

    @classmethod
    def get_image_tag(cls, repository_id, tag_name):
        key_image_tag = '{}:{}'.format(repository_id, tag_name)
        image_tag = cls.redis_client.get(key_image_tag)
        return image_tag

    @classmethod
    def save_image_tag(cls, image_tag_object, token):
        if image_tag_object:
            artifacts = image_tag_object.retrieve_artifacts(token)
            key_image_tag = '{}:{}'.format(image_tag_object.image_repo_id,
                                           image_tag_object.tag_name)
            data = json.dumps({
                'tag_name': image_tag_object.tag_name,
                'build_id': image_tag_object.build_id,
                'build_at': image_tag_object.updated_at,
                'artifacts_count': len(artifacts),
                'artifacts': artifacts,
            }, cls=DjangoJSONEncoder)
            cls.redis_client.set(key_image_tag, data)
            cls.redis_client.expire(key_image_tag, settings.CACHE_EXPIRED_TIME)
            return data
