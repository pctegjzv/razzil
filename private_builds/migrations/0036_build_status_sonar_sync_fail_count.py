# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-21 03:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('private_builds', '0035_build_build_filters'),
    ]

    operations = [
        migrations.AddField(
            model_name='build',
            name='status_sonar_sync_fail_count',
            field=models.SmallIntegerField(default=0, null=True),
        ),
    ]
