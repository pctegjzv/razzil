from commons.code_client.code_client_base import CodeClientBase
from commons.code_client.web_hook_data import WebHookData
from commons.docker_build_exception import DockerBuildException
from private_code_clients.models import CodeClientUser
from private_code_clients.serializers import CodeClientUserSerializer
from private_build_configs.models import CODE_REPO_TYPE_BRANCH, BuildConfigCode

from mekansm.request import MekRequest
from mekansm.exceptions import MekServiceException

import json
import logging
import hmac
import hashlib

LOG = logging.getLogger(__name__)


class OSChinaClient(CodeClientBase):
    CONFIG = {
        'oschina_api_base_url': 'http://git.oschina.net/api/v3',
    }

    def __init__(self):
        super(OSChinaClient, self).__init__()

    def load(self, namespace):
        try:
            code_client_user = CodeClientUser.objects.get(
                code_repo_client='OSCHINA', user_name=namespace)
        except CodeClientUser.DoesNotExist:
            LOG.warning('load client user {}/{} failed, '
                        'not found in db'.format('OSCHINA', namespace))
            return False
        self.code_client_user = CodeClientUserSerializer(code_client_user).data
        return True

    def parse_web_hook_payload(self, payload):
        code_repo_type = CODE_REPO_TYPE_BRANCH
        code_repo_type_value = None
        code_head_commit = None    
        if payload.get('after') and payload.get('ref'):
            code_head_commit = payload.get('after')
            code_repo_type_value = self._get_branch(payload.get('ref'))
        elif payload.get('hook'):
            hook = json.loads(payload.get('hook'))
            if hook.get('hook_name') == 'push_hooks':
                push_data = hook.get('push_data')
                code_repo_type_value = self._get_branch(push_data.get('ref'))
                code_head_commit = push_data.get('after')

        web_hook_data = WebHookData(code_repo_type,
                                    code_repo_type_value,
                                    code_head_commit)

        return web_hook_data

    def parse_web_hook_post_data(self, post_data, code_repo):
        web_hook_data = self.parse_web_hook_payload(post_data)

        if not web_hook_data.code_repo_type == code_repo['code_repo_type']:
            raise DockerBuildException('invalid_args', "{}!={}".format(
                    web_hook_data.code_repo_type, code_repo['code_repo_type']))
        valid, _1, err = BuildConfigCode.valid_runtime_code_repo_type_value(
            code_repo['code_repo_type_value'], web_hook_data.code_repo_type_value)
        if not valid:
            LOG.error('invalid_args {}'.format(err))
            raise DockerBuildException('invalid_args', err)

        return web_hook_data

    def _get_branch(self, ref):
        ref_prefix = 'refs/heads/'
        branch = None
        if ref_prefix in ref:
            branch = ref.replace(ref_prefix, '')
        return branch

    def get_client_login(self, user_name):
        headers = self._get_headers()
        user_result = OSChinaAPIRequest.send_request(
            'user', method='GET', headers=headers, user_name=user_name)['data']
        return user_result['username']

    def _get_headers(self):
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        if self.code_client_user.get('access_token'):
            headers['private-token'] = self.code_client_user['access_token']
        return headers

    def get_access_token(self, request_data):
        if self.code_client_user.get('access_token'):
            return self.code_client_user['access_token']
        oschina_user_email = request_data.get('oschina_user_email')
        oschina_user_pwd = request_data.get('oschina_user_pwd')
        if not oschina_user_email or not oschina_user_pwd:
            raise DockerBuildException('miss_args',
                                       message_params='oschina_user_email, oschina_user_pwd')
        data = {
            'email': oschina_user_email,
            'password': oschina_user_pwd
        }
        access_token_result = OSChinaAPIRequest.send_request(
            'session', method='POST', data=data
        )['data']
        access_token = access_token_result['private_token']
        return access_token, None

    def get_client_orgs(self, *args, **kwargs):
        headers = self._get_headers()
        org_list_result = OSChinaAPIRequest.send_request(
            'groups', method='GET', headers=headers,
            user_name=self.code_client_user['user_name'])['data']
        org_list = [
            {'name': org['path']} for org in org_list_result
        ]
        org_list.insert(
            0, {'name': self.code_client_user['code_repo_client_user_name']}
        )
        return org_list

    def get_repo_list(self, code_client_org, *args, **kwargs):
        headers = self._get_headers()
        if code_client_org == self.code_client_user['code_repo_client_user_name']:
            repo_list_result = OSChinaAPIRequest.send_request(
                'projects', method='GET', headers=headers,
                user_name=self.code_client_user['user_name'])['data']
        else:
            repo_list_result = OSChinaAPIRequest.send_request(
                'groups/{}/projects'.format(code_client_org),
                method='GET', headers=headers,
                user_name=self.code_client_user['user_name'])['data']['projects']
        repo_list = [
            {'code_repo_path': repo['path'], 'repo_id': repo['id']} for repo in repo_list_result
        ]
        return repo_list

    def create_deploy_key(self, code_repo_path, repo_id, description=None):
        deploy_key = super(OSChinaClient, self).create_deploy_key(code_repo_path, repo_id, description)
        data = {
            'title': deploy_key['description'],
            'key': deploy_key['public_key']
        }
        headers = self._get_headers()
        LOG.debug('create_deploy_key code_client_user: {}'.format(self.code_client_user))
        LOG.debug('create_deploy_key data:{}'.format(data))
        result = OSChinaAPIRequest.send_request(
            'projects/{}/keys'.format(repo_id),
            method='POST', headers=headers, data=data,
            user_name=self.code_client_user['user_name'])['data']
        if not result.get('id'):
            return DockerBuildException('failed_verify_deploy_keys')
        deploy_key['id'] = result['id']
        LOG.debug('create_deploy_key deploy_key: {}'.format(deploy_key))
        return deploy_key

    def delete_deploy_key(self, code_repo_path, id, repo_id):
        headers = self._get_headers()
        result = OSChinaAPIRequest.send_request(
            'projects/{}/keys/{}'.format(repo_id, id),
            method='DELETE', headers=headers,
            user_name=self.code_client_user['user_name'])
        LOG.debug('delete_deploy_key result: {}'.format(result))
        return result

    def create_webhook(self, code_repo_path, namespace, repo_id):
        webhook = super(OSChinaClient, self).create_webhook(code_repo_path, namespace, repo_id)
        headers = self._get_headers()
        signature = self.sign_signature(webhook['url'])
        data = {
            'url': webhook['url'],
            'password': signature[6:16]
        }
        LOG.debug('create_webhook data: {}'.format(data))
        result = OSChinaAPIRequest.send_request(
            'projects/{}/hooks'.format(repo_id),
            method='POST', headers=headers, data=data,
            user_name=self.code_client_user['user_name'])['data']
        LOG.debug('create_webhook request result: {}'.format(result))
        webhook['id'] = result.get('id')
        return webhook

    def delete_webhook(self, code_repo_path, id, repo_id):
        headers = self._get_headers()
        result = OSChinaAPIRequest.send_request(
            'projects/{}/hooks/{}'.format(repo_id, id),
            method='DELETE', headers=headers,
            user_name=self.code_client_user['user_name']
        )
        LOG.debug('delete_webhook result: {}'.format(result))
        return result

    def create_repo_webhook(self, code_repo_path, hook_url):
        pass

    def delete_repo_webhook(self, code_repo_path, hook_id):
        pass

    def get_full_path(self, code_repo_path, username, password):
        return 'git@git.oschina.net:{}.git'.format(code_repo_path)

    def sign_signature(self, text, secret=None):
        if secret is None:
            secret = 'www.mathildetech.com'
        return 'sha1=' + hmac.new(secret, text, hashlib.sha1).hexdigest()


class OSChinaAPIRequest(MekRequest):
    endpoint = OSChinaClient.CONFIG['oschina_api_base_url']

    @classmethod
    def send_request(cls, url, method='GET', headers=None, data=None, user_name=None):
        try:
            return OSChinaAPIRequest.send(url, method=method, headers=headers, data=data)
        except MekServiceException, ex:
            LOG.error('oschina request error: {}'.format(ex))
            if ex.status_code == 401:
                queryset = CodeClientUser.objects.filter(
                    code_repo_client='OSCHINA', user_name=user_name)
                if queryset.exists():
                    code_client_db_result = queryset.get()
                    code_client_db_result.delete()
                    raise DockerBuildException('token_expired')
                else:
                    raise DockerBuildException('os_china_login_error')
            else:
                raise DockerBuildException(
                    'os_china_error', 'oschina error code={}, message={}'.format(
                        ex.code, ex.message)
                )
