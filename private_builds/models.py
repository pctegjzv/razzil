from __future__ import unicode_literals

import uuid, logging

from jsonfield import JSONField

from django.conf import settings
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from commons.fields import EncryptedCharField
from commons.oss_client import OSS
from private_build_configs.cache import AlaudaCache
from private_build_configs.models import BuildConfig, BuildConfigCode, CODE_REPO_WITHOUT_WEBHOOK
from private_build_configs.models import (AUTO_TAG_TYPES, AUTO_TAG_TYPE_NONE,
                                          CODE_REPO_CLIENT, CODE_REPO_TYPE, INTEGRATION_TYPE)
from mekansm.exceptions import MekAPIException


LOG = logging.getLogger(__name__)

STATUS_BLOCKED = 'B'
STATUS_DELETED = 'D'
STATUS_IN_PROGRESS = 'I'
STATUS_FAILED = 'F'
STATUS_SUCCEEDED = 'S'
STATUS_WAITING = 'W'
STATUS_WEIGHTS = {
    STATUS_BLOCKED: 10,
    STATUS_WAITING: 20,
    STATUS_IN_PROGRESS: 30,
    STATUS_SUCCEEDED: 40,
    STATUS_FAILED: 40,
    STATUS_DELETED: 50
}

STATUS = (
    (STATUS_BLOCKED, STATUS_BLOCKED),
    (STATUS_DELETED, STATUS_DELETED),
    (STATUS_IN_PROGRESS, STATUS_IN_PROGRESS),
    (STATUS_FAILED, STATUS_FAILED),
    (STATUS_SUCCEEDED, STATUS_SUCCEEDED),
    (STATUS_WAITING, STATUS_WAITING)
)

COMPLETE_STATUS = [STATUS_DELETED, STATUS_FAILED, STATUS_SUCCEEDED]

STEP_NONE = 1
STEP_PREPARE = 10
STEP_PULL_CODE = 20
STEP_CREATE_IMAGE = 30
STEP_PUSH_IMAGE = 40
STEP_COMPLETE = 50

STEP = (
    (STEP_NONE, STEP_NONE),
    (STEP_PREPARE, STEP_PREPARE),
    (STEP_PULL_CODE, STEP_PULL_CODE),
    (STEP_CREATE_IMAGE, STEP_CREATE_IMAGE),
    (STEP_PUSH_IMAGE, STEP_PUSH_IMAGE),
    (STEP_COMPLETE, STEP_COMPLETE)
)

STEP_NEW_RUN_SONARSCANNER = 2254


@python_2_unicode_compatible
class Build(models.Model):
    build_id = models.CharField(max_length=36, primary_key=True, default=uuid.uuid4, editable=False)
    config = models.ForeignKey(BuildConfig, on_delete=models.CASCADE)
    image_id = models.CharField(max_length=128, null=True, blank=True)
    image_repo_id = models.CharField(max_length=36, null=True, blank=True, default='')
    registry_index = models.CharField(max_length=128, null=True, blank=True, default='')
    docker_repo_path = models.CharField(max_length=128, null=True, blank=True, default='')
    docker_repo_tag = models.CharField(max_length=32, null=True, blank=True, default='')
    auto_tag_type = models.CharField(max_length=16,
                                     blank=True, null=True,
                                     choices=AUTO_TAG_TYPES,
                                     default=AUTO_TAG_TYPE_NONE)
    customize_tag_rule = models.CharField(max_length=255, default='', blank=True, null=True)
    docker_repo_version_tag = models.CharField(max_length=128, null=True, blank=True)
    docker_repo_label_tag = models.CharField(max_length=128, default='', blank=True)
    status = models.CharField(max_length=8, choices=STATUS)
    step_at = models.IntegerField(choices=STEP, default=STEP_NONE)
    notification_id = models.CharField(max_length=36, null=True, blank=True)
    notification_name = models.CharField(max_length=128, null=True, blank=True)
    build_cache_enabled = models.BooleanField(default=False)
    ci_enabled = models.BooleanField(default=False)
    build_image_enabled = models.BooleanField(default=True)
    build_enabled = models.BooleanField(default=True)
    artifact_upload_enabled = models.BooleanField(default=False)
    allow_artifact_upload_fail = models.BooleanField(default=False)
    is_auto_created = models.BooleanField(default=False)
    is_scheduled = models.BooleanField(default=False)
    ci_envs = JSONField(blank=True, default={})
    ci_image_name = models.CharField(max_length=512, blank=True, default='')
    ci_image_tag = models.CharField(max_length=128, blank=True, default='')
    ci_config_file_location = models.CharField(max_length=255, blank=True)
    ci_steps = JSONField(blank=True, default=[])
    build_filters = JSONField(blank=True, default={})
    dockerfile_content = models.TextField(null=True, blank=True)
    endpoint_id = models.CharField(max_length=64, default='', blank=True)
    created_by = models.CharField(max_length=30)
    user_token = models.CharField(max_length=100)
    namespace = models.CharField(max_length=30)
    cpu = models.FloatField(default=0.5)
    memory = models.IntegerField(default=512)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)
    started_at = models.DateTimeField(null=True, blank=True)
    ended_at = models.DateTimeField(null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True)
    status_sync_schedule_id = models.CharField(max_length=36, default='', blank=True, null=True)
    status_sync_fail_count = models.SmallIntegerField(default=0, null=True)
    extra_dockerfile_enabled = models.BooleanField(default=False)
    ci_report = models.TextField(null=True)

    class Meta:
        ordering = ('-created_at',)

    def __str__(self):
        return self.build_id

    def save(self, *args, **kwargs):
        super(Build, self).save(*args, **kwargs)
        # only update cache after build succeed
        if Build.is_succeeded_status(self.status):
            if self.build_image_enabled:
                for tag_name in (self.docker_repo_version_tag, self.docker_repo_tag, self.docker_repo_label_tag):
                    if tag_name:
                        build_image_tag, created = BuildImageTag.objects.get_or_create(
                            image_repo_id=self.image_repo_id,
                            tag_name=tag_name)
                        build_image_tag.build_id = self.build_id
                        build_image_tag.namespace = self.namespace
                        build_image_tag.user_token = self.user_token
                        build_image_tag.artifact_upload_enabled = self.is_artifact_enabled
                        build_image_tag.allow_artifact_upload_fail = self.allow_artifact_upload_fail
                        build_image_tag.save()
                        AlaudaCache.save_image_tag(build_image_tag, self.user_token)

    def retrieve_artifacts(self, token):
        if self.is_artifact_enabled:
            return get_artifacts(self.namespace, self.build_id, token)
        return []

    @property
    def is_artifact_enabled(self):
        return self.ci_enabled and self.artifact_upload_enabled

    @property
    def exec_cmd_enabled(self):
        return self.ci_enabled and len(self.ci_steps) > 0

    @staticmethod
    def is_succeeded_status(build_status):
        return build_status == STATUS_SUCCEEDED

    @staticmethod
    def is_in_progress_status(build_status):
        return build_status == STATUS_IN_PROGRESS

    @staticmethod
    def is_waiting_status(build_status):
        return build_status == STATUS_WAITING

    @staticmethod
    def is_blocked_status(build_status):
        return build_status == STATUS_BLOCKED

    @staticmethod
    def can_removed(build_status, step_at):
        return STATUS_WEIGHTS[build_status] <= STATUS_WEIGHTS[STATUS_IN_PROGRESS]

    @staticmethod
    def need_call_gundar_delete(build_status):
        return build_status == STATUS_IN_PROGRESS or build_status == STATUS_WAITING


@python_2_unicode_compatible
class BuildCode(models.Model):
    code_id = models.CharField(max_length=36)
    build = models.OneToOneField(Build, on_delete=models.CASCADE,
                                 related_name='code_repo')
    code_repo_client = models.CharField(max_length=20, choices=CODE_REPO_CLIENT)
    code_repo_path = models.CharField(max_length=255)
    code_repo_username = EncryptedCharField(max_length=120, blank=True)
    code_repo_password = EncryptedCharField(max_length=128, blank=True)
    code_repo_type = models.CharField(max_length=16, choices=CODE_REPO_TYPE)
    code_repo_type_value = models.CharField(max_length=64)
    code_head_commit = models.CharField(max_length=128, null=True, blank=True)
    dockerfile_location = models.CharField(max_length=128)
    build_context_path = models.CharField(max_length=128)
    code_repo_key_id = models.IntegerField(null=True, blank=True)
    code_repo_private_key = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-created_at',)

    def __str__(self):
        return self.code_id

    def save(self, *args, **kwargs):
        super(BuildCode, self).save(*args, **kwargs)
        if self.code_repo_client in CODE_REPO_WITHOUT_WEBHOOK and self.code_head_commit:
            build_config_obj = BuildConfigCode.objects.get(code_id=self.code_id)
            build_config_obj.set_head_commit_id(self.code_repo_type_value, self.code_head_commit)
            build_config_obj.save()


PUBLIC_ENDPOINT_NAME = settings.PUBLIC_ENDPOINT_NAME


@python_2_unicode_compatible
class BuildEndpoint(models.Model):
    endpoint_id = models.CharField(
        max_length=36, primary_key=True, default=uuid.uuid4, editable=False)
    namespace = models.CharField(max_length=32)
    endpoint = models.CharField(max_length=128)
    builder_image = models.CharField(max_length=128)
    region_id = models.CharField(max_length=64, blank=True)
    es_host = models.CharField(max_length=255, blank=True)
    es_username = models.CharField(max_length=128, blank=True)
    es_password = models.CharField(max_length=128, blank=True)
    proxy_config = JSONField(blank=True, default={})
    is_public = models.BooleanField(default=False, blank=True)

    class Meta:
        ordering = ('is_public',)

    def __str__(self):
        return self.endpoint_id


@python_2_unicode_compatible
class BuildImageTag(models.Model):
    image_repo_id = models.CharField(max_length=36)
    tag_name = models.CharField(max_length=128)
    namespace = models.CharField(max_length=30, blank=True, null=True)
    user_token = models.CharField(max_length=100, blank=True, null=True)
    build_id = models.CharField(max_length=36, blank=True, null=True)
    artifact_upload_enabled = models.BooleanField(default=False)
    allow_artifact_upload_fail = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.tag_name

    class Meta:
        ordering = ('-updated_at',)
        unique_together = ("image_repo_id", "tag_name")

    def retrieve_artifacts(self, token):
        if self.artifact_upload_enabled:
            return get_artifacts(self.namespace, self.build_id, token)
        return []

def get_artifacts(namespace, build_id, token):
    artifacts = []
    try:
        oss_client = OSS(settings.OSS_ENDPOINT, token)
        artifacts = oss_client.list_objects(namespace, prefix='build/{}/'.format(build_id))
    except Exception as ex:
        LOG.error("Error happens when get artifacts from wukong: {}".
                  format(ex.message))
    finally:
        return artifacts


def delete_artifacts(namespace, build_id, token):
    try:
        oss_client = OSS(settings.OSS_ENDPOINT, token)
        oss_client.delete_objects(namespace, prefix='build/{}/'.format(build_id))
    except Exception as ex:
        LOG.error("Error happens when delete artifacts from wukong: {}".
                  format(ex.message))
        raise

SONARQUBE_STEP_PREPARE = 'PREPARE'
SONARQUBE_STEP_SET = 'SET-SONAR'
SONARQUBE_STEP_SCAN = 'SCAN'
SONARQUBE_STEP_COLLECT = 'COLLECT-DATA'
SONARQUBE_STEP_ANALYSIS = 'ANALYSIS'
SONARQUBE_STEP_COMPLETE = 'COMPLETE'
SONARQUBE_STEP_VALUE = {
    SONARQUBE_STEP_PREPARE: 10,
    SONARQUBE_STEP_SET: 20,
    SONARQUBE_STEP_SCAN: 30,
    SONARQUBE_STEP_COLLECT: 40,
    SONARQUBE_STEP_ANALYSIS: 50,
    SONARQUBE_STEP_COMPLETE: 60
}

@python_2_unicode_compatible
class BuildIntegration(models.Model):
    id  = models.CharField(
        db_column="id", max_length=36, primary_key=True, default=uuid.uuid4, editable=False)
    build_id = models.ForeignKey(Build, on_delete=models.CASCADE, db_column="build_id")
    type = models.CharField(max_length=32, null=False, blank=False, choices=INTEGRATION_TYPE)
    build_integration_config = JSONField(blank=True, default={})
    integration_instance_id = models.CharField(max_length=36, null=False, blank=False)
    integration_execution = JSONField(blank=True, default={})
    execution_status = models.CharField(max_length=8, blank=True, null=True, choices=STATUS)
    step_at = models.CharField(max_length=32, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # will set value when rattletrap calling integration
    started_at = models.DateTimeField(null=True, blank=True)
    ended_at = models.DateTimeField(null=True, blank=True)
    status_sonar_sync_fail_count = models.SmallIntegerField(default=0, null=True)
    sonar_status_sync_schedule_id = models.CharField(max_length=36, default='', blank=True, null=True)

    class Meta:
        ordering = ('-updated_at',)

    def __str__(self):
        return self.id

    @staticmethod
    def can_removed(execution_status, step_at):
        return STATUS_WEIGHTS[execution_status] <= STATUS_WEIGHTS[STATUS_IN_PROGRESS]
