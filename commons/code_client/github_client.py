import logging
import json

from django.conf import settings

from mekansm.request import MekRequest
from mekansm.exceptions import MekServiceException

from commons.code_client.code_client_base import CodeClientBase
from commons.code_client.web_hook_data import WebHookData
from commons.docker_build_exception import DockerBuildException
from private_code_clients.models import CodeClientUser
from private_code_clients.serializers import CodeClientUserSerializer
from private_build_configs.models import CODE_REPO_TYPE_BRANCH, BuildConfigCode


LOG = logging.getLogger(__name__)


class GitHubClient(CodeClientBase):
    CONFIG = {
        'github_auth_uri': 'https://github.com/login/oauth',
        'github_api_base_uri': 'https://api.github.com',
        'github_base_uri': 'https://github.com',
        'client_id': settings.GITHUB['client_id'],
        'client_secret': settings.GITHUB['client_secret'],
        'scope': 'repo'
    }

    def __init__(self):
        super(GitHubClient, self).__init__()

    def load(self, namespace):
        try:
            code_client_user = CodeClientUser.objects.get(code_repo_client='GITHUB',
                                                          user_name=namespace)
        except CodeClientUser.DoesNotExist:
            LOG.warning('load client user {}/{} failed, '
                        'not found in db'.format('GITHUB', namespace))
            return False
        self.code_client_user = CodeClientUserSerializer(code_client_user).data
        return True

    def parse_web_hook_payload(self, post_data):
        payload = {}
        if(post_data.get('payload')):
            # application/x-www-form-urlencoded
            payload = json.loads(post_data['payload'])
        else:
            # content-type: application/json
            payload = post_data

        code_repo_type = CODE_REPO_TYPE_BRANCH
        code_head_commit = payload['head_commit']['id']
        code_repo_type_value = self._get_branch(payload['ref'])
        web_hook_data = WebHookData(code_repo_type,
                                    code_repo_type_value,
                                    code_head_commit)

        return web_hook_data

    def parse_web_hook_post_data(self, post_data, code_repo):
        web_hook_data = self.parse_web_hook_payload(post_data)
        if not web_hook_data.code_repo_type == code_repo['code_repo_type']:
            raise DockerBuildException('invalid_args', "{}!={}".format(
                    web_hook_data.code_repo_type, code_repo['code_repo_type']))
        valid, _1, err = BuildConfigCode.valid_runtime_code_repo_type_value(
            code_repo['code_repo_type_value'], web_hook_data.code_repo_type_value)
        if not valid:
            LOG.error('invalid_args {}'.format(err))
            raise DockerBuildException('invalid_args', err)

        return web_hook_data

    def _get_branch(self, ref):
        ref_prefix = 'refs/heads/'
        branch = None
        if ref_prefix in ref:
            branch = ref.replace(ref_prefix, '')
        return branch

    def get_auth_url(self, state):
        auth_url = '{}/authorize?client_id={}&scope={}&state={}'.format(
            GitHubClient.CONFIG['github_auth_uri'], GitHubClient.CONFIG['client_id'],
            GitHubClient.CONFIG['scope'], state
        )
        return auth_url

    def get_access_token(self, request_data):
        if self.code_client_user.get('access_token'):
            return self.code_client_user['access_token']
        client_user_id = request_data.get('state')
        provider_code = request_data.get('code')
        if not client_user_id or not provider_code:
            raise DockerBuildException('wrong_auth_callback_data')
        data = {
            'client_id': self.CONFIG['client_id'],
            'client_secret': self.CONFIG['client_secret'],
            'code': provider_code
        }
        headers = self._get_headers()
        access_token_result = GitHubAuthRequest.send(
            'access_token', method='POST', data=data, headers=headers)['data']
        access_token = access_token_result.get('access_token')
        return access_token, None

    def get_client_login(self, user_name):
        headers = self._get_headers()
        user_result = GitHubAPIRequest.send_request(
            'user', method='GET', headers=headers,
            user_name=user_name)['data']
        return user_result['login']

    def get_client_orgs(self, *args, **kwargs):
        headers = self._get_headers()
        org_list_result = GitHubAPIRequest.send_request(
            'user/orgs', method='GET', headers=headers,
            user_name=self.code_client_user['user_name'])['data']
        org_list = [
            {'name': org['login']} for org in org_list_result
        ]
        org_list.insert(
            0, {'name': self.code_client_user['code_repo_client_user_name']})
        return org_list

    def get_repo_list(self, code_client_org, *args, **kwargs):
        headers = self._get_headers()
        if code_client_org == self.code_client_user['code_repo_client_user_name']:
            repo_list_result = GitHubAPIRequest.send_request(
                'users/{}/repos'.format(code_client_org),
                method='GET', headers=headers,
                user_name=self.code_client_user['user_name'])['data']
        else:
            repo_list_result = GitHubAPIRequest.send_request(
                'orgs/{}/repos'.format(code_client_org),
                method='GET', headers=headers,
                user_name=self.code_client_user['user_name'])['data']
        repo_list = [
            {'code_repo_path': repo.get('name'), 'repo_id': ''} for repo in repo_list_result
        ]
        return repo_list

    def create_deploy_key(self, code_repo_path, repo_id, description=None):
        deploy_key = super(GitHubClient, self).create_deploy_key(code_repo_path, repo_id, description)

        data = {
            'title': deploy_key['description'],
            'key': deploy_key['public_key']
        }

        headers = self._get_headers()
        result = GitHubAPIRequest.send_request(
            'repos/{}/keys'.format(code_repo_path),
            method='POST', headers=headers, data=data,
            user_name=self.code_client_user['user_name'])['data']

        if not result.get('verified'):
            raise DockerBuildException('failed_verify_deploy_keys')

        deploy_key['id'] = result.get('id')
        return deploy_key

    def delete_deploy_key(self, code_repo_path, id, repo_id):
        headers = self._get_headers()
        result = GitHubAPIRequest.send_request(
            'repos/{}/keys/{}'.format(code_repo_path, id),
            method='DELETE', headers=headers,
            user_name=self.code_client_user['user_name'])
        return result

    def create_webhook(self, code_repo_path, namespace, repo_id):
        webhook = super(GitHubClient, self).create_webhook(code_repo_path, namespace, repo_id)
        headers = self._get_headers()

        payload = {
            "name": "web",
            "active": True,
            "events": ["push"],
            "config": {
                "content_type": "application/json",
                "secret": webhook['password'],
                "url": webhook['url']
            }
        }

        result = GitHubAPIRequest.send_request(
            'repos/{}/hooks'.format(code_repo_path),
            method='POST', headers=headers, data=payload,
            user_name=self.code_client_user['user_name'])['data']

        webhook['id'] = result.get('id')
        return webhook

    def delete_webhook(self, code_repo_path, id, repo_id):
        headers = self._get_headers()

        result = GitHubAPIRequest.send_request(
            'repos/{}/hooks/{}'.format(code_repo_path, id),
            method='DELETE', headers=headers,
            user_name=self.code_client_user['user_name'])
        return result
    
    def create_repo_webhook(self, code_repo_path, hook_url):
        payload = {
            "name": "web",
            "active": True,
            "events": ["push"],
            "config": {
                "content_type": "json",
                "url": hook_url
            }
        }
        headers = self._get_headers()

        return GitHubAPIRequest.send_request(
            'repos/{}/hooks'.format(code_repo_path),
            method='POST', headers=headers, data=payload,
            user_name=self.code_client_user['user_name'])['data']

    def delete_repo_webhook(self, code_repo_path, hook_id):
        headers = self._get_headers()
        return GitHubAPIRequest.send_request(
            'repos/{}/hooks/{}'.format(code_repo_path, hook_id),
            method='DELETE', headers=headers,
            user_name=self.code_client_user['user_name'])

    def get_full_path(self, code_repo_path, username, password):
        return 'git@github.com:{}.git'.format(code_repo_path)

    def _get_headers(self):
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'User-Agent': 'alauda'
        }
        if self.code_client_user.get('access_token'):
            headers['Authorization'] = 'token ' + self.code_client_user['access_token']
        return headers


class GitHubAPIRequest(MekRequest):
    endpoint = GitHubClient.CONFIG['github_api_base_uri']

    @classmethod
    def send_request(cls, url, method='GET', headers=None, data=None, user_name=None):
        try:
            return GitHubAPIRequest.send(url, method=method, headers=headers, data=data)
        except MekServiceException, ex:
            if ex.status_code == 401:
                # delete invalid token
                code_client_db_result = CodeClientUser.objects.all().filter(
                    code_repo_client='GITHUB', user_name=user_name).get()
                code_client_db_result.delete()
                raise DockerBuildException("token_expired")
            else:
                raise DockerBuildException(
                    'git_hub_error', "github error code={}, message={}".format(
                        ex.code, ex.message))


class GitHubAuthRequest(MekRequest):
    endpoint = GitHubClient.CONFIG['github_auth_uri']
