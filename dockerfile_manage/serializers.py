from rest_framework import serializers

from dockerfile_manage.models import Dockerfile


class DockerfileSerializer(serializers.ModelSerializer):
    build_steps = serializers.JSONField(required=True)

    class Meta:
        model = Dockerfile


class DockerfileExcludeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dockerfile
        exclude = ('build_steps', 'created_at', 'updated_at')


class DockerfileUpdateSerializer(serializers.ModelSerializer):
    build_steps = serializers.JSONField(required=False)

    class Meta:
        model = Dockerfile
        read_only_fields = ('uuid', 'name', 'created_at', 'updated_at')
