from django.conf.urls import include, url
from django.http import HttpResponse

from ci_envs.views import CIEnvsViewSet
from diagnose.views import DiagnoseViewset
from private_build_configs.views import PrivateBuildConfigsViewset
from private_builds.views import PrivateBuildsViewset

urlpatterns = [
    url(r'^ping/?$', lambda request: HttpResponse('razzil:alive')),
    url(r'^_ping/?$', lambda request: HttpResponse('razzil:alive')),
    url(r'^_auth_ping/?$', lambda request: HttpResponse('razzil:alive')),
    url(r'^_diagnose/?$', DiagnoseViewset.as_view({'get': 'retrieve'})),
    url('^v1/tag_artifacts/', PrivateBuildsViewset.as_view(
        {'get': 'tag_artifacts'}
    )),
    url('^v1/tag_artifacts_count/', PrivateBuildsViewset.as_view(
        {'get': 'tag_artifacts_count'}
    )),
    url('^v1/copy_image_tag_info/?$', PrivateBuildsViewset.as_view(
        {'post': 'copy_image_tag_info'}
    )),
    url('^v1/private_builds/', include('private_builds.urls')),
    url('^v1/private_build_configs/', include('private_build_configs.urls')),
    url('^v1/private_code_clients/', include('private_code_clients.urls')),
    url('^v1/private_build_endpoints/', include('private_builds.endpoint_urls')),
    url('^v1/ci-envs-catalog/?$', CIEnvsViewSet.as_view({'get': 'list_ci_envs'})),
    url('^v1/parse_auto_build/?$', PrivateBuildConfigsViewset.as_view(
        {'post': 'parse_auto_build'})),
    url('^v1/private_build_integrations/', include('private_builds.build_integration_urls')),
    url('^v1/dockerfile/', include('dockerfile_manage.urls'))
]
