from rest_framework import viewsets
from rest_framework.response import Response

from .models import CIEnvsCatalog
from .serializers import CIEnvsCatalogSerializer
from alauda.tracing import tracing_response_time


class CIEnvsViewSet(viewsets.ViewSet):

    @tracing_response_time(module='ci_envs_catalog')
    def list_ci_envs(self, request):
        ci_envs = CIEnvsCatalog.objects.all()
        serializer = CIEnvsCatalogSerializer(ci_envs, many=True)
        return Response(serializer.data)
