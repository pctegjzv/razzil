from django.conf.urls import url

from commons.constants import APPLICATION_TEMPLATE_URL_REGEX

from private_code_clients.views import PrivateBuildCodesViewset


urlpatterns = [

    url(r'^parse_webhook_payload/?$',
        PrivateBuildCodesViewset.as_view(
            {
                'post': 'parse_webhook_payload'
            }
        )),

    # get the list of code client
    # add the code client
    url(r'^(?P<user_name>{})/?$'.format(APPLICATION_TEMPLATE_URL_REGEX),
        PrivateBuildCodesViewset.as_view(
            {
                'get': 'list_supported_code_client',
                'post': 'add_code_client'
            }
        )),
    # get method is used for get the code client detail. the return info will be displayed
    # in the first page of private build config
    # delete method is used for delete the code client,
    # after it the user can binding anothoer account
    url(r'^(?P<user_name>{})/(?P<code_client_name>{})/?$'.format(
        APPLICATION_TEMPLATE_URL_REGEX, APPLICATION_TEMPLATE_URL_REGEX),
        PrivateBuildCodesViewset.as_view(
            {
                'get': 'retrieve_code_client',
                'delete': 'delete_code_client'
            }
        )),
    # generate the auth url
    url(r'^(?P<user_name>{})/(?P<code_client_name>{})/auth-url/?$'
        .format(APPLICATION_TEMPLATE_URL_REGEX, APPLICATION_TEMPLATE_URL_REGEX),
        PrivateBuildCodesViewset.as_view(
            {
                'post': 'retrieve_auth_url'
            }
        )),
    # get the repo list for the client, for different code client,
    # may passed in different argument
    url(r'^(?P<user_name>{})/(?P<code_client_name>{})/orgs/(?P<code_client_org>{})/repos/?$'
        .format(APPLICATION_TEMPLATE_URL_REGEX, APPLICATION_TEMPLATE_URL_REGEX,
                APPLICATION_TEMPLATE_URL_REGEX),
        PrivateBuildCodesViewset.as_view(
            {
                'get': 'list_code_repo'
            }
        )),
    url(r'^(?P<user_name>{})/(?P<code_client_name>{})/(?P<code_client_org>{})/(?P<repo>{})/webhook/?$'
        .format(APPLICATION_TEMPLATE_URL_REGEX, APPLICATION_TEMPLATE_URL_REGEX,
                APPLICATION_TEMPLATE_URL_REGEX, APPLICATION_TEMPLATE_URL_REGEX),
        PrivateBuildCodesViewset.as_view(
            {
                'post': 'create_repo_webhook',
                'delete': 'delete_repo_webhook'
            }
        )),
    url(r'^(?P<user_name>{})/(?P<code_client_name>{})/(?P<code_client_org>{})/(?P<repo>{})/deploy_key/?$'
        .format(APPLICATION_TEMPLATE_URL_REGEX, APPLICATION_TEMPLATE_URL_REGEX,
                APPLICATION_TEMPLATE_URL_REGEX, APPLICATION_TEMPLATE_URL_REGEX),
        PrivateBuildCodesViewset.as_view(
            {
                'post': 'create_repo_deploy_key'
            }
        )),
    url(r'^(?P<user_name>{})/(?P<code_client_name>{})/(?P<code_client_org>{})/(?P<repo>{})/deploy_key/(?P<key_id>{})/?$'
        .format(APPLICATION_TEMPLATE_URL_REGEX, APPLICATION_TEMPLATE_URL_REGEX,
                APPLICATION_TEMPLATE_URL_REGEX, APPLICATION_TEMPLATE_URL_REGEX, APPLICATION_TEMPLATE_URL_REGEX),
        PrivateBuildCodesViewset.as_view(
            {
                'delete': 'delete_repo_deploy_key'
            }
        )),
]
