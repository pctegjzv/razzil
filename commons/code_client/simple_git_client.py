import logging
import json
import re

from commons.code_client.code_client_base import CodeClientBase
from commons.code_client.web_hook_data import WebHookData
from commons.docker_build_exception import DockerBuildException
from private_build_configs.models import CODE_REPO_TYPE_BRANCH, BuildConfigCode


LOG = logging.getLogger(__name__)


class SimpleGitClient(CodeClientBase):
    def __init__(self):
        super(SimpleGitClient, self).__init__()

    def parse_branch_name(self, post_data):
        if post_data and 'ref' in post_data:
            return self._get_branch(post_data.get("ref"))
    
    def parse_web_hook_payload(self, payload):
        code_repo_type = CODE_REPO_TYPE_BRANCH

        # parse simple git
        branch_name = payload.get('branch_name')
        if branch_name:
            code_head_commit = payload.get('commit_id')
        else:
            # parse gitlab
            branch_name = self.parse_branch_name(payload)
            code_head_commit = payload.get('checkout_sha')

            if not code_head_commit:
                # parse gogs
                code_head_commit = payload.get('after')

            if not branch_name or not code_head_commit:
                # parse oschina
                branch_name, code_head_commit = self._get_osachina_push_info(payload)

        web_hook_data = WebHookData(code_repo_type,
                                    branch_name,
                                    code_head_commit)
        return web_hook_data

    def parse_web_hook_post_data(self, post_data, code_repo):
        web_hook_data = self.parse_web_hook_payload(post_data)
        if not web_hook_data.code_repo_type == code_repo['code_repo_type']:
            raise DockerBuildException('invalid_args', "{}!={}".format(
                    web_hook_data.code_repo_type, code_repo['code_repo_type']))
        valid, _1, err = BuildConfigCode.valid_runtime_code_repo_type_value(
            code_repo['code_repo_type_value'], web_hook_data.code_repo_type_value)
        if not valid:
            LOG.info('private build webhook post_data:{}'.format(post_data))
            LOG.error('invalid_args {} , {}'.format("code repo error", err))
            raise DockerBuildException('invalid_args', err)

        return web_hook_data

    def _get_osachina_push_info(self, post_data):
        # parse oschina
        code_repo_type_value = None
        code_head_commit = None
        if post_data.get('after') and post_data.get('ref'):
            code_head_commit = post_data.get('after')
            code_repo_type_value = self._get_branch(post_data.get('ref'))
        elif post_data.get('hook'):
            hook = json.loads(post_data.get('hook'))
            if hook.get('hook_name') == 'push_hooks':
                push_data = hook.get('push_data')
                code_repo_type_value = self._get_branch(push_data.get('ref'))
                code_head_commit = push_data.get('after')

        return code_repo_type_value, code_head_commit

    def _get_branch(self, ref):
        if not ref:
            return None
        ref_prefix = 'refs/heads/'
        branch = None
        if ref_prefix in ref:
            branch = ref.replace(ref_prefix, '')
        return branch

    def create_deploy_key(self, code_repo_path, repo_id, description=None):
        if (code_repo_path.lower().startswith('ssh') or
                code_repo_path.lower().startswith('git')):
            pattern = re.compile(r'^git@(\w+)\..+')
            match = pattern.match(code_repo_path)
            if match and match.groups()[0] == 'bitbucket':
                self.bits = 2048
            return super(SimpleGitClient, self).create_deploy_key(code_repo_path, repo_id, description)
        else:
            return {
                'id': None,
                'private_key': None,
                'public_key': None
            }
