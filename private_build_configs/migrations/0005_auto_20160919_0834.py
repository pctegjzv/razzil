# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-19 08:34
from __future__ import unicode_literals

from django.db import migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('private_build_configs', '0004_auto_20160909_0352'),
    ]

    operations = [
        migrations.AlterField(
            model_name='buildconfig',
            name='ci_envs',
            field=jsonfield.fields.JSONField(blank=True, default={}),
        ),
    ]
