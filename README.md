
# Razzil

## generate migrations
Razzil is based on **Python2**, and we use virtualenv to manage python envs
1. create python2 virtualenv
```
mkdir -p ~/.virtualenv/razzil
virtualenv --python path/to/python2 ~/.virtualenv/razzil
source ~/.virtualenv/razzil/bin/activate
```
2. install packages
```
pip install -r requirements.txt
pip install --trusted-host pypi.alauda.io \
                   --extra-index-url http://mathildetech:Mathilde1861@pypi.alauda.io/simple/ \
                   -r requirements-alauda.txt
```
3. makemigrations
```
python manage.py makemigrations --settings config.settings_dev
```
4. add migrate files to git
```
git add
git commit
```

## run unittest
```
docker-compose -f docker-compose.yml -f docker-compose.test.yml run --rm web
```