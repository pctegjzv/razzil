from __future__ import unicode_literals

import uuid

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class CodeClientUser(models.Model):
    id = models.CharField(max_length=36, primary_key=True, default=uuid.uuid4, editable=False)
    access_token = models.CharField(max_length=128, null=True, blank=True)
    refresh_token = models.CharField(max_length=128, blank=True)
    access_token_secret = models.CharField(max_length=64, null=True, blank=True)
    code_repo_client_user_name = models.CharField(max_length=64, null=True, blank=True)
    code_repo_client = models.CharField(max_length=16)
    user_name = models.CharField(max_length=64)
    request_token_secret = models.CharField(max_length=32, null=True, blank=True)
    code_repo_client_user_id = models.CharField(max_length=64, null=True, blank=True)

    class Meta:
        ordering = ('code_repo_client',)

    def __str__(self):
        return self.id
