import logging
import os
import requests
import time

from django.conf import settings
from rest_framework import viewsets
from rest_framework.response import Response

from private_builds.log_client.log_client import ElasticSearchClient
from private_build_configs.cache import AlaudaCache
from private_build_configs.models import BuildConfig

LOG = logging.getLogger(__name__)
status_ok = 'OK'
status_error = 'ERROR'


class DiagnoseViewset(viewsets.ViewSet):

    def retrieve(self, request):
        self.init_result()
        self.check_env_var()
        self.check_redis()
        self.check_db()
        self.check_jakiro()
        self.check_lucifer()
        self.check_furion()
        self.check_medusa()
        self.check_tresdin()
        self.check_es()
        self.check_oss()
        self.check_github()
        self.check_bitbucket()
        self.check_oschina()

        for detail in self.diagonse_result['details']:
            if detail.get('status') == status_error:
                self.diagonse_result['status'] = status_error
                break
        return Response(self.diagonse_result)

    def init_result(self):
        self.diagonse_result = {}
        self.diagonse_result['status'] = status_ok
        self.diagonse_result['details'] = []

    def check_env_var(self):
        start = self._current()
        env_var_list = ['SOURCE', 'DOCKER_BUILD_TIMEOUT',
                        'COMP_NAME', 'TRACE_ENABLED', 'UWSGI_PROCESS']
        status = status_ok
        message = ''
        for env_var in env_var_list:
            if not os.getenv(env_var):
                status = status_error
                message += '{} not exist,'.format(env_var)
        attr_list = [
            'JAKIRO_API_VERSION', 'LUCIFER_API_VERSION',
            'MEDUSA_API_VERSION']
        for attr in attr_list:
            if not getattr(settings, attr, None):
                status = status_error
                message += '{} not exist,'.format(attr)
        if not settings.TRESDIN_CLIENT.get('api_version'):
            status = status_error
            message += '{} not exist,'.format('TRESDIN_API_VERSION')
        latency = self._current() - start
        self.add_diagnose_detail(status, 'env_var', message=message, latency=latency)

    def check_redis(self):
        start = self._current()
        status = status_ok
        message = ''
        try:
            AlaudaCache.redis_client.set("razzil_diagnose_test", 1)
            AlaudaCache.redis_client.delete("razzil_diagnose_test")
        except:
            status = status_error
            message = 'call redis failed :{}'
        latency = self._current() - start
        self.add_diagnose_detail(status, 'redis', message=message, latency=latency)

    def check_db(self):
        start = self._current()
        status = status_ok
        message = ''
        try:
            BuildConfig.objects.all().count()
        except:
            status = status_error
            message = 'query in db failed'
        latency = self._current() - start
        self.add_diagnose_detail(status, 'database', message=message, latency=latency)

    def check_jakiro(self):
        endpoint = settings.JAKIRO_API_ENDPOINT
        self._check_our_service(endpoint, 'jakiro')

    def check_lucifer(self):
        endpoint = settings.LUCIFER_API_ENDPOINT
        self._check_our_service(endpoint, 'lucifer')

    def check_furion(self):
        endpoint = settings.REGION_MANAGER['endpoint']
        self._check_our_service(endpoint, 'furion')

    def check_medusa(self):
        endpoint = settings.MEDUSA_ENDPOINT
        self._check_our_service(endpoint, 'medusa')

    def check_oss(self):
        pass

    def check_tresdin(self):
        endpoint = settings.TRESDIN_CLIENT.get('endpoint')
        self._check_our_service(endpoint, 'tresdin')

    def check_github(self):
        pass

    def check_bitbucket(self):
        pass

    def check_oschina(self):
        pass

    def check_es(self):
        start = self._current()
        status = status_ok
        message = ''
        try:
            es = ElasticSearchClient()
            if not es.conn:
                raise Exception('connect es failed')
            start_time = time.time()
            end_time = start_time + 60
            es.get_build_log('test_build_id', start_time, end_time)
        except:
            status = status_error
            message = 'connect es failed'
        latency = self._current() - start
        self.add_diagnose_detail(status, 'es', message=message, latency=latency)

    def add_diagnose_detail(self, status, name, message='', suggestion='', latency=''):
        detail = {
            'status': status,
            'name': name,
            'message': message,
            'suggestion': suggestion,
            'latency': '{}ms'.format(latency)
        }
        self.diagonse_result['details'].append(detail)

    def _check_our_service(self, endpoint, name):
        status = status_ok
        message = ''
        latency = 0
        try:
            start = self._current()
            result = requests.get('{}/_ping'.format(endpoint), timeout=1)
            if result.status_code >= 300:
                status = status_error
                message = 'call {} failed'.format(name)
            latency = self._current() - start
        except:
            message = 'call {} failed'.format(name)
            status = status_error
        self.add_diagnose_detail(status, name, message=message, latency=latency)

    def _current(self):
        return int(time.time() * 1000)
