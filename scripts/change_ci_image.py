#!/usr/bin/env python
# -*- codeing: utf-8 -*-
import os
import sys
import logging
import time
import django
from importlib import import_module
from django.db import transaction
import traceback
import argparse

'''
USAGE:
****python change_ci_image.py --source java:openjdk7 --target java:openjdk8

'''

__author__ = 'jtcheng'

FROM_LANG="N/A"
FROM_RUNTIME="N/A"
TO_LANG="N/A"
TO_RUNTIME="N/A"

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BASE_DIR)
sys.path.append("../")
django.setup()

import_module("config")

from private_build_configs.models import BuildConfig

logger = logging.getLogger(__name__)

def migrate():
    offset = 0
    step = 20
    change_count = 0

    try:
        with transaction.atomic():
            while True:
                query_set = BuildConfig.objects.all()[offset:offset+step]
                count = query_set.count()
                if count == 0:
                    break

                logger.info("Migrate: {} - {}".format(offset, offset+count-1))
                for model in query_set:
                    lang = model.ci_envs.get("language")
                    runtimes = model.ci_envs.get("runtimes")
                    if not lang or not runtimes:
                        continue
                    if not runtimes[0].get("name"):
                        continue
                    if lang == FROM_LANG and runtimes[0].get("name") == FROM_RUNTIME:
                        logger.info("{} lang {}->{} runtime_name {}->{}".format(
                            model.config_id, lang, TO_LANG, runtimes[0].get("name"), TO_RUNTIME))
                        model.ci_envs["language"] = TO_LANG
                        model.ci_envs["runtimes"][0]["name"] = TO_RUNTIME
                        model.save()
                        change_count = change_count +1 
                offset = offset + count
            if change_count > 0:
                ok = raw_input("Commit Transaction Y/N ?")
                if ok != "Y":
                    raise Exception("USER_ABORT")
                logging.info("Commiting...")
            else:
                logging.info("No Data need to be changed")
    except Exception as ex:
        if ex.message == "USER_ABORT":
            logging.info("user aborted, no data changed")
        else:
            logging.error("Aborted, got exception")
            logging.error("Exception: {}".format(traceback.format_exc()))
        return False
    return True

def check():
    offset = 0
    step = 50
    unpass_count = 0

    logging.info("Begin check migrating result")
    while True:
        query_set = BuildConfig.objects.all()[offset:offset+step]
        count = query_set.count()
        if count == 0:
            logger.info("Check done: unpass_count={}, {}".format(
                unpass_count,
                "SUCCESS" if unpass_count == 0 else "FAIL"
            ))
            break

        for model in query_set:
            lang = model.ci_envs.get("language")
            runtimes = model.ci_envs.get("runtimes")
            if not lang or not runtimes:
                continue
            if not runtimes[0].get("name"):
                continue
            if lang == FROM_LANG and runtimes[0].get("name") == FROM_RUNTIME:
                unpass_count = unpass_count + 1
                logger.info("{} check unpass".format(model.config_id))
        offset = offset + count + 1
    return unpass_count == 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--source",
                        help="source ci_image, java:jdk7")
    parser.add_argument("-t", "--target",
                        help="target ci_image, java:jdk8")
    args = parser.parse_args()
    if args.source is None or args.target is None \
        or ":" not in args.source or ":" not in args.target:
        logging.warning("--source and --target required or format error. try -h for help")
    else:
        FROM_LANG = args.source.split(":")[0]
        FROM_RUNTIME = args.source.split(":")[1]
        TO_LANG =  args.target.split(":")[0]
        TO_RUNTIME = args.target.split(":")[1]
        logger.info("Begin change ci image name {}-> {}  {} -> {} in build config".format(
            FROM_LANG, TO_LANG, FROM_RUNTIME, TO_RUNTIME
        ))
        if raw_input("Continue Y/N?") == "Y":
            if migrate():
                if check():
                    logger.info("All Data Migrated Success")
                else:
                    logger.info("Migrated OK , but check fail, do it again")
            else:
                logger.error("Migrated Fail")


