from django.conf.urls import url

from commons.constants import UUID_PATTERN, APPLICATION_TEMPLATE_URL_REGEX

from private_build_configs import views


urlpatterns = [
    url(r'^/?$', views.PrivateBuildConfigsViewset.as_view({
        'get': 'list',
        'post': 'create'
    })),
    url(r'^(?P<private_build_config_id>{})/?$'.format(UUID_PATTERN),
        views.PrivateBuildConfigsViewset.as_view({
            'get': 'retrieve',
            'put': 'update',
            'delete': 'delete'
        })),
    url(r'^(?P<private_build_config_id>{})/capability'.format(UUID_PATTERN),
        views.PrivateBuildConfigsViewset.as_view({
                'get': 'retrieve_config_capability'
        })),
    url(r'^config_list_without_webhook/$', views.PrivateBuildConfigsViewset.as_view({
            'get': 'config_list_without_webhook',
        })),
    url(r'^auto_build/(?P<code_repo_webhook_code>{})/?$'.format(APPLICATION_TEMPLATE_URL_REGEX),
        views.PrivateBuildConfigsViewset.as_view({
            'get': 'retrieve_by_hook_code',
        })),
    url('^alaudaci-yaml/?$',
        views.PrivateBuildConfigsViewset.as_view({
            'post': 'preview_alaudaci_yml',
        }))
]
