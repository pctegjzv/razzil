import datetime
import logging
import time

from elasticsearch import Elasticsearch

from django.conf import settings

from commons.util import get_now_cur_time_zone


LOG = logging.getLogger(__name__)
INSTANCE_SONARQUBE = "sonarqube"


class ElasticSearchClient(object):
    def __init__(self, host=None, username=None, password=None):
        host = host or settings.LOGGING_STORAGE['host']
        username = username or settings.LOGGING_STORAGE['username']
        password = password or settings.LOGGING_STORAGE['password']
        if username and password:
            self.conn = Elasticsearch([host], http_auth=(username, password))
        else:
            self.conn = Elasticsearch([host])

    def save_build_log(self, build_id, message, log_level=0):
        """
        Save build log to elasticsearch.
        this method will refactor when tiny refactored(release-1.11)
        """
        now_time = get_now_cur_time_zone()
        app_id = build_id.replace('-', '_')
        index = 'log' + '-%04d%02d%02d' % (now_time.year,
                                           now_time.month,
                                           now_time.day)
        doc = {
            'time': int(time.time()*1000000),
            'machine': '',
            'app_id': app_id,
            'instance_id': app_id,
            'container_name': '',
            'log_data': message,
            'log_type': 'stdout',
            'log_level': log_level,
            'filename': 'stdout',
            'service_name': '',
            'region_name': '',
            'region_id': '',
            'instance_id8': ''
        }

        return self.save_app_log(index=index, doc_type='log', body=doc)

    def save_app_log(self, **kwargs):
        ret = None
        try:
            ret = self.conn.index(**kwargs)
        except Exception as e:
            LOG.error('save log error, log: {}, exception: {}'.format(kwargs, e.message))
        return ret

    def get_build_log(self, build_id, start_time, end_time, limit=2000):
        build_id = build_id.replace('-', '_')
        return self.get_app_log(build_id, start_time, end_time, limit, build_id)

    def get_sonarqube_build_log(self, build_id, start_time, end_time, limit=2000):
        build_id = build_id.replace('-', '_')
        return self.get_app_log(build_id, start_time, end_time, limit, INSTANCE_SONARQUBE)

    def get_app_log(self, app_id, start_time, end_time, limit=2000, instance_id=None):
        size = limit or 2000
        fields = ['time', 'log_data', 'log_level']
        index_list = self._get_index(start_time, end_time)
        result_list, return_list, result = [], [], ''
        if index_list is None:
            return result, return_list

        for l in index_list:
            start, end, index = l['start'], l['end'], l['index']
            if start == end:
                continue

            must = [
                {
                    'term': {'app_id': app_id}
                },
                {
                    'range': {
                        'time': {'gte': start * 1000000, 'lt': end * 1000000}
                    }
                }
            ]
            if instance_id:
                must.append({'term':{'instance_id':instance_id}})
            body = {
                'sort': {'time': {'order': 'desc'}},
                'fields': fields,
                'query': {'bool': {'must': must}}
            }
            try:
                res = self.conn.search(size=size,
                                       doc_type='log',
                                       index=index,
                                       body=body)
            except Exception as ex:
                LOG.warn('get log fail! error: {}'.format(str(ex)))
                continue
            if res['hits']['total'] == 0:
                continue

            result_list.append(res)
            size = size - res['hits']['total']
            if size <= 0:
                break

        result_list.reverse()
        for res in result_list:
            res['hits']['hits'].reverse()
            for log in res['hits']['hits']:
                log_info = log['fields']
                try:
                    log_level = log_info['log_level'][0]
                except KeyError:
                    log_level = 'INFO'

                result += log_info['log_data'][0] + '\r\n'
                return_list.append({
                    'time': log_info['time'][0] / 1000000,
                    'message': log_info['log_data'][0],
                    'level': log_level
                })

        return result, return_list

    def _get_index(self, start_time, end_time, index_type='log'):
        if start_time >= end_time:
            return None
        result_list = []
        start_datetime = datetime.datetime.fromtimestamp(start_time)
        end_datetime = datetime.datetime.fromtimestamp(end_time)

        index = index_type + '-%04d%02d%02d' % (start_datetime.year,
                                                start_datetime.month,
                                                start_datetime.day)
        start = start_time

        next_day, time_stamp = self._get_next_day(start_datetime)
        next_day = start_datetime

        while next_day.date() < end_datetime.date():
            l = {
                'index': index,
                'start': start,
                'end': time_stamp
            }
            result_list.append(l)
            start = time_stamp
            next_day, time_stamp = self._get_next_day(next_day)
            index = index_type + '-%04d%02d%02d' % (next_day.year,
                                                    next_day.month,
                                                    next_day.day)
            time_stamp += 24 * 3600

        l = {
            'index': index,
            'start': start,
            'end': end_time
        }
        result_list.append(l)
        result_list.reverse()
        return result_list

    def _get_next_day(self, src):
        if not isinstance(src, datetime.date):
            return None
        td = datetime.datetime(year=src.year,
                               month=src.month,
                               day=src.day,
                               hour=23,
                               minute=59,
                               second=59)
        time_stamp = time.mktime(td.timetuple())
        time_stamp += 1

        next_day = datetime.datetime.fromtimestamp(time_stamp)
        return next_day, int(time_stamp)
