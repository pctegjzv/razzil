FROM index.alauda.cn/alaudaorg/alaudabase:ubuntu-14.04-global-1.1
MAINTAINER liaoj jliao@alauda.io

RUN mkdir -p /var/log/mathilde/ \
    && chmod 775 /var/log/mathilde/ \
    && mkdir -p /var/log/uwsgi/ \
    && chmod 755 /var/log/uwsgi/

# nginx config
RUN rm -rf /etc/nginx/conf.d/default.conf && echo "daemon off;" >> /etc/nginx/nginx.conf

WORKDIR /razzil

EXPOSE 80

COPY requirements.txt /
RUN pip install --no-cache-dir -r /requirements.txt

COPY . /razzil
RUN pip install --no-cache-dir --trusted-host pypi.alauda.io --extra-index-url \
    http://mathildetech:Mathilde1861@pypi.alauda.io/simple/ -r /razzil/requirements-alauda.txt

RUN ln -s /razzil/etc/nginx.conf /etc/nginx/conf.d/nginx.conf && \
    ln -s /razzil/etc/supervisord.conf /etc/supervisord.conf && \
    chmod +x /razzil/bootstrap.sh

ENV UWSGI_CHEAPER         10
ENV UWSGI_CHEAPER_INITIAL 10
ENV UWSGI_CHEAPER_STEP    5
ENV UWSGI_RELOAD_ON_AS    200
ENV UWSGI_PROCESS         20

CMD ["/razzil/bootstrap.sh"]
