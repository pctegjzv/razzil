from django.conf.urls import url

from commons.constants import UUID_PATTERN
from private_builds import views


urlpatterns = [
    url(r'^/?$', views.PrivateBuildsViewset.as_view(
        {'get': 'list', 'post': 'create'}
    )),
    url(r'^artifacts/?$', views.PrivateBuildsViewset.as_view(
        {'get': 'list_artefacts'}
    )),
    url(r'^multiple_report/?$', views.PrivateBuildsViewset.as_view(
        {'get': 'multiple_report'}
    )),
    url(r'^dashboard/?$', views.PrivateBuildsViewset.as_view(
        {'get': 'dashboard'}
    )),
    # namespace should be passed by query when retrive
    url(r'^(?P<private_build_id>{})/?$'.format(UUID_PATTERN), views.PrivateBuildsViewset.as_view(
        {'get': 'retrieve', 'put': 'update', 'delete': 'delete'}
    )),
    url(r'^(?P<private_build_id>{})/config/?$'.format(UUID_PATTERN),
        views.PrivateBuildsViewset.as_view(
            {'get': 'retrieve_config'}
        )),
    url(r'^(?P<private_build_id>{})/logs/?$'.format(UUID_PATTERN),
        views.PrivateBuildsViewset.as_view({'get': 'retrieve_logs'})),
    url(r'^(?P<private_build_id>{})/sync-status/?$'.format(UUID_PATTERN),
        views.PrivateBuildsViewset.as_view({'post': 'sync_status'})),
    url(r'^(?P<private_build_id>{})/sync-sonar-task-status/?$'.format(UUID_PATTERN),
        views.PrivateBuildsViewset.as_view({'post': 'sync_sonar_task_status'})),
    url(r'^(?P<private_build_id>{})/build-integration/?$'.format(UUID_PATTERN),
        views.PrivateBuildsViewset.as_view({'post': 'create_build_integration'})),
    url(r'^(?P<private_build_id>{})/ci_report/?$'.format(UUID_PATTERN),
        views.PrivateBuildsViewset.as_view({'get': 'retrieve_ci_report', 'post': 'ci_report'})),
    url(r'(?P<private_build_id>{})/artifacts/?$'.format(UUID_PATTERN),
        views.PrivateBuildsViewset.as_view({'delete': 'delete_artifacts'}))
]
