import logging
import base64
import semver
import string

from mekansm.request import MekRequest

logger = logging.getLogger(__name__)
SONAR60 = '6.0.0'
SONAR64 = '6.4.0'


class BaseClient(object):
    """Base sonar client for SonarQube"""

    def __init__(self, **kwargs):
        """
        :param kwargs: dict of arguments, contains
            endpoint: sonarqube address
            token: sonarapi token
            username: option
            password: option
        :type kwargs: dict
        """
        endpoint = kwargs.get("endpoint").rstrip("/")
        
        auth_token ="Basic " + BaseClient.get_authorization_token(
            kwargs.get("token"),
            kwargs.get("username"),
            kwargs.get("password")
        )

        
        self.endpoint = endpoint
        self.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'User-Agent': 'razzil/v1.0',
            'Authorization': auth_token
        }
        system_status = self.system_status()
        self.version = self.get_version(system_status['version'])
    
    @classmethod
    def get_authorization_token(cls, token, username=None, password=None):
        if token is not None:
            return base64.b64encode(token+":")

        return base64.b64encode(username+":"+password)

    @classmethod
    def get_url(cls, endpoint, path):
        return '{}/{}'.format(endpoint, path)

    def is_version60(self):
        return semver.compare(self.version, SONAR60) == 0
    
    def is_version64(self):
        return semver.compare(self.version, SONAR64) == 0

    def system_status(self):
        """Since version 5.2
        Get the server status

        :param endpoint: sonar base url
        :type param: str
        :rtype: dict, like {"id":"20180429053021","version":"6.0","status":"UP"}
        """
        path = 'api/system/status'
        return MekRequest.send(
            path, method="GET",
            endpoint=self.endpoint,
            headers=self.headers
        )['data']

    @classmethod
    def parsed_alert_status(cls, result):
        """
        :param result: samples:
            "measures": [
                    {
                        "metric": "security_rating",
                        "periods": [
                            {
                                "index": 1,
                                "value": "0.0"
                            }
                        ],
                        "value": "3.0"
                    }
                ]
        """
        measures = result.get("measures")
        for item in measures:
            if item.get("metric") == "alert_status":
                return item.get("value")

    def get_version(self, version):
        """return version with format major.minor like 6.4.0,6.0.0
        :param version: version info
        :type version: str
        :rtype: str
        """
        vers = string.split(version, '.')
        while len(vers) < 3:
            vers.append(str(0))
        return '.'.join(vers[:3])

    def get_language_list(self):
        """Since 5.1
        List supported programming languages
        """
        path = 'api/languages/list'
        return MekRequest.send(
            path, method="GET",
            endpoint=self.endpoint,
            headers=self.headers
        )['data']

    def get_qualitygates_list(self):
        """Since 4.3
        Get a list of quality gates
        """
        path = 'api/qualitygates/list'
        return MekRequest.send(
            path, method="GET",
            endpoint=self.endpoint,
            headers=self.headers
        )['data']

    def get_analysis_result(self, project_key):
        """Since 5.4
        Return component with specified measures.
        """
        fields64 = [
            "alert_status",
            "reliability_rating",
            "new_reliability_rating",
            "security_rating",
            "new_security_rating",
            "sqale_rating",
            "coverage",
            "new_coverage",
            "new_duplicated_lines_density",
            "duplicated_lines_density",
        ]
        fields60 = [
            "alert_status",
            "reliability_rating",
            "security_rating",
            "sqale_rating",
            "coverage",
            "duplicated_lines_density",
        ]
        params = {}

        if self.is_version64():
            logger.info('sonar version is {}, use componentKey'.format(SONAR64))
            params['componentKey'] = project_key
            params['metricKeys'] = ",".join(fields64)
        elif self.is_version60():
            logger.info('sonar version is {}, use componentId'.format(SONAR60))
            component = self.get_component(project_key)
            params['componentId'] = component['component']['id']
            params['metricKeys'] = ",".join(fields60)
        else:
            raise Exception('not support sonar version {}'.format(self.version))

        path = 'api/measures/component'
        data = MekRequest.send(
            path,
            method="GET",
            params=params,
            endpoint=self.endpoint,
            headers=self.headers
        )['data']

        if not data.get("component") or not data.get("component").get("measures"):
            logger.warn("get_analysis_result got unexpected data:{}".format(data))
            return None
        return {"measures": data.get("component").get("measures")}

    def get_component_tasks(self, project_key):
        params = {}
        if self.is_version64():
            logger.info('sonar version is {}, use componentKey'.format(SONAR64))
            params['componentKey'] = project_key
        elif self.is_version60():
            logger.info('sonar version is {}, use componentId'.format(SONAR60))
            component = self.get_component(project_key)
            params['componentId'] = component['component']['id']
        else:
            raise Exception('not support sonar version {}'.format(self.version))
        path = 'api/ce/component'
        
        return MekRequest.send(
            path, method="GET",
            params=params,
            endpoint=self.endpoint,
            headers=self.headers
        )['data']
    
    def get_qualitygates_project_status(self, project_key):
        path = 'api/qualitygates/project_status'
        params = {'projectKey': project_key}
        
        return MekRequest.send(
            path, method="GET",
            params=params,
            endpoint=self.endpoint,
            headers=self.headers
        )['data']

    def get_component(self, project_key):
        path = 'api/components/show'
        params = {}
        if self.is_version64():
            logger.info('sonar version is {}, use component'.format(SONAR64))
            params['component'] = project_key
        elif self.is_version60():
            logger.info('sonar version is {}, use key'.format(SONAR60))
            params['key'] = project_key
        else:
            raise Exception('not support sonar version {}'.format(self.version))
        
        return MekRequest.send(
            path, method="GET",
            params=params,
            endpoint=self.endpoint,
            headers=self.headers
        )['data']

    def get_analysis_task_info(self, task_id):
        path = 'api/ce/task?id={}'.format(task_id)
        return MekRequest.send(
            path, method="GET",
            endpoint=self.endpoint,
            headers=self.headers
        )['data']

    def get_ce_task_detail(self, task_id):
        return self.get_analysis_task_info(task_id)

    def remove_project_webhook(self, project_key):
        key = "sonar.webhooks.project"
        return self.reset_settings(project_key, key)

    def reset_settings(self, project_key, *keys):
        path = "api/settings/reset"
        if self.is_version60():
            logger.info('sonar version is {}, not support reset settings, skip'.format(self.version))
            return

        params = {
            "component": project_key,
            "keys": ",".join(keys)
        }

        return MekRequest.send(
            path,
            method="POST",
            params=params,
            endpoint=self.endpoint,
            headers=self.headers)['data']

class SonarClient(BaseClient):
    """
    kwargs:
        endpoint: sonarqube address
        token: sonarapi token
        username: option
        password: option
    """
    def __init__(self, **kwargs):
        super(SonarClient, self).__init__(**kwargs)