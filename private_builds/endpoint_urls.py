from django.conf.urls import url

from commons.constants import APPLICATION_TEMPLATE_URL_REGEX
from private_builds import views


urlpatterns = [
    url(r'(?P<namespace>{})/?$'.format(APPLICATION_TEMPLATE_URL_REGEX),
        views.PrivateBuildsViewset.as_view({
            'get': 'list_endpoint',
            'post': 'create_endpoint',
            'delete': 'delete_endpoint'
        })),
]
