import base64
from itertools import izip, cycle

from django.conf import settings
from django.db import models
from django.utils.six import string_types


class XORCipher(object):

    def __init__(self, secret_key):
        self.secret_key = secret_key

    def _xor_text(self, text):
        return ''.join(chr(ord(x) ^ ord(y)) for (x, y) in izip(text, cycle(self.secret_key)))

    def encrypt(self, text):
        return base64.encodestring(self._xor_text(text)).strip()

    def decrypt(self, text):
        return self._xor_text(base64.decodestring(text))


class AlaudaXORCipher(XORCipher):
    secret_key = settings.SECRET_KEY
    ENCRYPT_FLAG = 'AlaudaEncrypted:'

    def __init__(self):
        pass

    def is_encrypted_text(self, value):
        return bool(isinstance(value, string_types) and value.startswith(self.ENCRYPT_FLAG))

    def encrypt(self, value):
        return self.ENCRYPT_FLAG + super(AlaudaXORCipher, self).encrypt(value)

    def decrypt(self, value):
        assert self.is_encrypted_text(value), ('{} can not be decrypted by AlaudaXORCipher, '
                                               'AlaudaXORCipher can only decrypt string that '
                                               'starts with {}'.format(value, self.ENCRYPT_FLAG))

        value = value[len(self.ENCRYPT_FLAG):]
        return super(AlaudaXORCipher, self).decrypt(value)

    def safe_decrypt(self, value):
        if self.is_encrypted_text(value):
            return self.decrypt(value)
        else:
            return value


class EncryptedCharField(models.CharField):
    def to_python(self, value):

        if AlaudaXORCipher().is_encrypted_text(value):
            return value

        if value is None:
            return None

        return AlaudaXORCipher().safe_decrypt(value)

    def from_db_value(self, value, expression, connection, context):
        if value is None:
            return value
        return AlaudaXORCipher().safe_decrypt(value)

    def get_prep_value(self, value):

        if value is None:
            return None

        return AlaudaXORCipher().encrypt(value)
