import logging
import traceback

from django.conf import settings

from alauda.furion.client import FurionClient
from commons.docker_build_exception import DockerBuildException
from private_builds.models import BuildEndpoint, PUBLIC_ENDPOINT_NAME

LOG = logging.getLogger(__name__)
FURION_CLIENT = FurionClient(endpoint=settings.REGION_MANAGER['endpoint'],
                             username=settings.REGION_MANAGER['username'],
                             password=settings.REGION_MANAGER['password'])


class FurionUtil(object):

    build_endpoint_query_set = BuildEndpoint.objects

    display_name_map = {}

    @classmethod
    def get_display_name_map_by_endpoint_ids(cls, endpoint_ids):
        '''
            return map endpoint_id:display_name
        '''
        if not endpoint_ids:
            return {}
        endpoint_ids = filter(lambda x:x, endpoint_ids)
        filter_args = {
            'endpoint_id__in': endpoint_ids
        }
        endpoint_lists = cls.build_endpoint_query_set.filter(**filter_args)
        # generally , there is only one namespace
        namespaces = []
        for endpoint in endpoint_lists:
            if endpoint.namespace and not endpoint.namespace in namespaces:
                namespaces.append(endpoint.namespace)

        result = {}
        for namespace in namespaces:
            display_name_map = cls.get_display_name_map_by_namespace(namespace)
            result = dict(result, **display_name_map)
        # there is no need to care about some unusing endpoit id in result
        return result

    @classmethod
    def get_display_name_map_by_namespace(cls, namespace):
        '''
            return map endpoint_id:displayname
        '''
        if not namespace:
            return {}

        region_id_to_name_map = cls.get_region_name_map(namespace)
        endpoint_list = cls.build_endpoint_query_set.filter(
                    namespace=namespace)
        display_name_map = {}
        for endpoint in endpoint_list:
            if endpoint.is_public:
                display_name_map[endpoint.endpoint_id] = PUBLIC_ENDPOINT_NAME
                continue

            region_name = region_id_to_name_map.get(endpoint.region_id)
            if not region_name:
                display_name_map[endpoint.endpoint_id] = endpoint.endpoint_id
                LOG.warn("not get region name by region id:{}, endpoint id:{}".format(
                    endpoint.region_id, endpoint.endpoint_id))
                continue

            display_name_map[endpoint.endpoint_id] = region_name
        return display_name_map


    @classmethod
    def get_display_name_by_endpoint_id(cls, endpoint_id):
        if not endpoint_id:
            return None
        endpoint_id = str(endpoint_id)
        if not cls.display_name_map.get(endpoint_id):
            endpoint_query_set = cls.build_endpoint_query_set.filter(endpoint_id=endpoint_id)
            if endpoint_query_set.exists():
                endpoint_result = endpoint_query_set.get()
                if endpoint_result.is_public:
                    cls.display_name_map[endpoint_id] = PUBLIC_ENDPOINT_NAME
                    return PUBLIC_ENDPOINT_NAME
                namespace = endpoint_result.namespace
                endpoint_namespace_result_list = cls.build_endpoint_query_set.filter(
                    namespace=namespace)
                region_id_to_name_map = FurionUtil.get_region_name_map(namespace)
                for endpoint_namespace_result in endpoint_namespace_result_list:
                    if endpoint_namespace_result.is_public:
                        continue
                    tmp_endpoint_id = str(endpoint_namespace_result.endpoint_id)
                    tmp_region_name = region_id_to_name_map.get(
                        str(endpoint_namespace_result.region_id))
                    cls.display_name_map[tmp_endpoint_id] = tmp_region_name

        return cls.display_name_map.get(endpoint_id)

    @staticmethod
    def get_region_name_map(namespace):
        region_id_to_name_map = {}
        try:
            region_list = FURION_CLIENT.list_regions(namespace=namespace)
            for region in region_list:
                region_id_to_name_map[region.get('id')] = region.get('display_name')

        except Exception:
            LOG.error('call furion to get region name failed, error:{}'.format(
                traceback.format_exc()))
            raise DockerBuildException('call_furion_failed')
        return region_id_to_name_map
